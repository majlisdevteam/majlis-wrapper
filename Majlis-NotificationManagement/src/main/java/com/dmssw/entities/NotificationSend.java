/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dmssw.entities;

/**
 *
 * @author Shanka
 */
public class NotificationSend {
    
    
    private int notificationId;
    
    private int notificationSendTo;
    
    private int notifictionType;

    public int getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(int notificationId) {
        this.notificationId = notificationId;
    }

    public int getNotificationSendTo() {
        return notificationSendTo;
    }

    public void setNotificationSendTo(int notificationSendTo) {
        this.notificationSendTo = notificationSendTo;
    }

    public int getNotifictionType() {
        return notifictionType;
    }

    public void setNotifictionType(int notifictionType) {
        this.notifictionType = notifictionType;
    }
    
    
    
    
    
    
    
}
