/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.dmssw.majlis.entity;

/**
 * <p>
 * <b>Title</b> CategoryCount
 * </p>
 * <p>
 * <b>Description</b> Enter Purpose of this class
 * </p>
 * <p>
 * <b>Company</b> Data Management Systems (PVT) Ltd
 * </p>
 * <p>
 * <b>Copyright</b> Copyright (c) 2015
 * </p>
 * @author Hashan Jayakody
 * @version 1.0
 */

public class CategoryCount {

    private int id;
    private String categoryName;
    private int count;
    private double percentage;

    public CategoryCount() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    
    public CategoryCount(String categoryName, int count) {
        this.categoryName = categoryName;
        this.count = count;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public double getPercentage() {
        return percentage;
    }

    public void setPercentage(double percentage) {
        this.percentage = percentage;
    }
    
    
    
}

