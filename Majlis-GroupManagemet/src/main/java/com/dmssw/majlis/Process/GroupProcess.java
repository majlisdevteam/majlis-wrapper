/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dmssw.majlis.Process;

import com.dmssw.orm.controllers.DbCon;
import com.dmssw.orm.models.MajlisGroup;
import com.dmssw.majlis.controller.ResponseData;
import com.dmssw.majlis.entities.EventGroup;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Dilhan Ganegama
 */
public class GroupProcess {

    public void getAllGroupDetailsProcess(String sql,ResponseData rd) {
        
        DbCon db = new DbCon();

        Connection conn = db.getCon();
        
        List<EventGroup> groupList = new ArrayList<>();
        
        try {

            ResultSet rs = db.search(conn, sql);
            while (rs.next()) {

                EventGroup m = new EventGroup();

                m.setGroupId(rs.getInt("GROUP_ID"));
                m.setTitle(rs.getString("GROUP_TITLE"));
                m.setCountry(rs.getString("GROUP_COUNTRY"));
                m.setDistrict(rs.getString("GROUP_DISTRICT"));
                m.setCity(rs.getString("GROUP_CITY"));

                groupList.add(m);

            }
            
//            List<MajlisGroup> privateGroupList = getAllPrivateGroupDetails();
//            if (privateGroupList != null) {
//
//                rd.setResponseCode(privateGroupList);
//            } else {
//                rd.setResponseCode("No Private groups available.");
//            }

            rd.setResponseData(groupList);

            conn.close();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
