/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dmssw.majlis.service;

import com.dmssw.orm.models.MajlisGroup;
import com.dmssw.majlis.controller.GroupController;
import com.dmssw.majlis.entities.EventUserGroup;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

/**
 *
 * @author Sandali Kaushalya
 */
@Path("/groups")
public class Groups {

    @EJB
    GroupController groupController;
    //========================================================================================================

    @GET
    @Path("/getAllGroupLevels")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getAllGroupLevels(
            @QueryParam("country") @DefaultValue("all") String country,
            @QueryParam("district") @DefaultValue("all") String district,
            @QueryParam("city") @DefaultValue("all") String city) {
        GroupController groupController = new GroupController();
        return Response.status(Response.Status.OK).entity(groupController.getAllGroupLevels(country, district, city)).build();
    }

    @GET
    @Path("/getGroupPublicLevel")
    @Produces({MediaType.APPLICATION_JSON})

    public Response getGroupLevel() {
        GroupController groupController = new GroupController();
//     
        return Response.status(Response.Status.OK).entity(groupController.getGroupPublicLevel()).build();
    }

    @GET
    @Path("/getCount")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getCount(@QueryParam("userId") @DefaultValue("all") String userId) {

        return Response.status(Response.Status.OK).entity(groupController.getCount(userId)).build();
    }

    @GET
    @Path("/getAllAcceptUsers")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getAllAcceptUsers(@QueryParam("groupId") @DefaultValue("all") int groupId) {

        return Response.status(Response.Status.OK).entity(groupController.getAllAcceptUsers(groupId)).build();
    }

    @POST
    @Path("/approveUser")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response approveUser(EventUserGroup user) {

        return Response.status(Status.OK).entity(groupController.approveUser(user)).build();
    }

    @GET
    @Path("/getTopGroups")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getTopGroups() {

        return Response.status(Response.Status.OK).entity(groupController.getTopGroups()).build();
    }

    @GET
    @Path("/getTopGroups")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getAllMobileGroupsCMS(@QueryParam("groupId") @DefaultValue("0") int groupId,
            @QueryParam("userId") @DefaultValue("all") String userId) {

        return Response.status(Response.Status.OK).entity(groupController.getAllMobileGroupsCMS(groupId,userId)).build();
    }

    @GET
    @Path("/getAllGroupsById")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getAllGroupsById(@QueryParam("user") @DefaultValue("all") String userId,
            @QueryParam("start") @DefaultValue("0") int start,
            @QueryParam("limit") @DefaultValue("25") int limit) {
        GroupController groupController = new GroupController();
        return Response.status(Response.Status.OK).entity(groupController.getAllGroups(userId, start, limit)).build();

    }

    @GET
    @Path("/getGroupDetails")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getSelectedGroupDetails(@QueryParam("group") @DefaultValue("0") int groupId) {
        GroupController groupController = new GroupController();
        return Response.status(Response.Status.OK).entity(groupController.getGroupDetails(groupId)).build();

    }

    @GET
    @Path("/getAllGroups")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getAllGroups(@QueryParam("groupId") @DefaultValue("0") int groupId,
            @QueryParam("groupTitle") @DefaultValue("all") String groupTitle,
            @QueryParam("groupCountry") @DefaultValue("all") String groupCountry,
            @QueryParam("groupDistrict") @DefaultValue("all") String groupDistrict,
            @QueryParam("groupCity") @DefaultValue("all") String groupCity) {

        GroupController groupController = new GroupController();
        return Response.status(Response.Status.OK).entity(groupController.getAllGroupDetails(groupId,
                groupTitle, groupCountry, groupDistrict, groupCity)).build();

    }

    @GET
    @Path("/getEventsByGroupMobile")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getEventsByGroupMobile(@QueryParam("groupId") @DefaultValue("0") int groupId) throws Exception {
        System.out.println("method is called ");
        return Response.status(Response.Status.OK).entity(groupController.getEventsByGroupMobile(groupId)).build();

    }

    @GET
    @Path("/getAllMobileGroups")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getAllMobileGroups(@QueryParam("groupCountry") @DefaultValue("all") String groupCountry,
            @QueryParam("groupDistrict") @DefaultValue("all") String groupDistrict,
            @QueryParam("groupCity") @DefaultValue("all") String groupCity) throws Exception {
        System.out.println("method is called ");
        return Response.status(Response.Status.OK).entity(groupController.getAllMobileGroups(groupCountry, groupDistrict, groupCity)).build();

    }

    @GET
    @Path("/getGroupImage")
    @Produces(MediaType.TEXT_HTML)
    public Response getGroupImage(@QueryParam("groupId") @DefaultValue("0") int grpId) {

        GroupController ctrl = new GroupController();
        return Response.status(Response.Status.OK).entity(ctrl.getGroupImage(grpId)).build();
    }

    //===================================================================================================
    /**
     *
     * @param start- starting index of the record.
     * @param limit- number of records for a service call
     * @return a group list.
     * /Majlis-GroupManagement-1.0/service/groups/getGroupList
     */
    @GET
    @Path("/getGroupList")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getGroupList(@QueryParam("userId") @DefaultValue("all") String userId, @QueryParam("start") @DefaultValue("0") int start,
            @QueryParam("limit") @DefaultValue("25") int limit) {

        return Response.status(Response.Status.OK).entity(groupController.getGroupList(userId, start, limit)).build();
    }

    @POST
    @Path("/updateGroupsForUser")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateGroupsForUser(EventUserGroup group) {

        return Response.status(Status.OK).entity(groupController.updateGroupsForUser(group)).build();
    }

    /**
     *
     * @param groupId-unique id of a group.
     * @param start - starting index of the record.
     * @param limit - number of records for a service call
     * @return a majlis group object
     * /Majlis-GroupManagement-1.0/service/groups/getGroupDetail
     */
    @GET
    @Path("/getGroupDetail")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getGroupDetail(@QueryParam("groupid") @DefaultValue("0") int groupId) {

        return Response.status(Response.Status.OK).entity(groupController.getGroupDetail(groupId)).build();
    }

    /**
     *
     * @param majlisGroup
     * @return 1 if successful ,999 if fails.
     * /Majlis-GroupManagement-1.0/service/groups/createGroup
     */
    @POST
    @Path("/createGroup")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces({MediaType.APPLICATION_JSON})
    public Response createGroup(MajlisGroup majlisGroup,
            @HeaderParam("userName") String userName) {

        System.out.println("Starting service...");
        return Response.status(Response.Status.OK).entity(groupController.createGroup(majlisGroup, userName)).build();
    }

    /**
     * Update an existing 'majlis_group' database record
     *
     * @param majlisGroup
     * @return 1 if successful ,999 if fails.
     * /Majlis-GroupManagement-1.0/service/groups/updateGroup
     */
    @POST
    @Path("/updateGroup")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces({MediaType.APPLICATION_JSON})
    public Response updateGroup(@HeaderParam("userId") String userId, MajlisGroup majlisGroup,
            @HeaderParam("userName") String userName) {

        return Response.status(Response.Status.OK).entity(groupController.modifyGroup(userId, majlisGroup, userName)).build();
    }

    @GET
    @Path("/getStatus")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getGroupDetail(@QueryParam("type") String type, @QueryParam("subType") String subType) {

        return Response.status(Response.Status.OK).entity(groupController.getStatus(type, subType)).build();
    }

    @POST
    @Path("/saveGroupIcon")
    @Consumes(MediaType.MEDIA_TYPE_WILDCARD)
    @Produces(MediaType.APPLICATION_JSON)
    public Response saveGroupIcon(MultipartFormDataInput input, @Context HttpHeaders headers) {

        headers.getRequestHeaders().forEach((key, val) -> {

            for (String string : val) {
                System.out.println("key " + key + " val... " + val);
            }

        });

        return Response.status(Status.OK).entity(groupController.saveGroupIcon(input)).build();
    }

}
