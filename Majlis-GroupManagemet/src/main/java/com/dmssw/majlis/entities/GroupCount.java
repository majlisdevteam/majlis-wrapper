/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entities;

/**
 * <p>
 * <b>Title</b> GroupCount
 * </p>
 * <p>
 * <b>Description</b> Enter Purpose of this class
 * </p>
 * <p>
 * <b>Company</b> Data Management Systems (PVT) Ltd
 * </p>
 * <p>
 * <b>Copyright</b> Copyright (c) 2015
 * </p>
 * @author Hashan Jayakody
 * @version 1.0
 */

public class GroupCount {

    private String groupName;
    private int count;

    public GroupCount() {
    }

    public GroupCount(String groupName, int count) {
        this.groupName = groupName;
        this.count = count;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }
    
    
}

