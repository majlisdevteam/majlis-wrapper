/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dmssw.majlis.entities;

/**
 *
 * @author GRTZ
 */
public class EventGroup {
    
    private int groupId;
    private String groupSystemId;
    private String title;
    private String icon;
    private String Description;
    private String country;
    private String district;
    private String city;
    private int status;
    private int level;
    private String groupAdmin;
    private String CodeMessage;
    

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public String getGroupSystemId() {
        return groupSystemId;
    }

    public void setGroupSystemId(String groupSystemId) {
        this.groupSystemId = groupSystemId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getGroupAdmin() {
        return groupAdmin;
    }

    public void setGroupAdmin(String groupAdmin) {
        this.groupAdmin = groupAdmin;
    }

    public String getCodeMessage() {
        return CodeMessage;
    }

    public void setCodeMessage(String CodeMessage) {
        this.CodeMessage = CodeMessage;
    }



    
    
   

}
