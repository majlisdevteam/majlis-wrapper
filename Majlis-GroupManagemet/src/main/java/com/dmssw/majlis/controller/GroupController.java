/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dmssw.majlis.controller;

import com.dmssw.majlis.entities.Event;
import com.dmssw.orm.controllers.DbCon;
import com.dmssw.orm.controllers.ORMConnection;
import com.dmssw.orm.models.MajlisCategory;
import com.dmssw.orm.models.MajlisGroup;
import com.dmssw.orm.models.MajlisMdCode;
import com.dmssw.orm.config.AppParams;

import com.dmssw.orm.config.AppParams;

import com.dmssw.majlis.entities.EventGroup;
import com.dmssw.majlis.entities.EventUserGroup;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.annotation.Resources;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.imageio.ImageIO;
import javax.transaction.UserTransaction;
import javax.ws.rs.core.MultivaluedMap;
import org.apache.commons.io.IOUtils;
import org.hibernate.Session;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import org.jboss.resteasy.util.Base64.InputStream;
import com.dmssw.majlis.util.ResponseWrapper;
import java.util.Random;
import com.dmssw.orm.controllers.DbCon;
import com.dmssw.orm.models.MajlisGroupEvent;
import com.dmssw.orm.models.MajlisMobileUsers;
import entities.GroupCount;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author Sandali Kaushalya
 */
@Stateless
@LocalBean
@TransactionManagement(TransactionManagementType.BEAN)
public class GroupController {

    private org.apache.log4j.Logger logger;

    public GroupController() {

        logger = AppParams.logger;
    }

    @Resource
    UserTransaction transaction;

    public ResponseWrapper getGroupPublicLevel() {
//        System.out.println("getGroupPublicLevel method call.................");

        List<MajlisMdCode> mdCodeList = new ArrayList<>();

        ResponseWrapper rw = new ResponseWrapper();
        int i = 0;

        try {

            ORMConnection conn = new ORMConnection();

            //      System.out.println("select try block 4");
            String SelectQuery = "SELECT C FROM MajlisMdCode C WHERE C.codeType = 'GROUPS' AND C.codeSubType='GROUP_LEVELS' AND C.codeLocale='LK'";

            //   System.out.println("Query............" + SelectQuery);
            List<Object> query = conn.hqlGetResults(SelectQuery);

            //   System.out.println("select try block 44");
            //   System.out.println("result");
            for (Object object : query) {
                i++;
                MajlisMdCode mdCode = (MajlisMdCode) object;

                MajlisMdCode md = new MajlisMdCode();

                md.setCodeId(mdCode.getCodeId());
                md.setCodeLocale(mdCode.getCodeLocale());
                md.setCodeMessage(mdCode.getCodeMessage());
                md.setCodeSeq(mdCode.getCodeSeq());
                md.setCodeSubType(mdCode.getCodeSubType());

                mdCodeList.add(md);
                rw.setResponseFlag(1);
                rw.setTotalRecords(i);
                System.out.println("mcode out..... " + mdCode.getCodeMessage());

            }

            rw.setResponseData(mdCodeList);

        } catch (Exception e) {

            rw.setResponseFlag(-1);
            rw.setTotalRecords(i);
            rw.setResponseData("Error in getGroupPublicLevel method");
            e.printStackTrace();
        }

        return rw;
    }

    /**
     * Returns All groups by given user id.
     *
     * @param userId
     * @param start
     * @param limit
     * @return
     */
    public ResponseWrapper getAllGroups(String userId, int start, int limit) {
        System.out.println("getAllGroups method call.................");

        ResponseWrapper rw = new ResponseWrapper();
        ORMConnection conn = new ORMConnection();

        MajlisGroup group = new MajlisGroup();
        List<MajlisGroup> groupList = new ArrayList<>();

        int i = 0;

        try {

            System.out.println("User ID " + userId);

            if (userId.equals("all")) {

                System.out.println("equals to admin condition");

                String selectQuery = "SELECT G FROM MajlisGroup G ORDER BY groupTitle ASC";
                List<Object> li = conn.hqlGetResultsWithLimit(selectQuery, start, limit);

                for (Object obj : li) {
                    i++;
                    group = (MajlisGroup) obj;
                    groupList.add(group);

                }

            } else {
                String selectQuery = "SELECT G FROM MajlisGroup G "
                        + "WHERE G.groupAdmin = '" + userId + "'";

                List<Object> list = conn.hqlGetResults(selectQuery);

                for (Object obj : list) {
                    i++;
                    group = (MajlisGroup) obj;
                    groupList.add(group);

                }
            }

            rw.setResponseData(groupList);
            rw.setResponseFlag(1);
            rw.setTotalRecords(i);

        } catch (Exception e) {
            System.out.println("Error in getAllGroups method:  " + e.getMessage());
            rw.setResponseFlag(-1); // exception occur
            rw.setTotalRecords(i);
            rw.setResponseData("Error in getAllGroups method");
            e.printStackTrace();
        }
        return rw;
    }

    public ResponseWrapper getGroupDetails(int groupId) {
        System.out.println("getGroupDetails method call.................");

        int i = 0;

        ResponseWrapper rw = new ResponseWrapper();
        ORMConnection conn = new ORMConnection();

        MajlisGroup group = new MajlisGroup();

        try {

            if (groupId != 0) {

                String selectQuery = "SELECT G FROM MajlisGroup G WHERE G.groupId='" + groupId + "'";
                List<Object> li = conn.hqlGetResults(selectQuery);

                for (Object obj : li) {
                    i++;
                    group = (MajlisGroup) obj;

                }
                rw.setResponseData(group);
                rw.setTotalRecords(i);
                rw.setResponseFlag(1);
                //success

            } else {
                rw.setResponseFlag(8); // group id not provided
                rw.setTotalRecords(i);
                rw.setResponseData("Group id not provided.");
            }

        } catch (Exception e) {
            System.out.println("Error in getGroupDetails method:  " + e.getMessage());
            rw.setResponseFlag(-1); // exception occur
            rw.setTotalRecords(i);
            rw.setResponseData("Error in getAllGroups method");
            e.printStackTrace();
        }
        return rw;
    }

    public ResponseData getEventsByGroupMobile(int groupId) {

        int result = -1;

        DbCon db = new DbCon();

        Connection conn = db.getCon();

        ResponseData rd = new ResponseData();

        List<Event> eventList = new ArrayList<>();

        try {
            String where = (groupId == 0) ? "" : " WHERE E.GROUP_ID='" + groupId + "'";

            String sqlForGroups = "SELECT * "
                    + "FROM majlis_group_event E " + where;

            ResultSet rs = db.search(conn, sqlForGroups);

            while (rs.next()) {

                Event event = new Event();

                event.setEventCaption(rs.getString("EVENT_CAPTION"));
                event.setEventDate(rs.getString("EVENT_DATE"));

                String imgIcon = com.dmssw.majlis.config.AppParams.MEDIA_SERVER + "/" + rs.getString("EVENT_ICON_PATH");

                event.setEventIconPath(imgIcon);

                event.setEventLocation(rs.getString("EVENT_LOCATION"));

                eventList.add(event);

            }
            rd.setResponseData(eventList);
            result = 1;

        } catch (Exception ex) {

            result = 999;

            ex.printStackTrace();

        }
        rd.setResponseCode(result);
        return rd;
    }

    public ResponseData getAllMobileGroups(String countyCode, String province, String city) {

        int result = -1;

        ResponseData rd = new ResponseData();

        List<MajlisGroup> groupList = new ArrayList<>();

//        System.out.println("province"+province);
        try {

            if (!countyCode.toUpperCase().equals("NULL")) {

                DbCon db1 = new DbCon();

                Connection conn1 = db1.getCon();

                String sqlForGetCountryGroup = "SELECT * "
                        + "FROM majlis_group G "
                        + "WHERE G.GROUP_PUBLIC_LEVEL IN (SELECT CODE_ID FROM majlis_md_code C "
                        + "                             WHERE C.CODE_TYPE='GROUPS' AND C.CODE_SUB_TYPE = 'GROUP_LEVELS' "
                        + "                             AND C.CODE_MESSAGE='Country') "
                        + "AND G.GROUP_COUNTRY ='" + countyCode + "' AND G.GROUP_STATUS=3 ";

                ResultSet rsCountry = db1.search(conn1, sqlForGetCountryGroup);

//                System.out.println("Country wise "+ sqlForGetCountryGroup);
                while (rsCountry.next()) {
                    MajlisGroup g = new MajlisGroup();

                    g.setGroupTitle(rsCountry.getString("GROUP_TITLE"));
                    g.setGroupDescription(rsCountry.getString("GROUP_DESCRIPTION"));
                    g.setGroupId(rsCountry.getInt("GROUP_ID"));

                    String iconPath = com.dmssw.majlis.config.AppParams.MEDIA_SERVER + "/" + rsCountry.getString("GROUP_ICON_PATH");

                    g.setGroupIconPath(iconPath);

                    groupList.add(g);
                }

                conn1.close();
            }

            if (!province.toUpperCase().equals("NULL")) {
                DbCon db2 = new DbCon();

                Connection conn2 = db2.getCon();

                String sqlForGetProvinceGroup = "SELECT * "
                        + "FROM majlis_group G "
                        + "WHERE G.GROUP_PUBLIC_LEVEL IN (SELECT CODE_ID FROM majlis_md_code C "
                        + "                             WHERE C.CODE_TYPE='GROUPS' AND C.CODE_SUB_TYPE = 'GROUP_LEVELS' "
                        + "                             AND C.CODE_MESSAGE='District') "
                        + "AND G.GROUP_DISTRICT ='" + province + "'  AND G.GROUP_STATUS=3";

//                System.out.println("Country wise "+ sqlForGetProvinceGroup);
                ResultSet rsProvince = db2.search(conn2, sqlForGetProvinceGroup);

                while (rsProvince.next()) {

                    MajlisGroup g = new MajlisGroup();

                    g.setGroupTitle(rsProvince.getString("GROUP_TITLE"));
                    g.setGroupDescription(rsProvince.getString("GROUP_DESCRIPTION"));
                    g.setGroupId(rsProvince.getInt("GROUP_ID"));

                    String iconPath = com.dmssw.majlis.config.AppParams.MEDIA_SERVER + "/" + rsProvince.getString("GROUP_ICON_PATH");

                    g.setGroupIconPath(iconPath);

                    groupList.add(g);
                }

            }

            if (!city.toUpperCase().equals("NULL")) {
                DbCon db3 = new DbCon();

                Connection conn3 = db3.getCon();

                String sqlForGetProvinceGroup = "SELECT * "
                        + "FROM majlis_group G "
                        + "WHERE G.GROUP_PUBLIC_LEVEL IN (SELECT CODE_ID FROM majlis_md_code C "
                        + "                             WHERE C.CODE_TYPE='GROUPS' AND C.CODE_SUB_TYPE = 'GROUP_LEVELS' "
                        + "                             AND C.CODE_MESSAGE='City') "
                        + "AND G.GROUP_CITY ='" + city + "'  AND G.GROUP_STATUS=3";

//                System.out.println("Country wise "+ sqlForGetProvinceGroup);
                ResultSet rsProvince = db3.search(conn3, sqlForGetProvinceGroup);

                while (rsProvince.next()) {

                    MajlisGroup g = new MajlisGroup();

                    g.setGroupTitle(rsProvince.getString("GROUP_TITLE"));
                    g.setGroupDescription(rsProvince.getString("GROUP_DESCRIPTION"));
                    g.setGroupId(rsProvince.getInt("GROUP_ID"));

                    String iconPath = com.dmssw.majlis.config.AppParams.MEDIA_SERVER + "/" + rsProvince.getString("GROUP_ICON_PATH");

                    g.setGroupIconPath(iconPath);

                    groupList.add(g);
                }

                conn3.close();
            }

            DbCon db4 = new DbCon();

            Connection conn4 = db4.getCon();

            String sqlForGetProvinceGroup = "SELECT * "
                    + "FROM majlis_group G "
                    + "WHERE G.GROUP_PUBLIC_LEVEL IN (SELECT CODE_ID FROM majlis_md_code C "
                    + "                             WHERE C.CODE_TYPE='GROUPS' AND C.CODE_SUB_TYPE = 'GROUP_LEVELS' "
                    + "                             AND C.CODE_MESSAGE='Private') "
                    + "AND G.GROUP_COUNTRY ='" + countyCode + "'  AND G.GROUP_STATUS=3";

//                System.out.println("Private groups "+ sqlForGetProvinceGroup);
            ResultSet rsProvince = db4.search(conn4, sqlForGetProvinceGroup);

            while (rsProvince.next()) {

                MajlisGroup g = new MajlisGroup();

                g.setGroupTitle(rsProvince.getString("GROUP_TITLE"));
                g.setGroupDescription(rsProvince.getString("GROUP_DESCRIPTION"));
                g.setGroupId(rsProvince.getInt("GROUP_ID"));

                String iconPath = com.dmssw.majlis.config.AppParams.MEDIA_SERVER + "/" + rsProvince.getString("GROUP_ICON_PATH");

                g.setGroupIconPath(iconPath);

                groupList.add(g);
            }

            rd.setResponseData(groupList);
            result = 1;
        } catch (Exception e) {

            result = 999;

            e.printStackTrace();

        }

        rd.setResponseCode(result);

        return rd;

    }

    public ResponseData getAllGroupDetails(int groupId, String groupTitle, String groupCountry, String groupDistrict, String groupCity) {

        System.out.println("getAllGroupDetails method call.................");
        ResponseData rd = new ResponseData();
        DbCon db = new DbCon();

        Connection conn = db.getCon();
        List<EventGroup> groupList = new ArrayList<>();
        try {
            String where = (groupId == 0 ? " g.GROUP_ID= g.GROUP_ID" : " g.GROUP_ID = '" + groupId + "'");
            where += (groupTitle.toUpperCase().equals("ALL")) ? " AND IFNULL(g.GROUP_TITLE,'AA') =IFNULL(g.GROUP_TITLE,'AA') " : " AND g.GROUP_TITLE LIKE '%" + groupTitle + "%' ";
            where += (groupCountry.toUpperCase().equals("ALL")) ? " AND IFNULL(g.GROUP_COUNTRY,'AA')=IFNULL(g.GROUP_COUNTRY,'AA') " : " AND g.GROUP_COUNTRY LIKE '%" + groupCountry + "%' ";
            where += (groupDistrict.toUpperCase().equals("ALL")) ? " AND IFNULL(g.GROUP_DISTRICT,'AA') = IFNULL(g.GROUP_DISTRICT,'AA') " : " AND g.GROUP_DISTRICT LIKE '%" + groupDistrict + "%' ";
            where += (groupCity.toUpperCase().equals("ALL")) ? " AND IFNULL(g.GROUP_CITY,'AA')=IFNULL(g.GROUP_CITY,'AA') " : " AND g.GROUP_CITY LIKE '%" + groupCity + "%' ";

            String sql = "SELECT * "
                    + "FROM majlis_group g WHERE " + where;

            System.out.println("query... " + sql);

            ResultSet rs = db.search(conn, sql);

            while (rs.next()) {

                EventGroup m = new EventGroup();

                m.setGroupId(rs.getInt("GROUP_ID"));
                m.setTitle(rs.getString("GROUP_TITLE"));

                String icon = rs.getString("GROUP_ICON_PATH");

                String iconpath = com.dmssw.majlis.config.AppParams.MEDIA_SERVER + "/" + icon;

                m.setIcon(iconpath);
                m.setCountry(rs.getString("GROUP_COUNTRY"));
                m.setDistrict(rs.getString("GROUP_DISTRICT"));
                m.setCity(rs.getString("GROUP_CITY"));

                groupList.add(m);

            }
            List<MajlisGroup> privateGroupList = getAllPrivateGroupDetails();
            if (privateGroupList != null) {

                rd.setResponseCode(privateGroupList);
            } else {
                rd.setResponseCode("No Private groups available.");
            }

            rd.setResponseData(groupList);

            conn.close();
        } catch (Exception e) {
            System.out.println("Error in getAllGroupDetails: " + e.getMessage());
            e.printStackTrace();
        }

        return rd;
    }

    public ResponseData approveUser(EventUserGroup user) {

        ResponseData rd = new ResponseData();

        int result = -1;

        DbCon db = new DbCon();

        Connection conn = db.getCon();

        try {
            transaction.begin();
            try {

                String sqlForAddroup = "INSERT INTO majlis_mobile_user_group (USER_ID,GROUP_ID) VALUES(?,?)";

                PreparedStatement ps1 = db.prepare(conn, sqlForAddroup);

                ps1.setInt(1, user.getUserId());
                ps1.setInt(2, user.getGroupId());

                ps1.executeUpdate();

                String sqlForUpdatePermission = "UPDATE tmp_group_request SET STATUS=2 WHERE USER_ID='" + user.getUserId() + "' AND GROUP_ID='" + user.getGroupId() + "'";

                PreparedStatement ps2 = db.prepare(conn, sqlForUpdatePermission);

                ps2.executeUpdate();

                transaction.commit();

                result = 1;

            } catch (Exception e) {
                result = 999;
                transaction.rollback();
                e.printStackTrace();
            }

        } catch (Exception ex) {
            result = 999;
            ex.printStackTrace();
        }

        rd.setResponseCode(result);
        return rd;
    }

    public ResponseData getAllAcceptUsers(int groupId) {

        ResponseData rd = new ResponseData();

        int result = -1;

        DbCon db = new DbCon();

        Connection conn = db.getCon();

        List<MajlisMobileUsers> mobileUserList = new ArrayList<>();

        try {

            String sqlForPendingUsers = "SELECT * "
                    + "FROM  majlis_mobile_users u, tmp_group_request r "
                    + "WHERE u.MOBILE_USER_ID = r.USER_ID "
                    + "AND r.GROUP_ID = '" + groupId + "' "
                    + "AND r.STATUS=1";

            ResultSet rs = db.search(conn, sqlForPendingUsers);

            while (rs.next()) {

                MajlisMobileUsers mobile = new MajlisMobileUsers();

                mobile.setMobileUserId(rs.getInt("MOBILE_USER_ID"));
                mobile.setMobileUserName(rs.getString("MOBILE_USER_NAME"));
                mobile.setMobileUserEmail(rs.getString("MOBILE_USER_EMAIL"));
                mobile.setMobileUserDob(rs.getDate("MOBILE_USER_DOB"));
                mobile.setMobileUserMobileNo(rs.getString("MOBILE_USER_MOBILE_NO"));
                mobile.setMobileUserCountry(rs.getString("MOBILE_USER_COUNTRY"));
                mobile.setMobileUserProvince(rs.getString("MOBILE_USER_PROVINCE"));
                mobile.setMobileUserCity(rs.getString("MOBILE_USER_CITY"));
                mobile.setMobileUserDateRegistration(rs.getDate("MOBILE_USER_DATE_REGISTRATION"));

                mobileUserList.add(mobile);

            }

            rd.setResponseData(mobileUserList);
            conn.close();
            result = 1;
        } catch (Exception e) {

            result = 999;

            e.printStackTrace();
        }

        rd.setResponseCode(result);

        return rd;

    }

    public ResponseData getCount(String userId) {

        ResponseData responseData = new ResponseData();
        int result = -1;

        Map countMap = new HashMap<String, Integer>();

        DbCon dbCon = new DbCon();
        Connection connection = dbCon.getCon();

        try {

            String usernameForGroups = (userId.equals("all")) ? "" : " AND m.GROUP_ADMIN='" + userId + "'";

            String sqlForActiveGroup = "SELECT count(*) CNT FROM majlis_group m WHERE m.GROUP_STATUS = 3  " + usernameForGroups;

            ResultSet rsForActiveGroups = dbCon.search(connection, sqlForActiveGroup);

            while (rsForActiveGroups.next()) {

                countMap.put("group_active", rsForActiveGroups.getInt("CNT"));
            }

            String sqlForGroupRequest = "SELECT count(*) CNT FROM majlis_group m,tmp_group_request t WHERE m.GROUP_ID = t.GROUP_ID  " + usernameForGroups;

            ResultSet rsForGroupsRequest = dbCon.search(connection, sqlForGroupRequest);

            countMap.put("group_pending", 0);

            while (rsForGroupsRequest.next()) {
                countMap.put("group_pending", rsForGroupsRequest.getInt("CNT"));
            }

            String sqlForActiveEvents = "SELECT COUNT(*) CNT "
                    + "FROM majlis_group m, majlis_group_event e "
                    + "WHERE m.GROUP_ID = e.GROUP_ID "
                    + "AND (e.EVENT_DATE) >= CURDATE()  " + usernameForGroups;

            ResultSet rsForActiveEvents = dbCon.search(connection, sqlForActiveEvents);

            countMap.put("event_active", 0);

            while (rsForActiveEvents.next()) {
                countMap.put("event_active", rsForActiveEvents.getInt("CNT"));
            }

            String notiWhere = (userId.equals("all")) ? "" : " AND u.USER_ID = '" + userId + "' ";
            String notiAmountWhere = (userId.equals("all")) ? "-1" : " SELECT u.NO_ALLOWED_NOTIFICATIONS FROM majlis_cms_users u WHERE u.USER_ID = '" + userId + "' ";

            String sqlForNotification = "SELECT ( "
                    + "SELECT COUNT(*) "
                    + "FROM majlis_notification_send s, majlis_notification n, majlis_cms_users u "
                    + "WHERE s.NOTIFICATION_ID = n.NOTIFICATION_ID "
                    + "AND u.USER_ID = n.NOTIFICATION_USER_ID "
                    + " " + notiWhere + " ) R_NOTIF, "
                    + "( " + notiAmountWhere + " ) R_CNT";

            ResultSet rsForNotification = dbCon.search(connection, sqlForNotification);

            countMap.put("tot_notification", 0);
            countMap.put("tot_notification_limit", 0);

            while (rsForNotification.next()) {
                countMap.put("tot_notification", rsForNotification.getInt("R_NOTIF"));
                countMap.put("tot_notification_limit", rsForNotification.getInt("R_CNT"));
            }

            result = 1;
            responseData.setResponseData(countMap);
        } catch (Exception ex) {
            result = 999;
            ex.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                logger.error("error in get count method " + ex.getMessage());
            }
        }

        responseData.setResponseCode(result);
        return responseData;
    }
//========================

    public ResponseData getAllMobileGroupsCMS(int groupId, String userId) {

        ResponseData rd = new ResponseData();
        int result = -1;
        try {

            ORMConnection conn = new ORMConnection();

            MajlisGroup majlisGroup = new MajlisGroup();
            List<MajlisGroup> majlisGroupList = new ArrayList();

            String where = "";

            System.out.println("Group user Id " + userId);

            where += (userId.equals("all")) ? " m.groupAdmin.userId = m.groupAdmin.userId" : "  m.groupAdmin.userId ='" + userId + "'";
            where += (groupId== 0) ? " m.groupId = m.groupId" : "  m.groupId ='" + groupId + "'";

            String hql = "SELECT m FROM MajlisGroup m WHERE " + where;

            System.out.println("HQL " + hql);

            List<Object> resultList = conn.hqlGetResults(hql);

            for (Object obj : resultList) {
                majlisGroup = (MajlisGroup) obj;
                majlisGroup.setGroupIconPath(validateGroupIconPath(majlisGroup.getGroupIconPath()));
                majlisGroupList.add(majlisGroup);
            }

            result = 1;
            rd.setResponseData(resultList);

        } catch (Exception e) {
            
            result = 999;
            e.printStackTrace();

        }

        rd.setResponseCode(result);
        
        return rd;
    }

    public ResponseData getTopGroups() {

        ResponseData responseData = new ResponseData();
        int result = -1;
        List<Object> list = new ArrayList<Object>();

        DbCon dbCon = new DbCon();
        Connection connection = dbCon.getCon();

        try {

            String sql = "SELECT mg.GROUP_TITLE,count(mmug.USER_ID) as count FROM majlis_mobile_user_group mmug,"
                    + "majlis_group mg WHERE mg.GROUP_ID=mmug.GROUP_ID GROUP BY mmug.GROUP_ID ORDER BY count DESC LIMIT 5";

            ResultSet rs = dbCon.search(connection, sql);

            while (rs.next()) {
                GroupCount groupCount = new GroupCount(rs.getString(1), rs.getInt(2));

                list.add(groupCount);

            }

            result = 1;
            responseData.setResponseData(list);
        } catch (Exception ex) {
            result = 999;
            ex.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                logger.error("Error in get top group " + ex.getMessage());
            }
        }

        responseData.setResponseCode(result);
        return responseData;
    }

    public List<MajlisGroup> getAllPrivateGroupDetails() {
        System.out.println("getAllPrivateGroupDetails method call.................");

        Session session = null;
        int code = 0;

        ORMConnection conn = new ORMConnection();

        List<MajlisGroup> privateGroupList = new ArrayList<>();

        try {
            String selectMdCode = "SELECT m.codeId FROM MajlisMdCode m WHERE m.codeMessage= 'None'";
            List<Object> li = conn.hqlGetResults(selectMdCode);

            for (Object obj : li) {
                code = (int) obj;
                System.out.println("code" + code);

            }

            if (code != 0) {

                String selectPrivateGroups = "SELECT g FROM MajlisGroup g WHERE g.groupPublicLevel= '" + code + "'";
                List<Object> privateList = conn.hqlGetResults(selectPrivateGroups);

                for (Object object : privateList) {
                    MajlisGroup pGroup = (MajlisGroup) object;
                    privateGroupList.add(pGroup);
                }

            } else {
                privateGroupList = null;
            }

        } catch (Exception e) {
            logger.info("Error in getAllPrivateGroupDetails: " + e.getMessage());
            privateGroupList = null;
            e.printStackTrace();

        }
        return privateGroupList;
    }

    public ResponseData getAllGroupLevels(String country, String district, String city) {
        ResponseData rd = new ResponseData();
        DbCon db = new DbCon();

        int count = 0;

        Connection conn = db.getCon();

        try {

            String sql = "SELECT G.GROUP_ID,G.GROUP_SYSTEM_ID,G.GROUP_TITLE,G.GROUP_ICON_PATH,G.GROUP_DESCRIPTION,"
                    + "G.GROUP_COUNTRY,G.GROUP_DISTRICT,G.GROUP_CITY,G.GROUP_ADMIN,"
                    + "G.GROUP_STATUS,m.CODE_MESSAGE "
                    + "FROM majlis_group G , majlis_md_code m "
                    + "WHERE m.CODE_ID = G.GROUP_PUBLIC_LEVEL "
                    + "AND m.CODE_LOCALE = 'EN'"
                    + "AND m.CODE_TYPE = 'GROUPS' "
                    + "AND m.CODE_SUB_TYPE = 'GROUP_LEVELS'";

            List<EventGroup> allGroupLevelsList = new ArrayList<>();
            List<EventGroup> filteredList = new ArrayList<>();

            System.out.println("SQL Query " + sql);

            ResultSet rs = db.search(conn, sql);

            while (rs.next()) {
                count++;
                EventGroup group = new EventGroup();
                //allGroupLevels.put(rs.getInt("GROUP_ID"), rs.getString("CODE_MESSAGE"));
                group.setGroupId(rs.getInt("GROUP_ID"));
                group.setGroupSystemId(rs.getString("GROUP_SYSTEM_ID"));
                group.setTitle(rs.getString("GROUP_TITLE"));
                String path = com.dmssw.majlis.config.AppParams.MEDIA_SERVER + "/" + rs.getString("GROUP_ICON_PATH");
                group.setIcon(path);
                group.setDescription(rs.getString("GROUP_DESCRIPTION"));
                group.setCountry(rs.getString("GROUP_COUNTRY"));
                group.setDistrict(rs.getString("GROUP_DISTRICT"));
                group.setCity(rs.getString("GROUP_CITY"));
                group.setGroupAdmin(rs.getString("GROUP_ADMIN"));
                group.setStatus(rs.getInt("GROUP_STATUS"));
                group.setCodeMessage(rs.getString("CODE_MESSAGE"));

                allGroupLevelsList.add(group);
            }

            EventGroup g = new EventGroup();

            for (Object object : allGroupLevelsList) {
                g = (EventGroup) object;

                if ((g.getStatus() == 6) && (g.getCountry().equalsIgnoreCase(country))) {

                    if ((!district.equalsIgnoreCase("all")) && (g.getDistrict().equalsIgnoreCase(district))) {

                        if ((!city.equalsIgnoreCase("all")) && (g.getCity().equalsIgnoreCase(city))) {
                            filteredList.add(g);
                            System.out.println("adding to list");
                        }
                    }
                    if ((district.equalsIgnoreCase("all")) && (city.equalsIgnoreCase("all"))) {
                        filteredList.add(g);
                    }
                }

                if ((g.getStatus() == 7) && (g.getDistrict().equalsIgnoreCase(district))) {

                    if ((!city.equalsIgnoreCase("all")) && (g.getCity().equalsIgnoreCase(city))) {
                        filteredList.add(g);
                        System.out.println("adding to District");
                    }
                    if (city.equalsIgnoreCase("all")) {
                        filteredList.add(g);
                        System.out.println("adding to getCity");
                    }
                }

                if ((g.getStatus() == 8) && (g.getCity().equalsIgnoreCase(city))) {
                    filteredList.add(g);
                    System.out.println("adding to City");
                }

                if (g.getStatus() == 5) {
                    filteredList.add(g);
                    System.out.println("adding to Private");
                }
            }

            rd.setResponseData(filteredList);
            rd.setResponseCode(count);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return rd;
    }

    public String getGroupImage(int grpId) {

        String path = "Majlis_images\\GroupIcons\\" + grpId + "_GroupIcons.jpg";
        path = path.replace("\\", "/");
        path = com.dmssw.majlis.config.AppParams.MEDIA_SERVER + "/" + path;
        System.out.println("path " + path);

        return path;

    }

    public ResponseData updateGroupsForUser(EventUserGroup user) {

        DbCon db = new DbCon();

        Connection conn = db.getCon();

        try {

            transaction.begin();

            try {

                String deleteCurentRecord = "DELETE FROM majlis_mobile_user_group "
                        + "WHERE USER_ID='" + user.getUserId() + "' AND GROUP_ID = '" + user.getGroupId() + "'";

                PreparedStatement ps0 = db.prepare(conn, deleteCurentRecord);

                ps0.executeUpdate();

                if (user.isOperation()) {

                    String sqlForModifyGroups = "INSERT INTO majlis_mobile_user_group (USER_ID,GROUP_ID) VALUES(?,?)";

                    PreparedStatement ps = db.prepare(conn, sqlForModifyGroups);

                    ps.setInt(1, user.getUserId());
                    ps.setInt(2, user.getGroupId());

                    ps.executeUpdate();

                }

                transaction.commit();
            } catch (Exception e) {

                transaction.rollback();

                e.printStackTrace();
            }

        } catch (Exception ex) {
            logger.error("EJB exception " + ex.getMessage());
        }

        return null;
    }

    //===============================================================================================
    public ResponseData getGroupList(String userId, int start, int limit) {

        ResponseData rd = new ResponseData();
        int result = -1;
        try {

            ORMConnection conn = new ORMConnection();

            MajlisGroup majlisGroup = new MajlisGroup();
            List<MajlisGroup> majlisGroupList = new ArrayList();

            String where = "";

            System.out.println("Group user Id " + userId);

            where += (userId.equals("all")) ? "" : " WHERE m.groupAdmin.userId ='" + userId + "'";

            String hql = "SELECT m FROM MajlisGroup m " + where;

            System.out.println("HQL " + hql);

            List<Object> resultList = conn.hqlGetResultsWithLimit(hql, start, limit);

            for (Object obj : resultList) {
                majlisGroup = (MajlisGroup) obj;
                majlisGroup.setGroupIconPath(validateGroupIconPath(majlisGroup.getGroupIconPath()));
                majlisGroupList.add(majlisGroup);
            }

            result = 1;
            rd.setResponseData(resultList);
            
        } catch (Exception e) {
            result = 999;
            e.printStackTrace();

        }

        rd.setResponseCode(result);

        return rd;
    }

    /**
     * Insert a new 'majlis_group' database record
     *
     * @param majlisGroup
     * @return 1 if successful ,999 if fails.
     */
    public ResponseData createGroup(MajlisGroup majlisGroup, String userName) {

        ResponseData responseData = new ResponseData();

        DbCon db = new DbCon();

        Connection conn = db.getCon();

        int result = -1;

        int groupId = 0;
        int systemId = 0;
        System.out.println("Starting the method...");

        try {
            if ((majlisGroup.getGroupTitle() != null) && (majlisGroup.getGroupStatus() != null)
                    && (majlisGroup.getGroupPublicLevel() != null) && (majlisGroup.getExpiryDate() != null)) {

                try {

                    transaction.begin();

                    String sqlForInsertGroup = "INSERT INTO majlis_group ("
                            + "GROUP_TITLE, "
                            + "GROUP_ICON_PATH, "
                            + "GROUP_DESCRIPTION, "
                            + "GROUP_COUNTRY, "
                            + "GROUP_DISTRICT, "
                            + "GROUP_CITY, "
                            + "GROUP_ADMIN, "
                            + "GROUP_PUBLIC_LEVEL, "
                            + "GROUP_STATUS, "
                            + "USER_INSERTED, "
                            + "DATE_INSERTED, "
                            + "EXPIRY_DATE,GROUP_SYSTEM_ID)"
                            + " VALUES (?,?,?,?,?,?,?,?,?,?,NOW(),?,0)";

                    PreparedStatement pr = db.prepareAutoId(conn, sqlForInsertGroup);

                    pr.setString(1, majlisGroup.getGroupTitle());

                    String imgPath = "";

                    if (majlisGroup.getGroupIconPath() == null) {

                        System.out.println("Group icon is null....... ");

                        imgPath = com.dmssw.majlis.config.AppParams.IMG_PATH_GROUP + "/default.jpg";//Default Value if the group icon path is null.

                    } else {

                        String url = majlisGroup.getGroupIconPath();

                        System.out.println("Media server" + com.dmssw.majlis.config.AppParams.MEDIA_SERVER);

                        String[] arrayUrl = url.split(com.dmssw.majlis.config.AppParams.MEDIA_SERVER + "/");// Rempve media server path from he image url

                        imgPath = arrayUrl[1];

                    }

                    pr.setString(2, imgPath);
                    pr.setString(3, majlisGroup.getGroupDescription());
                    pr.setString(4, majlisGroup.getGroupCountry());
                    pr.setString(5, majlisGroup.getGroupDistrict());
                    pr.setString(6, majlisGroup.getGroupCity());
                    pr.setString(7, majlisGroup.getGroupAdmin().getUserId());
                    pr.setInt(8, majlisGroup.getGroupPublicLevel().getCodeId());
                    pr.setInt(9, majlisGroup.getGroupStatus().getCodeId());

                    java.sql.Date date = new java.sql.Date(majlisGroup.getExpiryDate().getTime());

                    pr.setString(10, userName);
                    pr.setDate(11, date);

                    pr.executeUpdate();

                    ResultSet gen = pr.getGeneratedKeys();

                    while (gen.next()) {
                        groupId = gen.getInt(1);
                    }

                    systemId = generateSystemId(groupId);

                    String sqlForUpdate = "UPDATE majlis_group SET GROUP_SYSTEM_ID = '" + systemId + "' WHERE GROUP_ID='" + groupId + "'";

                    db.save(conn, sqlForUpdate);

                    transaction.commit();

                    result = 1;
                } catch (Exception ex) {

                    ex.printStackTrace();

                    result = 999;

                } finally {
                    conn.close();
                }

            } else {
                result = 999;

                logger.info("Null entries ");
            }

        } catch (Exception ex) {
            logger.info("Exception");
            ex.printStackTrace();
        }
        responseData.setResponseCode(result);
        return responseData;

    }

    /**
     * Update an existing 'majlis_group' database record with hibernate
     *
     * @param majlisGroup
     * @return 1 if successful ,999 if fails.
     */
    public ResponseData updateGroup(MajlisGroup majlisGroup) {

        ResponseData responseData = new ResponseData();
        ORMConnection conn = new ORMConnection();
        Session session = conn.beginTransaction();

        int result = -1;
        int groupId = majlisGroup.getGroupId();

        try {
            if (groupId != 0 && (majlisGroup.getGroupTitle() != null) && (majlisGroup.getGroupStatus() != null)
                    && (majlisGroup.getGroupPublicLevel()) != null) {
                majlisGroup.setGroupIconPath(majlisGroup.getGroupIconPath().replace(com.dmssw.majlis.config.AppParams.MEDIA_SERVER, ""));
                try {
                    conn.updateObject(session, majlisGroup);

                    responseData.setResponseData(majlisGroup);// fro testing
                    conn.commitObject(session);
                    result = 1;
                } catch (Exception ex) {
                    logger.info("Exception ...!");
                    ex.printStackTrace();
                }
            } else {
                result = 999;
                logger.info("Null entries!");
            }
        } catch (Exception ex) {
            result = 999;
            logger.info("Exception Occurs.. !");
            ex.printStackTrace();
        }
        responseData.setResponseCode(result);
        return responseData;
    }

    /**
     * Retrieves details of a group specified by an ID
     *
     * @param groupId
     * @return
     */
    public ResponseData getGroupDetail(int groupId) {
        ResponseData responseData = new ResponseData();
        MajlisGroup majlisGroup = new MajlisGroup();
        List<MajlisGroup> majlisGroupList = new ArrayList<MajlisGroup>();
        int result = -1;

        try {
            ORMConnection conn = new ORMConnection();

            String hql = "SELECT m FROM MajlisGroup m WHERE m.groupId = " + groupId;
            List<Object> resultList;

            resultList = conn.hqlGetResults(hql);

            for (Object object : resultList) {
                majlisGroup = (MajlisGroup) object;
                majlisGroup.setGroupIconPath(validateGroupIconPath(majlisGroup.getGroupIconPath()));
                majlisGroupList.add(majlisGroup);
            }

            result = 1;
            responseData.setResponseData(majlisGroupList);

        } catch (Exception e) {
            result = 999;
            e.printStackTrace();

        }
        responseData.setResponseCode(result);
        return responseData;

    }

    /**
     * This method creates a new system ID for a new group
     *
     * @param groupId
     * @return the new system Id
     */
    private int generateSystemId(int groupId) {
        int systemId = 0;
        Date currentDate = new Date();

        Calendar cal = Calendar.getInstance();
        cal.setTime(currentDate);
        String year = String.valueOf(cal.get(Calendar.YEAR));
        String month = String.valueOf(cal.get(Calendar.MONTH) + 1);
        systemId = Integer.parseInt(year + month + groupId);
        return systemId;

    }

    /**
     * This method converts base64 string to the icon image and set actual URL.
     * iVBORw0KGgoAAAANSUhEUgAAAHkAAAB5CAMAAAAqJH57AAAAV1BMVEX///9mZmZjY2NfX19bW1tXV1d5eXmVlZWenp7Z2dltbW34+Pjx8fHHx8fp6ens7Oy7u7uysrKkpKSDg4Pi4uJycnKJiYnR0dGqqqqPj4/BwcFSUlJNTU2CWPSYAAADtklEQVRoge2b65aqMAyFaVLKXUCu6rz/cx5AZwTUsZF96jpzZi9/svhW2pAmafS8X/2rSqOsD2rn2CQKG9KsO9fgzPhMSil2TK6NHrGDKHIK7k8X7kB2uc9Jq9VVO3fg3PCVS03yHrCi1hk4qeZgpXtn5MMCrLQz1+71AkzG1TZHvlqaXDgCJ4pW5NQRebXW7vyrXlmslCuTQ16ZfHQETuMl2F0UOa6+KOUscAaLbSZ2FkRWi82ZK7BXL/yLXXnXoM6fL7U7iz2vuDoYx05zoC8ycesqgszJxNq4TnSLD619Pw7dJpujkizLur+c7yV5XUdR/WQz07orykHHrM4h1LpsTUyKiOIq7B68M+36KqZhz3n8DU/uo41OtysNa6IvJ9bqUNwscF4EsWaax9LhyabfkPPvAub1ETwUbdUcnmYt65tz+gwPX/WCnvjOG8/w8rzsUaDuYi8BhsqXDG70w1cOBp1Mfwzj02PsJF3Jtzv7xpYL/HYr7jwlri3L+wstl/TYLr9ZaSlaZHWBsnhCx/YuHuEsntCVbYKWIi0e9WH7cVXPXVYi0rbgArvW3Nhuc45da32w5N6WLNvEe2vwbZW2CRxag70AaTK39gXPDsgdYoggOYHF61EnSVLaALeZAwE4QprsSxLBHkgWmewZ4GKLmnIpEEyNxGTk8SiIXh42I5BdK+yBZBKVOC1wn2MJ2ItxZKpEZP/5G20l+5qhZJFr/4/k5GfY/DYPe99XpdTbyCRqVQDBwq4zsqIikYsF0DRfcrkAzXm1ZLkzaK/ACMg1tID1BSlgigTLggky6x2MFtyqQCtJUTQ5gsn2F+HYul10YiFryQlt3TTAdkkk6A5NVtryMiuPn79LKG7s6hxkmXER6b2Ni2fYDuBZrMvnS56ivfusk8WgCdy7J9lEs/VYDAZsM/yQYE+Ns+xu5MFd5kmx1TeNPaQn2QYyZM/iLNuxxBS93PYJMNpo+4QMbDTZXyiAjZa0IVNgk0hYSSO/aV92/42r7Vg4oQdLBUnWC/Rw98Dyee4Ek5xILqw+tUN8WWReGStB5EUvTuhtL7Ks73/X2pqSCbtxM+W0CS27Q1mq24ImtWVkakPjhDb+S+FxQU2s9WpCagneOit3F03MTbUvjseyNeruEA8BpiG79QDRNPt1HTzbdftxOmz5DDeIP4SkZsYm7Zvy5tjblcafP+QHoOHirBk3dBy2o0P2IBzmRUXTvhP7BjgOGfVt07Rl9K0pSVSEh8OhR49hJu7+1vKrn6M/XKoyZiVvmakAAAAASUVORK5CYII=
     *
     * @param base64Img
     * @param groupId
     * @return the path of the icon location
     */
    private String convertIconPath(String base64Img, int groupId) {
        String iconPath = null;

        if ((base64Img != null) && (base64Img.length() > 0)) {

            try {

                byte[] decodedBytes = Base64.getMimeDecoder().decode(base64Img);
                BufferedImage bufferedImage = ImageIO.read(new ByteArrayInputStream(decodedBytes));

                if (bufferedImage != null) {

                    String path = "Majlis_images/group/" + groupId + "_GroupIcons" + ".jpg";
                    iconPath = "/" + path;
                    path = path.replace("\\", "/");

                    File f = new File(com.dmssw.majlis.config.AppParams.IMG_PATH + path);
                    ImageIO.write(bufferedImage, "png", f);

                } else {
                    // LOGGER.info("NO image to write");

                }

            } catch (IOException e) {

                //LOGGER.info("Error in Streaming...");
                e.printStackTrace();
            }

        } else {
            //LOGGER.info("Invalid Image...");
        }

        return iconPath;

    }

    /**
     * Returns the absolute path to the group icon.
     *
     * @param path- the relative path recorded in the database
     * @return
     */
    private String validateGroupIconPath(String path) {

        String ipath = "";
        if (path != null) {

            return com.dmssw.majlis.config.AppParams.MEDIA_SERVER + "/" + path;

        }
        return ipath;
    }

    public ResponseData getStatus(String type, String subType) {

        ResponseData rd = new ResponseData();

        DbCon db = new DbCon();

        Connection conn = db.getCon();

        ORMConnection orm = new ORMConnection();

        List<MajlisMdCode> mdList = new ArrayList<>();

        int result = -1;

        try {

            String sql = "SELECT U FROM MajlisMdCode U WHERE U.codeType= '" + type + "' AND U.codeSubType='" + subType + "' AND U.codeLocale='" + AppParams.LOCALE + "' ORDER BY CODE_SEQ ASC";

            System.out.println("sql.." + sql);

            List<Object> resultSet = orm.hqlGetResults(sql);

            for (Object object : resultSet) {
                MajlisMdCode md = (MajlisMdCode) object;

                mdList.add(md);

            }
            result = 1;
            rd.setResponseData(mdList);

        } catch (Exception e) {
            e.printStackTrace();
            result = 999;

        }

        rd.setResponseCode(result);

        return rd;
    }

    /**
     * Modify a group
     *
     * @param majlisGroup
     * @param userName
     * @return
     */
    public ResponseData modifyGroup(String userId, MajlisGroup majlisGroup, String userName) {

        ResponseData responseData = new ResponseData();

        DbCon db = new DbCon();

        Connection conn = db.getCon();

        int result = -1;

        try {
            if ((majlisGroup.getGroupSystemId() != 0) && (majlisGroup.getGroupId() != null) && (majlisGroup.getGroupTitle() != null) && (majlisGroup.getGroupStatus() != null)
                    && (majlisGroup.getGroupPublicLevel() != null) && (majlisGroup.getExpiryDate() != null)) {

                try {

                    transaction.begin();

                    String sqlForUpdateGroup = "UPDATE majlis_group SET GROUP_TITLE = ?,GROUP_ICON_PATH = ?, "
                            + "GROUP_DESCRIPTION = ?, "
                            + "GROUP_COUNTRY = ?, "
                            + "GROUP_DISTRICT = ?, "
                            + "GROUP_CITY = ?, "
                            + "GROUP_ADMIN = ?, "
                            + "GROUP_PUBLIC_LEVEL = ?, "
                            + "GROUP_STATUS = ?, "
                            + "USER_MODIFIED = ?, "
                            + "DATE_MODIFIED = NOW(), "
                            + "EXPIRY_DATE = ?"
                            + "WHERE GROUP_ID = " + majlisGroup.getGroupId();

                    PreparedStatement pr = db.prepare(conn, sqlForUpdateGroup);

                    pr.setString(1, majlisGroup.getGroupTitle());

                    String imgPath = "";

                    if (majlisGroup.getGroupIconPath() == null) {

                        majlisGroup.setGroupIconPath("/img_path");//Default Value if the group icon path is null.
                    } else {

                        String url = majlisGroup.getGroupIconPath();

                        String[] arrayUrl = url.split(com.dmssw.majlis.config.AppParams.MEDIA_SERVER + "/");// Rempve media server path from he image url

                        imgPath = arrayUrl[1];

                    }

                    pr.setString(2, imgPath);
                    pr.setString(3, majlisGroup.getGroupDescription());
                    pr.setString(4, majlisGroup.getGroupCountry());
                    pr.setString(5, majlisGroup.getGroupDistrict());
                    pr.setString(6, majlisGroup.getGroupCity());
                    if (majlisGroup.getGroupAdmin() == null) {
                        pr.setString(7, userId);
                    } else {
                        pr.setString(7, majlisGroup.getGroupAdmin().getUserId());
                    }
                    pr.setInt(8, majlisGroup.getGroupPublicLevel().getCodeId());
                    pr.setInt(9, majlisGroup.getGroupStatus().getCodeId());

                    java.sql.Date sqlDate = new java.sql.Date(majlisGroup.getExpiryDate().getTime());

                    pr.setString(10, userName);
                    pr.setDate(11, sqlDate);

                    pr.executeUpdate();

                    transaction.commit();

                    result = 1;
                } catch (Exception ex) {

                    ex.printStackTrace();

                    result = 999;

                }

            } else {
                result = 2;

                logger.info("Null entries ");
            }

        } catch (Exception ex) {
            logger.info("Exception");
            ex.printStackTrace();
        }
        responseData.setResponseCode(result);
        return responseData;
    }

    /**
     * Upload a group icon to the server.
     *
     * @param input
     * @return the path url to the image
     */
    public ResponseData saveGroupIcon(MultipartFormDataInput input) {

        int result = -1;
        ResponseData rd = new ResponseData();

        Map<String, List<InputPart>> uploadForm = input.getFormDataMap();

        try {

            logger.info("start saveEventVideo method............");

            transaction.begin();

            List<InputPart> inputParts = uploadForm.get("file");

            for (InputPart inputPart : inputParts) {

                MultivaluedMap<String, String> headers = inputPart.getHeaders();
                try {

                    java.io.InputStream inputStream = inputPart.getBody(java.io.InputStream.class, null);
                    byte[] bytes = IOUtils.toByteArray(inputStream);
                    String saveLocation = com.dmssw.majlis.config.AppParams.IMG_PATH;

                    Random rnd = new Random();

                    int randomId = 100000 + rnd.nextInt(900000);

                    String pathForDb = com.dmssw.majlis.config.AppParams.IMG_PATH_GROUP + "/" + randomId + ".jpg";

                    File dir = new File(saveLocation + com.dmssw.majlis.config.AppParams.IMG_PATH_GROUP);// Creating the directory if the directory is not created.

                    dir.mkdir();
                    logger.info("image path is " + saveLocation);

                    FileOutputStream stream = new FileOutputStream(saveLocation + pathForDb);

                    try {
                        stream.write(bytes);
                    } finally {
                        stream.close();
                    }

                    result = 1;
                    rd.setResponseData(com.dmssw.majlis.config.AppParams.MEDIA_SERVER + pathForDb);
                    transaction.commit();
                } catch (IOException e) {
                    transaction.rollback();
                    result = 999;
                    logger.error("Cannot save the video " + e.getMessage());

                }
            }

        } catch (Exception ex) {

            logger.error("EJB Exception " + ex.getMessage());

        }
        rd.setResponseCode(result);

        return rd;

    }

}
