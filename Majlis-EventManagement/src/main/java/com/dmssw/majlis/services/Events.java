/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dmssw.majlis.services;

import com.dmssw.majlis.controllers.EventController;
import com.dmssw.majlis.models.DeleteEventData;
import com.dmssw.majlis.models.Event;
import com.dmssw.majlis.models.EventFeedback;
import com.dmssw.majlis.models.RSVPStatus;
import com.dmssw.orm.models.MajlisEventComment;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

/**
 *
 * @author Danushka
 */
@Path("/events")
public class Events {

    @EJB
    EventController controller;

    @GET
    @Path("/getEventDetails")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getEventDetails(
            @QueryParam("eventId") @DefaultValue("0") int eventId,
            @QueryParam("start") @DefaultValue("0") int start,
            @QueryParam("limit") @DefaultValue("25") int limit) {

        System.out.println("getEventDetails............");

        return Response.status(Response.Status.OK).entity(controller.getEvents(eventId, start, limit)).build();
    }

    @GET
    @Path("/getEventLocationsForMap")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getEventLocationsForMap(
            @QueryParam("userId") @DefaultValue("all") String userId) {

        return Response.status(Response.Status.OK).entity(controller.getEventLocationsForMap(userId)).build();
    }

    @POST
    @Path("/deleteEventData")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteEventData(DeleteEventData event) {
        return Response.status(Response.Status.OK).entity(controller.deleteEventData(event)).build();
    }

    @GET
    @Path("/getEventComments")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getEventComments(
            @QueryParam("eventId") @DefaultValue("0") int eventId) {

        return Response.status(Response.Status.OK).entity(controller.getEventComments(eventId)).build();
    }
    
    
    @GET
    @Path("/getEventParticipants")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getEventParticipants(
            @QueryParam("eventId") @DefaultValue("0") int eventId) {
        
        return Response.status(Response.Status.OK).entity(controller.getEventParticipants(eventId)).build();
    }

    @POST
    @Path("/createEvent")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public Response createEvent(Event majlisGroupEvent, @HeaderParam("userId") String userId) {

        System.out.println("Starting Creating>>>");
        return Response.status(Response.Status.OK).entity(controller.createEvent(majlisGroupEvent, userId)).build();

    }

//    @GET
//    @Path("/getEventListNotificaionByID")
//    @Produces({MediaType.APPLICATION_JSON})
//    public Response createEvent(){
//     
//        return Response.status(Response.Status.OK).entity(controller.getEventListNotificaion()).build();
//    }
    @GET
    @Path("/getComments")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getComments(
            @QueryParam("eventId") @DefaultValue("0") int eventId, @QueryParam("start") @DefaultValue("0") int start,
            @QueryParam("limit") @DefaultValue("25") int limit) {

        return Response.status(Response.Status.OK).entity(controller.getCommentDetails(eventId, start, limit)).build();
    }

    @GET
    @Path("/getEventList")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getEventList(
            @QueryParam("eventId") @DefaultValue("0") int eventId, @QueryParam("start") @DefaultValue("0") int start,
            @QueryParam("limit") @DefaultValue("25") int limit) {

        return Response.status(Response.Status.OK).entity(controller.getEventList(eventId, start, limit)).build();
    }

    @GET
    @Path("/getEventStatus")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getEventStatus(
            // @QueryParam("eventId") @DefaultValue("0") int eventId,
            @QueryParam("userId") @DefaultValue("0") int userId,
            @QueryParam("eventId") @DefaultValue("0") int eventId
    ) {

        return Response.status(Response.Status.OK).entity(controller.getEventStatus(userId, eventId)).build();
    }

//    @GET
//    @Path("/updateLikes")
//    public Response updateLikes(){
//    
//        return Response.status(Response.Status.OK).entity(controller.updateLikes()).build();
//    }
//    public Response geteventStatus(){
//         
//       return Response.status(Response.Status.OK).entity(controller.geteventStatus()).build();
//    }
    @POST
    @Path("/updateLikes")
    @Produces({MediaType.APPLICATION_JSON})
    public Response updateLikes(@HeaderParam("commentId") @DefaultValue("0") int commentId) {

        return Response.status(Response.Status.OK).entity(controller.updateLikes(commentId)).build();
    }

    /////////////////////////////Sandali ////////////////////////////////////////////////////////////////////////////////////
    @GET
    @Path("/getUserEvents")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUserEvents(@QueryParam("userId") @DefaultValue("0") int userId,
            @QueryParam("search") @DefaultValue("all") String search,
            @QueryParam("eventId") @DefaultValue("0") int eventId,
            @QueryParam("start") @DefaultValue("0") int start,
            @QueryParam("limit") @DefaultValue("25") int limit) {

        return Response.status(Response.Status.OK).entity(controller.getUserEvents(userId, search, eventId, start, limit)).build();
    }

    @POST
    @Path("/updateRsvpStatus")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateRsvpStatus(RSVPStatus status) {

        EventController controller = new EventController();
        return Response.status(Response.Status.OK).entity(controller.updateRsvpStatus(status)).build();
    }

    @POST
    @Path("/updateEventDetails")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateRsvpStatus(@HeaderParam("userId") String userId, Event event) {

        EventController controller = new EventController();
        return Response.status(Response.Status.OK).entity(controller.updateEventDetails(userId, event)).build();
    }

    @GET
    @Path("/getStatus")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getStatus(@QueryParam("eventId") @DefaultValue("0") int eventId) {

        EventController controller = new EventController();
        return Response.status(Response.Status.OK).entity(controller.getStatus(eventId)).build();
    }

    @POST
    @Path("/updateEventFeedback")
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateEventFeedback(EventFeedback feedback) {

        EventController controller = new EventController();
        return Response.status(Response.Status.OK).entity(controller.updateEventFeedback(feedback)).build();
    }

    @GET
    @Path("/getEventFeedback")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getEventFeedback(@QueryParam("eventId") @DefaultValue("0") int eventId,
            @QueryParam("userId") @DefaultValue("0") int userId) {

        EventController controller = new EventController();
        return Response.status(Response.Status.OK).entity(controller.getEventFeedback(eventId, userId)).build();
    }

    @POST
    @Path("/postComments")
    @Produces(MediaType.APPLICATION_JSON)
    public Response postComments(MajlisEventComment comment) {

        EventController controller = new EventController();
        return Response.status(Response.Status.OK).entity(controller.postComments(comment)).build();
    }
    
    @GET
    @Path("/getEventVideos")
    public Response getEventVideos(@QueryParam("eventId") @DefaultValue("0") int eventId){
        
        
        return Response.status(Response.Status.OK).entity(controller.getEventVideos(eventId)).build();
    }

    @POST
    @Path("/saveEventVideo")
    @Consumes(MediaType.MEDIA_TYPE_WILDCARD)
    @Produces(MediaType.TEXT_PLAIN)
    public Response saveEventVideos(MultipartFormDataInput input, @Context HttpHeaders headers) {

        headers.getRequestHeaders().forEach((key, val) -> {

            for (String string : val) {
                System.out.println("key " + key + " val... " + val);
            }

        });

        String eventId = headers.getHeaderString("eventId");
        String userId = headers.getHeaderString("userId");

        return Response.status(Status.OK).entity(controller.saveEventVideos(input, Integer.parseInt(eventId), Integer.parseInt(userId))).build();
    }

    @POST
    @Path("/saveEventImages")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response saveEventImages(com.dmssw.majlis.models.EventImage eventImage) {

        return Response.status(Status.OK).entity(controller.saveEventImages(eventImage)).build();

    }

    @GET
    @Path("/getEventImages")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getEventImageList(
            @QueryParam("eventId") @DefaultValue("0") int eventId) {

        return Response.status(Status.OK).entity(controller.getEventImageList(eventId)).build();
    }

    @GET
    @Path("/getEventImagesCMS")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getEventImagesCMS(
            @QueryParam("eventId") @DefaultValue("0") int eventId) {

        return Response.status(Status.OK).entity(controller.getEventImagesCMS(eventId)).build();
    }

    @GET
    @Path("/getEventVideosCMS")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getEventVideoList(
            @QueryParam("eventId") @DefaultValue("0") int eventId) {

        return Response.status(Status.OK).entity(controller.getEventVideoList(eventId)).build();
    }

    @GET
    @Path("/getStatus")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getGroupDetail(@QueryParam("type") String type) {

        return Response.status(Response.Status.OK).entity(controller.getStatus(type)).build();
    }

    @GET
    @Path("/getPublicLevels")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getPublicLevels(@QueryParam("subType") String subType) {

        System.out.println("Starting public levels");
        return Response.status(Response.Status.OK).entity(controller.getPublicLevels(subType)).build();
    }

    @GET
    @Path("/getEventsListForDetails")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getEventsListForDetails(@QueryParam("eventId") @DefaultValue("0") int eventId, @QueryParam("start") @DefaultValue("0") int start, @QueryParam("limit") @DefaultValue("25") int limit) {

        return Response.status(Response.Status.OK).entity(controller.getEventsListForDetails(eventId, start, limit)).build();
    }

    @GET
    @Path("/getEventsList")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getEventsList(@QueryParam("userId") @DefaultValue("all") String userId, @QueryParam("start") @DefaultValue("0") int start, @QueryParam("limit") @DefaultValue("25") int limit) {

        return Response.status(Response.Status.OK).entity(controller.getEventList(userId, start, limit)).build();
    }

    @GET
    @Path("/getEventsByGroupId")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getEventsByGroupId(@QueryParam("groupId") @DefaultValue("0") int groupId) {

        return Response.status(Response.Status.OK).entity(controller.getAllEventsByGroupId(groupId)).build();
    }

    @GET
    @Path("/getCount")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getCount() {

        return Response.status(Response.Status.OK).entity(controller.getCount()).build();
    }

    @POST
    @Path("/saveEventIcon")
    @Consumes(MediaType.MEDIA_TYPE_WILDCARD)
    @Produces(MediaType.APPLICATION_JSON)
    public Response saveEventIcon(MultipartFormDataInput input, @Context HttpHeaders headers) {

        headers.getRequestHeaders().forEach((key, val) -> {

            for (String string : val) {
                System.out.println("key " + key + " val... " + val);
            }

        });

        return Response.status(Response.Status.OK).entity(controller.saveEventIcon(input)).build();
    }

}
