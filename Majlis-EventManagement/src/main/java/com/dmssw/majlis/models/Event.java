/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dmssw.majlis.models;

import com.dmssw.orm.models.MajlisCategory;
import com.dmssw.orm.models.MajlisGroup;
import com.dmssw.orm.models.MajlisMdCode;
import java.util.Date;
import java.sql.Time;
/**
 * <p>
 * <b>Title</b> Event
 * </p>
 * <p>
 * <b>Description</b> Enter Purpose of this class
 * </p>
 * <p>
 * <b>Company</b> Data Management Systems (PVT) Ltd
 * </p>
 * <p>
 * <b>Copyright</b> Copyright (c) 2015
 * </p>
 *
 * @author Hashan Jayakody
 * @version 1.0
 */
public class Event {

    private int eventId;
    private String eventSystemId;
    private String userInserted;
    private Date dateInserted;
    private String userModified;
    private Date dateModified;
    private String eventCaption;
    private String eventIconPath;
    private String eventMessage;
    private String eventLocation;
    private String eventLocationLat;
    private String eventLocationLont;
    private String eventTime;
    private String eventDate;
    private int manualCount;
    private MajlisCategory eventCategory;
    private MajlisMdCode eventStatus;
    private MajlisGroup groupId;

    public Event() {
    }

   

    public Event(int eventId, String eventSystemId, String userInserted, Date dateInserted, String userModified,
            Date dateModified, String eventCaption, String eventIconPath, String eventMessage, String eventLocation, 
            String eventLocationLat, String eventLocationLont, String eventTime, String eventDate, int manualCount, MajlisCategory eventCategory, MajlisMdCode eventStatus, MajlisGroup groupId) {
        this.eventId = eventId;
        this.eventSystemId = eventSystemId;
        this.userInserted = userInserted;
        this.dateInserted = dateInserted;
        this.userModified = userModified;
        this.dateModified = dateModified;
        this.eventCaption = eventCaption;
        this.eventIconPath = eventIconPath;
        this.eventMessage = eventMessage;
        this.eventLocation = eventLocation;
        this.eventLocationLat = eventLocationLat;
        this.eventLocationLont = eventLocationLont;
        this.eventTime = eventTime;
        this.eventDate = eventDate;
        this.manualCount = manualCount;
        this.eventCategory = eventCategory;
        this.eventStatus = eventStatus;
        this.groupId = groupId;
    }
    

    public String getEventCaption() {
        return eventCaption;
    }

    public void setEventCaption(String eventCaption) {
        this.eventCaption = eventCaption;
    }

    public String getEventIconPath() {
        return eventIconPath;
    }

    public void setEventIconPath(String eventIconPath) {
        this.eventIconPath = eventIconPath;
    }

    public String getEventMessage() {
        return eventMessage;
    }

    public void setEventMessage(String eventMessage) {
        this.eventMessage = eventMessage;
    }

    public String getEventLocation() {
        return eventLocation;
    }

    public void setEventLocation(String eventLocation) {
        this.eventLocation = eventLocation;
    }

    public String getEventLocationLat() {
        return eventLocationLat;
    }

    public void setEventLocationLat(String eventLocationLat) {
        this.eventLocationLat = eventLocationLat;
    }

    public String getEventLocationLont() {
        return eventLocationLont;
    }

    public void setEventLocationLont(String eventLocationLont) {
        this.eventLocationLont = eventLocationLont;
    }

    public String getEventTime() {
        return eventTime;
    }

    public void setEventTime(String eventTime) {
        this.eventTime = eventTime;
    }

    public String getEventDate() {
        return eventDate;
    }

    public void setEventDate(String eventDate) {
        this.eventDate =  eventDate;
    }

    public int getManualCount() {
        return manualCount;
    }

    public void setManualCount(int manualCount) {
        this.manualCount = manualCount;
    }

    public MajlisCategory getEventCategory() {
        return eventCategory;
    }

    public void setEventCategory(MajlisCategory eventCategory) {
        this.eventCategory = eventCategory;
    }

    public MajlisMdCode getEventStatus() {
        return eventStatus;
    }

    public void setEventStatus(MajlisMdCode eventStatus) {
        this.eventStatus = eventStatus;
    }

    public MajlisGroup getGroupId() {
        return groupId;
    }

    public void setGroupId(MajlisGroup groupId) {
        this.groupId = groupId;
    }

    public int getEventId() {
        return eventId;
    }

    public void setEventId(int eventId) {
        this.eventId = eventId;
    }

    public String getEventSystemId() {
        return eventSystemId;
    }

    public void setEventSystemId(String eventSystemId) {
        this.eventSystemId = eventSystemId;
    }

    public String getUserInserted() {
        return userInserted;
    }

    public void setUserInserted(String userInserted) {
        this.userInserted = userInserted;
    }

    public Date getDateInserted() {
        return dateInserted;
    }

    public void setDateInserted(Date dateInserted) {
        this.dateInserted = dateInserted;
    }

    public String getUserModified() {
        return userModified;
    }

    public void setUserModified(String userModified) {
        this.userModified = userModified;
    }

    public Date getDateModified() {
        return dateModified;
    }

    public void setDateModified(Date dateModified) {
        this.dateModified = dateModified;
    }

    
}
