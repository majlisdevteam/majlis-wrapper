/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dmssw.majlis.models;

import com.dmssw.orm.models.MajlisCategory;
import com.dmssw.orm.models.MajlisGroup;
import com.dmssw.orm.models.MajlisGroupEventStatus;
import java.io.Serializable;
import java.sql.Time;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Shanka
 */

public class MajlisGroupEvent implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private Integer eventId;
   
    private String eventSystemId;
   
    private String eventCaption;
   
    private String eventIconPath;
    
    private Time eventTimeString;
    
    private Date eventDate;
   
    private String eventMessage;
   
    private String eventLocation;
    
    private String eventLocationLat;
   
    private String eventLocationLont;
   
    private Date eventTime;
   
    private String userInserted;
   
    private Date dateInserted;
    
    private Integer manualCount;
   
    private String userModified;
    
    private Date dateModified;
   
    private MajlisCategory eventCategory;
    
  
   
    private MajlisGroup groupId;
   
   
    private List<MajlisGroupEventStatus> majlisGroupEventStatusList;
    
    

    public MajlisGroupEvent() {
    }

    public MajlisGroupEvent(Integer eventId) {
        this.eventId = eventId;
    }

    public MajlisGroupEvent(Integer eventId, String eventCaption, String eventIconPath, String eventMessage, String eventLocation, String eventLocationLat, String eventLocationLont, Date eventTime) {
        this.eventId = eventId;
        this.eventCaption = eventCaption;
        this.eventIconPath = eventIconPath;
        this.eventMessage = eventMessage;
        this.eventLocation = eventLocation;
        this.eventLocationLat = eventLocationLat;
        this.eventLocationLont = eventLocationLont;
        this.eventTime = eventTime;
    }

    public Time getEventTimeString() {
        return eventTimeString;
    }

    public void setEventTimeString(Time eventTimeString) {
        this.eventTimeString = eventTimeString;
    }

    public Date getEventDate() {
        return eventDate;
    }

    public void setEventDate(Date eventDate) {
        this.eventDate = eventDate;
    }
    
    

    public Integer getEventId() {
        return eventId;
    }

    public void setEventId(Integer eventId) {
        this.eventId = eventId;
    }

    public String getEventSystemId() {
        return eventSystemId;
    }

    public void setEventSystemId(String eventSystemId) {
        this.eventSystemId = eventSystemId;
    }

    public String getEventCaption() {
        return eventCaption;
    }

    public void setEventCaption(String eventCaption) {
        this.eventCaption = eventCaption;
    }

    public String getEventIconPath() {
        return eventIconPath;
    }

    public void setEventIconPath(String eventIconPath) {
        this.eventIconPath = eventIconPath;
    }

    public String getEventMessage() {
        return eventMessage;
    }

    public void setEventMessage(String eventMessage) {
        this.eventMessage = eventMessage;
    }

    public String getEventLocation() {
        return eventLocation;
    }

    public void setEventLocation(String eventLocation) {
        this.eventLocation = eventLocation;
    }

    public String getEventLocationLat() {
        return eventLocationLat;
    }

    public void setEventLocationLat(String eventLocationLat) {
        this.eventLocationLat = eventLocationLat;
    }

    public String getEventLocationLont() {
        return eventLocationLont;
    }

    public void setEventLocationLont(String eventLocationLont) {
        this.eventLocationLont = eventLocationLont;
    }

    public Date getEventTime() {
        return eventTime;
    }

    public void setEventTime(Date eventTime) {
        this.eventTime = eventTime;
    }

    public String getUserInserted() {
        return userInserted;
    }

    public void setUserInserted(String userInserted) {
        this.userInserted = userInserted;
    }

    public Date getDateInserted() {
        return dateInserted;
    }

    public void setDateInserted(Date dateInserted) {
        this.dateInserted = dateInserted;
    }

    public String getUserModified() {
        return userModified;
    }

    public void setUserModified(String userModified) {
        this.userModified = userModified;
    }

    public Date getDateModified() {
        return dateModified;
    }

    public void setDateModified(Date dateModified) {
        this.dateModified = dateModified;
    }

    public MajlisCategory getEventCategory() {
        return eventCategory;
    }

    public void setEventCategory(MajlisCategory eventCategory) {
        this.eventCategory = eventCategory;
    }

   

    public Integer getManualCount() {
        return manualCount;
    }

    public void setManualCount(Integer manualCount) {
        this.manualCount = manualCount;
    }


    
    

    

    public MajlisGroup getGroupId() {
        return groupId;
    }

    public void setGroupId(MajlisGroup groupId) {
        this.groupId = groupId;
    }

  
   

   

    
    public List<MajlisGroupEventStatus> getMajlisGroupEventStatusList() {
        return majlisGroupEventStatusList;
    }

    public void setMajlisGroupEventStatusList(List<MajlisGroupEventStatus> majlisGroupEventStatusList) {
        this.majlisGroupEventStatusList = majlisGroupEventStatusList;
    }

   

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (eventId != null ? eventId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MajlisGroupEvent)) {
            return false;
        }
        MajlisGroupEvent other = (MajlisGroupEvent) object;
        if ((this.eventId == null && other.eventId != null) || (this.eventId != null && !this.eventId.equals(other.eventId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.dmssw.orm.models.MajlisGroupEvent[ eventId=" + eventId + " ]";
    }

}
