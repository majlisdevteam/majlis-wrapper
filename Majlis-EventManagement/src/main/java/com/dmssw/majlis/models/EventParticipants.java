/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dmssw.majlis.models;

/**
 *
 * @author Shanka
 */
public class EventParticipants {
    
    private int eventId;
    
    private String eventCaption;
    
    private int mobileUserId;
    
    private String mobileUserNo;
    
    private String mobileUser;
    
    private int userEventStatus;

    public int getEventId() {
        return eventId;
    }

    public void setEventId(int eventId) {
        this.eventId = eventId;
    }

    public String getEventCaption() {
        return eventCaption;
    }

    public void setEventCaption(String eventCaption) {
        this.eventCaption = eventCaption;
    }

    public int getMobileUserId() {
        return mobileUserId;
    }

    public void setMobileUserId(int mobileUserId) {
        this.mobileUserId = mobileUserId;
    }

    public String getMobileUserNo() {
        return mobileUserNo;
    }

    public void setMobileUserNo(String mobileUserNo) {
        this.mobileUserNo = mobileUserNo;
    }

    public String getMobileUser() {
        return mobileUser;
    }

    public void setMobileUser(String mobileUser) {
        this.mobileUser = mobileUser;
    }

    public int getUserEventStatus() {
        return userEventStatus;
    }

    public void setUserEventStatus(int userEventStatus) {
        this.userEventStatus = userEventStatus;
    }
    
    
    
    
    
}
