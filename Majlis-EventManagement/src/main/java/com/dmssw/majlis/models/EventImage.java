/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dmssw.majlis.models;

import java.util.ArrayList;

/**
 *
 * @author Shanka
 */
public class EventImage {
    
    private ArrayList<String> imageData;

    private int imageEvent;

    private int imageOwner;

    public ArrayList<String> getImageData() {
        return imageData;
    }

    public void setImageData(ArrayList<String> imageData) {
        this.imageData = imageData;
    }

    public int getImageEvent() {
        return imageEvent;
    }

    public void setImageEvent(int imageEvent) {
        this.imageEvent = imageEvent;
    }

    public int getImageOwner() {
        return imageOwner;
    }

    public void setImageOwner(int imageOwner) {
        this.imageOwner = imageOwner;
    }
}
