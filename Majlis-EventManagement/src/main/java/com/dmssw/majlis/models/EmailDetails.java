/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dmssw.majlis.models;

/**
 *
 * @author Sandali Kaushalya
 */

public class EmailDetails {
     
    private String mobileUserName;
    private String mobilUserEmail;
    private String mobileNo;
    private String mobileUserCounty;  
    private String groupTitle;
    private String groupDescription;
    private String eventCaption;
    private String eventIconPath;
    private String eventMessage;
    private String eventLocation;
    private String status;
    

    public String getMobileUserName() {
        return mobileUserName;
    }

    public void setMobileUserName(String mobileUserName) {
        this.mobileUserName = mobileUserName;
    }

    public String getMobilUserEmail() {
        return mobilUserEmail;
    }

    public void setMobilUserEmail(String mobilUserEmail) {
        this.mobilUserEmail = mobilUserEmail;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

      public String getMobileUserCounty() {
        return mobileUserCounty;
    }

    public void setMobileUserCounty(String mobileUserCounty) {
        this.mobileUserCounty = mobileUserCounty;
    }
    
    public String getGroupTitle() {
        return groupTitle;
    }

    public void setGroupTitle(String groupTitle) {
        this.groupTitle = groupTitle;
    }

    public String getGroupDescription() {
        return groupDescription;
    }

    public void setGroupDescription(String groupDescription) {
        this.groupDescription = groupDescription;
    }

    public String getEventCaption() {
        return eventCaption;
    }

    public void setEventCaption(String eventCaption) {
        this.eventCaption = eventCaption;
    }

    public String getEventIconPath() {
        return eventIconPath;
    }

    public void setEventIconPath(String eventIconPath) {
        this.eventIconPath = eventIconPath;
    }

    public String getEventMessage() {
        return eventMessage;
    }

    public void setEventMessage(String eventMessage) {
        this.eventMessage = eventMessage;
    }

    public String getEventLocation() {
        return eventLocation;
    }

    public void setEventLocation(String eventLocation) {
        this.eventLocation = eventLocation;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }  
}
