/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dmssw.majlis.models;

/**
 *
 * @author Dilhan Ganegama
 */
public class EventFeedback {
    
    private int eventId;
    private int userId;
    private int feedbackCnt;

    public int getEventId() {
        return eventId;
    }

    public void setEventId(int eventId) {
        this.eventId = eventId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getFeedbackCnt() {
        return feedbackCnt;
    }

    public void setFeedbackCnt(int feedbackCnt) {
        this.feedbackCnt = feedbackCnt;
    }
    
    
    
}
