/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.dmssw.majlis.models;

import com.dmssw.orm.models.MajlisCategory;
import com.dmssw.orm.models.MajlisEventComment;
import com.dmssw.orm.models.MajlisGroup;
import com.dmssw.orm.models.MajlisGroupEventImages;
import com.dmssw.orm.models.MajlisGroupEventStatus;
import com.dmssw.orm.models.MajlisGroupEventVideos;
import com.dmssw.orm.models.MajlisMdCode;
import com.dmssw.orm.models.MajlisNotification;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Dasun Chathuranga
 */
public class Events {
    private Integer eventId;
    private String eventSystemId;
    private String eventCaption;
    private String eventIconPath;
    private String eventMessage;
    private String eventLocation;
    private String eventLocationLat;
    private String eventLocationLont;
    private MajlisCategory majlisCategory;
    private List<MajlisCategory> majlisCategoryList;
    private List<MajlisMdCode> majlisMdList;
    private List<Group> groupId;
    private List<MajlisNotification> majlisNotificationList;
    private List<MajlisGroupEventVideos> majlisGroupEventVideosList;
    private List<MajlisGroupEventStatus> majlisGroupEventStatusList;
    private List<MajlisEventComment> majlisEventCommentList;
    private List<MajlisGroupEventImages> majlisGroupEventImagesList;
    private int count;
//    private int groupId;
    private Date eventTime;
    private String userInserted;
    private String userModified;
    private Date dateInserted;
    private Date dateModified;
    private int userId;
    private int manualCount;
    private int evtCnt;

    public MajlisCategory getMajlisCategory() {
        return majlisCategory;
    }

    public void setMajlisCategory(MajlisCategory majlisCategory) {
        this.majlisCategory = majlisCategory;
    }
    
    
    /**
     * @return the eventId
     */
    
    
    
    
    
    
    
    public Integer getEventId() {
        return eventId;
    }

    /**
     * @param eventId the eventId to set
     */
    public void setEventId(Integer eventId) {
        this.eventId = eventId;
    }

    /**
     * @return the eventSystemId
     */
    public String getEventSystemId() {
        return eventSystemId;
    }

    /**
     * @param eventSystemId the eventSystemId to set
     */
    public void setEventSystemId(String eventSystemId) {
        this.eventSystemId = eventSystemId;
    }

    /**
     * @return the eventCaption
     */
    public String getEventCaption() {
        return eventCaption;
    }

    /**
     * @param eventCaption the eventCaption to set
     */
    public void setEventCaption(String eventCaption) {
        this.eventCaption = eventCaption;
    }

    /**
     * @return the eventIconPath
     */
    public String getEventIconPath() {
        return eventIconPath;
    }

    /**
     * @param eventIconPath the eventIconPath to set
     */
    public void setEventIconPath(String eventIconPath) {
        this.eventIconPath = eventIconPath;
    }

    /**
     * @return the eventMessage
     */
    public String getEventMessage() {
        return eventMessage;
    }

    /**
     * @param eventMessage the eventMessage to set
     */
    public void setEventMessage(String eventMessage) {
        this.eventMessage = eventMessage;
    }

    /**
     * @return the eventLocation
     */
    public String getEventLocation() {
        return eventLocation;
    }

    /**
     * @param eventLocation the eventLocation to set
     */
    public void setEventLocation(String eventLocation) {
        this.eventLocation = eventLocation;
    }

    /**
     * @return the eventLocationLat
     */
    public String getEventLocationLat() {
        return eventLocationLat;
    }

    /**
     * @param eventLocationLat the eventLocationLat to set
     */
    public void setEventLocationLat(String eventLocationLat) {
        this.eventLocationLat = eventLocationLat;
    }

    /**
     * @return the eventLocationLont
     */
    public String getEventLocationLont() {
        return eventLocationLont;
    }

    /**
     * @param eventLocationLont the eventLocationLont to set
     */
    public void setEventLocationLont(String eventLocationLont) {
        this.eventLocationLont = eventLocationLont;
    }

    /**
     * @return the majlisCategoryList
     */
    public List<MajlisCategory> getMajlisCategoryList() {
        return majlisCategoryList;
    }

    /**
     * @param majlisCategoryList the majlisCategoryList to set
     */
    public void setMajlisCategoryList(List<MajlisCategory> majlisCategoryList) {
        this.majlisCategoryList = majlisCategoryList;
    }

    /**
     * @return the majlisMdList
     */
    public List<MajlisMdCode> getMajlisMdList() {
        return majlisMdList;
    }

    /**
     * @param majlisMdList the majlisMdList to set
     */
    public void setMajlisMdList(List<MajlisMdCode> majlisMdList) {
        this.majlisMdList = majlisMdList;
    }

    

    /**
     * @return the majlisNotificationList
     */
    public List<MajlisNotification> getMajlisNotificationList() {
        return majlisNotificationList;
    }

    /**
     * @param majlisNotificationList the majlisNotificationList to set
     */
    public void setMajlisNotificationList(List<MajlisNotification> majlisNotificationList) {
        this.majlisNotificationList = majlisNotificationList;
    }

    /**
     * @return the majlisGroupEventVideosList
     */
    public List<MajlisGroupEventVideos> getMajlisGroupEventVideosList() {
        return majlisGroupEventVideosList;
    }

    /**
     * @param majlisGroupEventVideosList the majlisGroupEventVideosList to set
     */
    public void setMajlisGroupEventVideosList(List<MajlisGroupEventVideos> majlisGroupEventVideosList) {
        this.majlisGroupEventVideosList = majlisGroupEventVideosList;
    }

    /**
     * @return the majlisGroupEventStatusList
     */
    public List<MajlisGroupEventStatus> getMajlisGroupEventStatusList() {
        return majlisGroupEventStatusList;
    }

    /**
     * @param majlisGroupEventStatusList the majlisGroupEventStatusList to set
     */
    public void setMajlisGroupEventStatusList(List<MajlisGroupEventStatus> majlisGroupEventStatusList) {
        this.majlisGroupEventStatusList = majlisGroupEventStatusList;
    }

    /**
     * @return the majlisEventCommentList
     */
    public List<MajlisEventComment> getMajlisEventCommentList() {
        return majlisEventCommentList;
    }

    /**
     * @param majlisEventCommentList the majlisEventCommentList to set
     */
    public void setMajlisEventCommentList(List<MajlisEventComment> majlisEventCommentList) {
        this.majlisEventCommentList = majlisEventCommentList;
    }

    /**
     * @return the majlisGroupEventImagesList
     */
    public List<MajlisGroupEventImages> getMajlisGroupEventImagesList() {
        return majlisGroupEventImagesList;
    }

    /**
     * @param majlisGroupEventImagesList the majlisGroupEventImagesList to set
     */
    public void setMajlisGroupEventImagesList(List<MajlisGroupEventImages> majlisGroupEventImagesList) {
        this.majlisGroupEventImagesList = majlisGroupEventImagesList;
    }

    /**
     * @return the count
     */
    public int getCount() {
        return count;
    }

    /**
     * @param count the count to set
     */
    public void setCount(int count) {
        this.count = count;
    }

    

    /**
     * @return the eventTime
     */
    public Date getEventTime() {
        return eventTime;
    }

    /**
     * @param eventTime the eventTime to set
     */
    public void setEventTime(Date eventTime) {
        this.eventTime = eventTime;
    }

    /**
     * @return the userInserted
     */
    public String getUserInserted() {
        return userInserted;
    }

    /**
     * @param userInserted the userInserted to set
     */
    public void setUserInserted(String userInserted) {
        this.userInserted = userInserted;
    }

    /**
     * @return the userModified
     */
    public String getUserModified() {
        return userModified;
    }

    /**
     * @param userModified the userModified to set
     */
    public void setUserModified(String userModified) {
        this.userModified = userModified;
    }

    /**
     * @return the dateInserted
     */
    public Date getDateInserted() {
        return dateInserted;
    }

    /**
     * @param dateInserted the dateInserted to set
     */
    public void setDateInserted(Date dateInserted) {
        this.dateInserted = dateInserted;
    }

    /**
     * @return the dateModified
     */
    public Date getDateModified() {
        return dateModified;
    }

    /**
     * @param dateModified the dateModified to set
     */
    public void setDateModified(Date dateModified) {
        this.dateModified = dateModified;
    }

    public List<Group> getGroupId() {
        return groupId;
    }

    public void setGroupId(List<Group> groupId) {
        this.groupId = groupId;
    }

    /**
     * @return the manualCount
     */
    public int getManualCount() {
        return manualCount;
    }

    /**
     * @param manualCount the manualCount to set
     */
    public void setManualCount(int manualCount) {
        this.manualCount = manualCount;
    }

    /**
     * @return the evtCnt
     */
    public int getEvtCnt() {
        return evtCnt;
    }

    /**
     * @param evtCnt the evtCnt to set
     */
    public void setEvtCnt(int evtCnt) {
        this.evtCnt = evtCnt;
    }

    /**
     * @return the userId
     */
    public int getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(int userId) {
        this.userId = userId;
    }


    
}
