/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dmssw.majlis.models;

/**
 *
 * @author GRTZ
 */
public class DeleteEventData {
    
    private int eventId;
    
    private int deletedObjId;
    
    private String operaton;

    public int getEventId() {
        return eventId;
    }

    public void setEventId(int eventId) {
        this.eventId = eventId;
    }

    public int getDeletedObjId() {
        return deletedObjId;
    }

    public void setDeletedObjId(int deletedObjId) {
        this.deletedObjId = deletedObjId;
    }

    public String getOperaton() {
        return operaton;
    }

    public void setOperaton(String operaton) {
        this.operaton = operaton;
    }
    
    
    
    
}
