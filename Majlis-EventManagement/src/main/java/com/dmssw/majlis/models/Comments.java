/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.dmssw.majlis.models;

import com.dmssw.orm.models.MajlisEventCommentReply;
import com.dmssw.orm.models.MajlisGroupEvent;
import java.sql.Date;
import java.util.List;

/**
 *
 * @author Dasun Chathuranga
 */
public class Comments {
    
    private Integer commentId;
    private String commentMessage;
    private Date dateInserted;
    private String userInserted;
    private List<MajlisEventCommentReply> majlisEventCommentReplyList;
    private int commentEventId;
    private int commentLike;

    public Integer getCommentId() {
        return commentId;
    }

    public void setCommentId(Integer commentId) {
        this.commentId = commentId;
    }

    public String getCommentMessage() {
        return commentMessage;
    }

    public void setCommentMessage(String commentMessage) {
        this.commentMessage = commentMessage;
    }

    public Date getDateInserted() {
        return dateInserted;
    }

    public void setDateInserted(Date dateInserted) {
        this.dateInserted = dateInserted;
    }

    public String getUserInserted() {
        return userInserted;
    }

    public void setUserInserted(String userInserted) {
        this.userInserted = userInserted;
    }

    public List<MajlisEventCommentReply> getMajlisEventCommentReplyList() {
        return majlisEventCommentReplyList;
    }

    public void setMajlisEventCommentReplyList(List<MajlisEventCommentReply> majlisEventCommentReplyList) {
        this.majlisEventCommentReplyList = majlisEventCommentReplyList;
    }

    public int getCommentEventId() {
        return commentEventId;
    }

    public void setCommentEventId(int commentEventId) {
        this.commentEventId = commentEventId;
    }

    public int getCommentLike() {
        return commentLike;
    }

    public void setCommentLike(int commentLike) {
        this.commentLike = commentLike;
    }

   
    
}
