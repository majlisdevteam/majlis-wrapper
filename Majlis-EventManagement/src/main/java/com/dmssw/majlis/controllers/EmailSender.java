/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dmssw.majlis.controllers;

/**
 *
 * @author Sandali Kaushalya
 */

import com.dmssw.majlis.models.EmailDetails;
import com.sun.mail.smtp.SMTPTransport;

import java.io.FileInputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;


/**
 *
 * @author Sandali Kaushalya
 */
public class EmailSender {

    public static HashMap getMailConfigurations() {

        String mailTransport = null;
        String mailIp = null;
        String smtpsHost = null;
        String mailAddress = null;
        String smtpsAuth = null;
        String sentFrom = null;
        String sentFromPword = null;
        HashMap mailConfig = new HashMap();

        try {

            Properties pro = new Properties();
            String wspath = System.getenv("WS_HOME");
            String pathset = wspath.replace("\\", "/");
            String ippath = pathset + "/Majlis_Conn.properties";
            pro.load(new FileInputStream(ippath));

            mailTransport = pro.getProperty("MAIL_TRANSPORT");
            mailConfig.put("mailTransport", mailTransport);

            mailIp = pro.getProperty("MAIL_CONNECT_IP");
            mailConfig.put("mailIp", mailIp);

            smtpsHost = pro.getProperty("MAIL_SMTPS_HOST");
            mailConfig.put("smtpsHost", smtpsHost);

            mailAddress = pro.getProperty("MAIL_ADDRESS");
            mailConfig.put("mailAddress", mailAddress);
            
            smtpsAuth = pro.getProperty("MAIL_SMTPS_AUTH");
            mailConfig.put("smtpsAuth", smtpsAuth);
            
            sentFrom = pro.getProperty("MAIL_SENT_FROM");
            mailConfig.put("sentFrom", sentFrom);

            sentFromPword = pro.getProperty("MAIL_SENT_FROM_PASSWORD");
            mailConfig.put("sentFromPword", sentFromPword);

        } catch (Exception e) {

            e.getMessage();
        }
        return mailConfig;
    }

    @SuppressWarnings("empty-statement")

    public static boolean sendHTMLMail(String toMailAdd, String admin, ResponseData data) throws MessagingException {
        boolean isSent = false;
       
        try {
            
            HashMap mailConfig = EmailSender.getMailConfigurations();
            String mailTransport = mailConfig.get("mailTransport").toString();
            String mailIp = mailConfig.get("mailIp").toString();
            String smtpsHost = mailConfig.get("smtpsHost").toString();
            String mailAddress = mailConfig.get("mailAddress").toString();
            String smtpsAuth = mailConfig.get("smtpsAuth").toString();
            String sentFrom = mailConfig.get("sentFrom").toString();
            String sentFromPword = mailConfig.get("sentFromPword").toString();

            
            Properties props = System.getProperties();
            props.put(smtpsHost, mailAddress);
            props.put(smtpsAuth, "false");
            Session session = Session.getInstance(props, null);
            Message msg = new MimeMessage(session);
            msg.setFrom(new InternetAddress(sentFrom));;
            msg.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(toMailAdd, false));

            EmailDetails det = (EmailDetails) data.getResponseData();

            msg.setSubject(det.getEventCaption() + " Attendence");
            msg.setText("Dear " + admin + ", \n\n      "
                    + "\n\n Event Name:  " + det.getEventCaption() + "\n Event Location: " + det.getEventLocation() + "\n Event Message: " + det.getEventMessage() + "\n\n"
                    + " Group Title: " + det.getGroupTitle() + "\n Description:  " + det.getGroupDescription() + "\n\n"
                    + " Mobile User Name:    " + det.getMobileUserName() + " \n User Mobile Number:    " + det.getMobileNo() + " \n Mobile User Country:  " + det.getMobileUserCounty() + "\n\n "
                    + "\n\n Marked as:  " + det.getStatus()
                    + "\n\n\n\n\n\n\n Majlis Event RSVP mail");
            msg.setHeader("X-Mailer", "Majlis Event RSVP mail");

            msg.setSentDate(new Date());
            SMTPTransport t = (SMTPTransport) session.getTransport(mailTransport);
            t.connect(mailIp, 25, sentFrom, sentFromPword);
            t.sendMessage(msg, msg.getAllRecipients());

            t.close();
            isSent = true;
        } catch (Exception ex) {

        }

        return isSent;
    }

}
