/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dmssw.majlis.controllers;

import com.dmssw.majlis.config.AppParams;
import com.dmssw.majlis.models.Comments;
import com.dmssw.majlis.models.DeleteEventData;
import com.dmssw.majlis.models.EmailDetails;
import com.dmssw.majlis.models.Event;
import com.dmssw.majlis.models.EventFeedback;
import com.dmssw.majlis.models.EventImage;
import com.dmssw.majlis.models.EventParticipants;
import com.dmssw.majlis.models.EventStatus;
import com.dmssw.majlis.models.EventVideo;
import com.dmssw.majlis.models.Events;
import com.dmssw.majlis.models.Group;

import com.dmssw.majlis.models.RSVPStatus;
import com.dmssw.orm.controllers.DbCon;
import com.dmssw.orm.controllers.ORMConnection;
import com.dmssw.orm.models.MajlisCategory;

import com.dmssw.orm.models.MajlisEventComment;
import com.dmssw.orm.models.MajlisEventFeedback;
import com.dmssw.orm.models.MajlisGroup;

import com.dmssw.orm.models.MajlisGroupEvent;
import com.dmssw.orm.models.MajlisGroupEventImages;
import com.dmssw.orm.models.MajlisGroupEventStatus;
import com.dmssw.orm.models.MajlisGroupEventVideos;
import com.dmssw.orm.models.MajlisMdCode;
import com.sun.javafx.scene.control.skin.VirtualFlow;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.logging.Level;
import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.imageio.ImageIO;
import javax.transaction.UserTransaction;
import javax.ws.rs.core.MultivaluedMap;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

//import util.ResponseWrapper;
/**
 *
 * @author Dasun Chathuranga
 */
@Stateless
@LocalBean
@TransactionManagement(TransactionManagementType.BEAN)
public class EventController {

    Logger logger;

    @Resource
    UserTransaction transaction;

    public EventController() {
        logger = com.dmssw.orm.config.AppParams.logger;

    }

    public ResponseData getEventLocationsForMap(String userId) {

        int result = -1;

        ResponseData rd = new ResponseData();

        DbCon db = new DbCon();

        Connection conn = db.getCon();

        List<MajlisGroupEvent> liEvent = new ArrayList<>();

        try {

            String where = (userId.equals("all")) ? "" : "AND g.GROUP_ADMIN = '" + userId + "'";

            String sqlForLocaions = "SELECT e.EVENT_LOCATION_LONT, e.EVENT_LOCATION_LAT,EVENT_ID  "
                    + "FROM majlis_group_event e, majlis_group g "
                    + "WHERE e.EVENT_STATUS='15'  "
                    + "AND g.GROUP_ID = e.GROUP_ID "
                    + "AND e.EVENT_DATE >= CURDATE()  "
                    + where;

            ResultSet psLong = db.search(conn, sqlForLocaions);

            while (psLong.next()) {

                MajlisGroupEvent event = new MajlisGroupEvent();

                event.setEventLocationLat(psLong.getString("EVENT_LOCATION_LAT"));
                event.setEventLocationLat(psLong.getString("EVENT_LOCATION_LONT"));
                event.setEventId(psLong.getInt("EVENT_ID"));

                liEvent.add(event);
            }

            rd.setResponseData(liEvent);

            result = 1;

        } catch (Exception e) {
            result = 999;
            e.printStackTrace();
        }

        rd.setResponseCode(result);

        return rd;
    }

    public ResponseData getEventParticipants(int eventId) {
        ResponseData rd = new ResponseData();

        DbCon db = new DbCon();

        Connection conn = db.getCon();

        int result = -1;

        List<EventParticipants> listP = new ArrayList<>();

        try {

            String where = (eventId == 0) ? "" : " AND e.EVENT_ID = '" + eventId + "' ";

            String sqlForUsers = "SELECT e.EVENT_ID,e.EVENT_CAPTION, m.MOBILE_USER_ID, m.MOBILE_USER_MOBILE_NO, m.MOBILE_USER_NAME, me.EVENT_STATUS "
                    + "FROM majlis_group_event e, majlis_mobile_users_event me, majlis_mobile_users m "
                    + "WHERE e.EVENT_ID = me.EVENT_ID "
                    + "AND m.MOBILE_USER_ID = me.USER_ID "
                    + "AND me.EVENT_STATUS IN (1,2) "
                    + where;

            ResultSet rs = db.search(conn, sqlForUsers);

            while (rs.next()) {
                EventParticipants p = new EventParticipants();

                p.setEventCaption(rs.getString("EVENT_CAPTION"));
                p.setEventId(rs.getInt("EVENT_ID"));
                p.setMobileUser(rs.getString("MOBILE_USER_NAME"));
                p.setMobileUserId(rs.getInt("MOBILE_USER_ID"));
                p.setMobileUserNo(rs.getString("MOBILE_USER_MOBILE_NO"));
                p.setUserEventStatus(rs.getInt("EVENT_STATUS"));
                listP.add(p);

            }

            rd.setResponseData(listP);
            result = 1;
            conn.close();
        } catch (Exception e) {

            result = 999;
            e.printStackTrace();
        }
        rd.setResponseCode(result);
        return rd;
    }

    public ResponseData getEventComments(int eventId) {

        ResponseData rd = new ResponseData();

        int result = -1;

        DbCon db = new DbCon();

        Connection conn = db.getCon();

        List<Comments> commentList = new ArrayList<>();

        try {

            String where = (eventId == 0) ? "" : " AND c.COMMENT_EVENT_ID = '" + eventId + "' ";

            String sqlForGetComments = "SELECT c.COMMENT_MESSAGE, c.DATE_INSERTED, c.COMMENT_LIKE, u.MOBILE_USER_NAME, c.COMMENT_EVENT_ID, c.COMMENT_ID "
                    + "FROM majlis_event_comment c, majlis_mobile_users u "
                    + "WHERE u.MOBILE_USER_ID = c.USER_INSERTED "
                    + where;

            System.out.println("SQL " + sqlForGetComments);

            ResultSet rs = db.search(conn, sqlForGetComments);

            while (rs.next()) {
                Comments c = new Comments();

                c.setCommentEventId(rs.getInt("COMMENT_EVENT_ID"));
                c.setCommentLike(rs.getInt("COMMENT_LIKE"));
                c.setCommentMessage(rs.getString("COMMENT_MESSAGE"));
                c.setCommentId(rs.getInt("COMMENT_ID"));
                c.setUserInserted(rs.getString("MOBILE_USER_NAME"));
                c.setDateInserted(rs.getDate("DATE_INSERTED"));

                commentList.add(c);
            }

            conn.close();
            rd.setResponseData(commentList);
            result = 1;
        } catch (Exception e) {
            result = 999;
            e.printStackTrace();
        }
        rd.setResponseCode(result);
        return rd;
    }

    public ResponseData deleteEventData(DeleteEventData event) {

        ResponseData rd = new ResponseData();
        int result = -1;

        DbCon db = new DbCon();

        Connection conn = db.getCon();

        try {
            transaction.begin();
            try {

                if (event.getOperaton().equals("comment")) {

                    String sqlForDelete = "DELETE FROM majlis_event_comment "
                            + "WHERE COMMENT_EVENT_ID='" + event.getEventId() + "' AND COMMENT_ID='" + event.getDeletedObjId() + "'";

                    PreparedStatement ps = db.prepare(conn, sqlForDelete);

                    ps.executeUpdate();

                } else if (event.getOperaton().equals("image")) {

                    String sqlForDelete = "DELETE FROM majlis_group_event_images "
                            + "WHERE EVENT_ID='" + event.getEventId() + "' AND EVENT_IMAGE_ID='" + event.getDeletedObjId() + "'";

                    PreparedStatement ps = db.prepare(conn, sqlForDelete);

                    ps.executeUpdate();

                } else if (event.getOperaton().equals("video")) {

                    String sqlForVideo = "DELETE FROM majlis_group_event_videos "
                            + "WHERE EVENT_ID='" + event.getEventId() + "' AND VIDEO_ID='" + event.getDeletedObjId() + "'";

                    PreparedStatement ps = db.prepare(conn, sqlForVideo);

                    ps.executeUpdate();

                }

                transaction.commit();
                conn.close();
                result = 1;
            } catch (Exception e) {
                result = 999;
                transaction.rollback();
                e.printStackTrace();
            }

        } catch (Exception ex) {
            result = 999;
            ex.printStackTrace();

        }

        rd.setResponseCode(result);
        return rd;
    }

    public ResponseData getEventVideos(int eventId) {

        ResponseData rd = new ResponseData();

        int result = -1;

        DbCon db = new DbCon();
        Connection conn = db.getCon();

        List<EventVideo> videoList = new ArrayList<>();

        try {

            String where = (eventId == 0) ? "" : " AND v.EVENT_ID = '" + eventId + "'";

            String sqlForVideos = "SELECT v.VIDEO_ID, v.EVENT_ID, v.VIDEO_FILE_PATH, u.MOBILE_USER_NAME "
                    + "FROM majlis_group_event_videos v, majlis_mobile_users u "
                    + "WHERE u.MOBILE_USER_ID = v.USER_INSERTED "
                    + where;
            
            System.out.println("SQL code video "+ sqlForVideos);

            ResultSet rs = db.search(conn, sqlForVideos);

            while (rs.next()) {
                
                
                System.out.println("Video found..........");
                EventVideo v = new EventVideo();

                v.setEventId(rs.getInt("EVENT_ID"));
                v.setVideoId(rs.getInt("VIDEO_ID"));
                v.setUserInserted(rs.getString("MOBILE_USER_NAME"));

                String path = com.dmssw.majlis.config.AppParams.MEDIA_SERVER + "/" + rs.getString("VIDEO_FILE_PATH");

                v.setVideoFilePath(path);

                videoList.add(v);
            }

            conn.close();
            rd.setResponseData(videoList);
            result = 1;
        } catch (Exception e) {
            result = 999;
            e.printStackTrace();
        }
        rd.setResponseCode(result);
        return rd;
    }

//    Get Events
    public ResponseData getEvents(int eventId, int start, int limit) {

        System.out.println("Events controller......");

        ORMConnection conn = new ORMConnection();

        ResponseData rw = new ResponseData();
        int result = -1;

        try {

            String where = (eventId == 0 ? " C.eventId=C.eventId" : " C.eventId = " + eventId);

            String hql = "SELECT C FROM MajlisGroupEvent C WHERE C.eventStatus =15 AND " + where;

            System.out.println("HQL " + hql);

//            String hql = "SELECT C from MajlisGroupEvent C WHERE C.eventStatus =1 ";
            List<Object> li = conn.hqlGetResultsWithLimit(hql, start, limit);
            List<MajlisGroupEvent> eventList = new ArrayList<>();

            for (Object obj : li) {
                MajlisGroupEvent evt = (MajlisGroupEvent) obj;

                String imgPath = evt.getEventIconPath(); //event/icon/
                String grpimgPath = evt.getGroupId().getGroupIconPath();
                String url = AppParams.MEDIA_SERVER + imgPath;
                String url2 = AppParams.MEDIA_SERVER + "/" + grpimgPath;
                evt.setEventIconPath(url);

                evt.getGroupId().setGroupIconPath(url2);
                eventList.add(evt);
            }
            rw.setResponseData(eventList);
            result = 1;

        } catch (Exception e) {
            result = -1;
            e.printStackTrace();
        }
        rw.setResponseCode(result);
        return rw;
    }
    //End Events

//    public ResponseData getEventListNotificaion(){
//    
//        System.out.println("create Event");
//       ResponseData rw = new ResponseData();
//       
//        
//       return rw;
//    }
    public ResponseData getCommentDetails(int eventId, int start, int limit) {

        System.out.println("Comments ");

        ORMConnection conn = new ORMConnection();

        ResponseData rw = new ResponseData();
        int result = -1;

        try {
            String where = (eventId == 0 ? " WHERE C.commentEventId=C.commentEventId" : " WHERE C.commentEventId = " + eventId);
            String hql = "SELECT C FROM MajlisEventComment C " + where;

            List<Object> li = conn.hqlGetResultsWithLimit(hql, start, limit);
            List<MajlisEventComment> comntList = new ArrayList<>();

            for (Object obj : li) {
                MajlisEventComment cmt = (MajlisEventComment) obj;

//              String imgPath = evt.getEventIconPath(); //event/icon/
//              
//              String url = AppParams.MEDIA_SERVER+imgPath;
//              evt.setEventIconPath(url);
                comntList.add(cmt);
            }
            rw.setResponseData(comntList);
            result = 1;

        } catch (Exception e) {
            result = -1;
            e.printStackTrace();
        }
        rw.setResponseCode(result);
        return rw;
    }

    public ResponseData getEventList(int eventId, int start, int limit) {

        ResponseData rd = new ResponseData();
        int result = -1;
        int rowCount = 0;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();

        List<Events> groupList = new ArrayList<>();
        try {

            System.out.println("Calling getEventList......");

            String where = (eventId == 0 ? " U.EVENT_ID= U.EVENT_ID" : " U.EVENT_ID = " + eventId);

            String sql = "select * from majlis_group_event G, majlis_mobile_users_event U "
                    + " where G.EVENT_ID = U.EVENT_ID and " + where + " GROUP BY G.EVENT_ID ";

            String sqlcnt = "select count(*) as CNT from majlis_mobile_users_event U, majlis_group_event_status E "
                    + " where E.STATUS_ID = 1 and " + where + " GROUP BY E.EVENT_ID ";

            System.out.println("Count for SQL " + sql);

            ResultSet status = dbCon.search(con, sql);
            ResultSet statusCnt = dbCon.search(con, sqlcnt);

            while (statusCnt.next()) {

                rowCount = statusCnt.getInt("CNT");
            }

            while (status.next()) {
                Events evt = new Events();
                evt.setEventId(status.getInt("EVENT_ID"));
                evt.setEventSystemId(status.getString("EVENT_SYSTEM_ID"));
                evt.setEventCaption(status.getString("EVENT_CAPTION"));
                evt.setEventIconPath(status.getString("EVENT_ICON_PATH"));
                evt.setEventMessage(status.getString("EVENT_MESSAGE"));
                evt.setEventLocation(status.getString("EVENT_LOCATION"));
                evt.setEventLocationLat(status.getString("EVENT_LOCATION_LAT"));
                evt.setEventLocationLont(status.getString("EVENT_LOCATION_LONT"));
                evt.setUserId(status.getInt("USER_ID"));

                if (status.getInt("MANUAL_COUNT") != 0) {
                    evt.setManualCount(status.getInt("MANUAL_COUNT"));
                } else {
                    evt.setManualCount(rowCount);
                }
                int groupId = status.getInt("GROUP_ID");

//                evt.setMajlisCategoryList(status.getInt("EVENT_CATEGORY"));
                //   evt.setEventTime(status.getDate("EVENT_TIME"));
                evt.setUserInserted(status.getString("USER_INSERTED"));
                //   evt.setDateInserted(status.getDate("DATE_INSERTED"));
                evt.setUserModified(status.getString("USER_MODIFIED"));
                evt.setDateModified(status.getDate("DATE_MODIFIED"));
//                evt.setCount(status.getInt("MANUAL_COUNT"));
                List<Group> grp = new ArrayList<>();

                String sqlForImg = "SELECT GROUP_ICON_PATH FROM majlis_group WHERE GROUP_ID='" + groupId + "'";

                ResultSet rs = dbCon.search(con, sqlForImg);
                String path = "";
                String url = "";
                while (rs.next()) {

                    Group g = new Group();

                    path = rs.getString("GROUP_ICON_PATH");

                    url = AppParams.MEDIA_SERVER + path;

                    g.setGroupIconPath(url);

                    grp.add(g);

                }
                evt.setGroupId(grp);
                String url2 = AppParams.MEDIA_SERVER + evt.getEventIconPath();
                evt.setEventIconPath(url2);

                groupList.add(evt);
                result = 1;
            }
            rd.setResponseData(groupList);
            result = 1;
        } catch (Exception e) {
            e.printStackTrace();
            result = 999;
        }
        rd.setResponseCode(result);
        return rd;
    }

//    public ResponseData getEventStatus(int userId) {
//
//        System.out.println("getEventStatus zzz ");
//        ResponseData rd = new ResponseData();
//        int result = -1;
//
//        DbCon dbCon = new DbCon();
//        Connection con = dbCon.getCon();
//        List<EventStatus> statusList = new ArrayList<>();
//        try {
////            String where = (eventId == 0 ? " WHERE U.EVENT_ID=U.EVENT_ID" : " WHERE U.EVENT_ID = '"+ eventId +"'");
////            where += (userId == 0  ? " WHERE U.USER_ID=U.USER_ID" : " WHERE U.USER_ID = '"+ userId +"'");
//            String where = (userId == 0 ? " U.USER_ID= U.USER_ID" : " U.USER_ID = " + userId);
//
//            String sql = "select * from majlis_mobile_users_event U, majlis_group_event_status E where " + where;
//            ResultSet status = dbCon.search(con, sql);
//            System.out.println("status qry " + sql);
//            while (status.next()) {
//                EventStatus es = new EventStatus();
//                es.setEventId(status.getInt("EVENT_ID"));
//                es.setUserId(status.getInt("USER_ID"));
//                es.setStatusId(status.getInt("EVENT_STATUS"));
//                es.setStatusId(status.getInt("STATUS_ID"));
//                es.setStatus(status.getString("STATUS"));
//
//                statusList.add(es);
//                result = 1;
//            }
//            rd.setResponseData(statusList);
//        } catch (Exception e) {
//            e.printStackTrace();
//            result = -1;
//        }
//
//        rd.setResponseCode(result);
//        return rd;
//    }
    public ResponseData getEventStatus(int userId, int eventId) {

        ResponseData rd = new ResponseData();
        int result = -1;

        DbCon db = new DbCon();
        Connection con = db.getCon();

        List<EventStatus> statusList = new ArrayList<>();
        try {
////            String where = (eventId == 0 ? " WHERE U.EVENT_ID=U.EVENT_ID" : " WHERE U.EVENT_ID = '"+ eventId +"'");
////            where += (userId == 0  ? " WHERE U.USER_ID=U.USER_ID" : " WHERE U.USER_ID = '"+ userId +"'");
//            if (userId != 0 && eventId != 0) {
//                String sql = "select * from majlis_group_event_status mges, majlis_mobile_users_event mmue where mmue.USER_ID=" + userId + " and mmue.EVENT_ID = " + eventId + " and mmue.EVENT_STATUS=mges.STATUS_ID ";
//
//                //select * from majlis_group_event_status mges, majlis_mobile_users_event mmue where mmue.USER_ID= 441 and mmue.EVENT_STATUS= 1 and mmue.EVENT_STATUS=mges.STATUS_ID
//                //else{
////                String where = (userId == 0 ? " U.USER_ID= U.USER_ID" : " U.USER_ID = " + userId);
////                where += (eventId == 0 ? "and U.EVENT_ID= U.EVENT_ID" : " U.EVENT_ID = " + eventId);
////
////                sql = "select * from majlis_mobile_users_event U, majlis_group_event_status E where " + where;
////            }
//                ResultSet status = dbCon.search(con, sql);
//                System.out.println("status qry " + sql);
//                while (status.next()) {
//                    EventStatus es = new EventStatus();
//                    es.setEventId(status.getInt("EVENT_ID"));
//                    es.setUserId(status.getInt("USER_ID"));
//                    es.setStatusId(status.getInt("EVENT_STATUS"));
//                    es.setStatusId(status.getInt("STATUS_ID"));
//                    es.setStatus(status.getString("STATUS"));
//
//                    statusList.add(es);
//
//                }
//
//                result = 1;
//                rd.setResponseData(statusList);
//            } else {
//                result = 1;
//
//                String selectQuery = "Select s.STATUS_ID,s.STATUS "
//                        + "from majlis_group_event_status s ";
//
//                ResultSet rs = dbCon.search(con, selectQuery);
//
//                while (rs.next()) {
//
//                    EventStatus s = new EventStatus();
//
//                    s.setStatusId(rs.getInt("STATUS_ID"));
//                    s.setStatus(rs.getString("STATUS"));
//
//                    statusList.add(s);
//                    ;
//                    rd.setResponseData("Successfull");
//
//                }
//                rd.setResponseData(statusList);
//
//            }

            String sqlForGetStatus = "SELECT * "
                    + "FROM majlis_group_event_status ";

            ResultSet rs = db.search(con, sqlForGetStatus);

            while (rs.next()) {
                EventStatus status = new EventStatus();

                status.setStatus(rs.getString("STATUS"));
                status.setStatusId(rs.getInt("STATUS_ID"));

                statusList.add(status);
            }

            result = 1;
            rd.setResponseData(statusList);
            con.close();
        } catch (Exception e) {
            result = 999;
            e.printStackTrace();
        }

        rd.setResponseCode(result);
        return rd;
    }

    public ResponseData saveEventIcon(MultipartFormDataInput input) {

        int result = -1;
        ResponseData rd = new ResponseData();

        Map<String, List<InputPart>> uploadForm = input.getFormDataMap();

        try {

            logger.info("start saveimage method............");

            transaction.begin();

            List<InputPart> inputParts = uploadForm.get("file");

            for (InputPart inputPart : inputParts) {

                MultivaluedMap<String, String> headers = inputPart.getHeaders();
                try {

                    java.io.InputStream inputStream = inputPart.getBody(java.io.InputStream.class, null);
                    byte[] bytes = IOUtils.toByteArray(inputStream);
                    String saveLocation = com.dmssw.majlis.config.AppParams.IMG_PATH;

                    Random rnd = new Random();

                    int randomId = 100000 + rnd.nextInt(900000);

                    String pathForDb = com.dmssw.majlis.config.AppParams.IMG_PATH_EVENT + "/" + randomId + ".jpg";

                    File dir = new File(saveLocation + com.dmssw.majlis.config.AppParams.IMG_PATH_EVENT);// Creating the directory if the directory is not created.

                    dir.mkdir();
                    logger.info("image path is " + saveLocation);

                    FileOutputStream stream = new FileOutputStream(saveLocation + pathForDb);

                    try {
                        stream.write(bytes);
                    } finally {
                        stream.close();
                    }

                    result = 1;
                    rd.setResponseData(com.dmssw.majlis.config.AppParams.MEDIA_SERVER + pathForDb);
                    transaction.commit();
                } catch (IOException e) {
                    transaction.rollback();
                    result = 999;
                    logger.error("Cannot save the image " + e.getMessage());

                }
            }

        } catch (Exception ex) {

            logger.error("EJB Exception " + ex.getMessage());

        }
        rd.setResponseCode(result);

        return rd;

    }

    public ResponseData createEvent(Event majlisGroupEvent, String userId) {
        ResponseData responseData = new ResponseData();
        int result = -1;

        System.out.println("createEvent method call.......");

        DbCon dbCon = new DbCon();
        Connection connection = dbCon.getCon();

        int groupId = 0, systemId = 0;
        String imgPath = null;

        if (majlisGroupEvent.getEventCaption() != null && majlisGroupEvent.getEventMessage() != null
                && majlisGroupEvent.getEventLocation() != null && majlisGroupEvent.getEventLocationLat() != null && majlisGroupEvent.getEventLocationLont() != null
                && majlisGroupEvent.getEventCategory() != null && majlisGroupEvent.getEventDate() != null && majlisGroupEvent.getEventStatus() != null) {

            try {

                try {

                    System.out.println("createEvent transaction started.......");

                    transaction.begin();
                    String insertSql = "INSERT INTO majlis_group_event ("
                            + "GROUP_ID,"
                            + "EVENT_CAPTION,"
                            + "EVENT_ICON_PATH,"
                            + "EVENT_MESSAGE,"
                            + "EVENT_LOCATION,"
                            + "EVENT_LOCATION_LAT,"
                            + "EVENT_LOCATION_LONT,"
                            + "EVENT_CATEGORY,"
                            + "EVENT_DATE,"
                            + "EVENT_TIME,"
                            + "EVENT_STATUS,"
                            + "USER_INSERTED,"
                            + "DATE_INSERTED,"
                            + "MANUAL_COUNT) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,NOW(),?)";

                    PreparedStatement pr = dbCon.prepareAutoId(connection, insertSql);

                    if (majlisGroupEvent.getEventIconPath() != null) {
                        String url = majlisGroupEvent.getEventIconPath();
                        String[] arrayUrl = url.split(com.dmssw.majlis.config.AppParams.MEDIA_SERVER + "/");

                        imgPath = arrayUrl[1];
                    } else {
                        imgPath = AppParams.IMG_PATH_EVENT + "/" + "images.jpeg";
                        imgPath = imgPath.substring(1);
                    }

                    pr.setInt(1, majlisGroupEvent.getGroupId().getGroupId());
                    pr.setString(2, majlisGroupEvent.getEventCaption());
                    pr.setString(3, imgPath);
                    pr.setString(4, majlisGroupEvent.getEventMessage());
                    pr.setString(5, majlisGroupEvent.getEventLocation());
                    pr.setString(6, majlisGroupEvent.getEventLocationLat());
                    pr.setString(7, majlisGroupEvent.getEventLocationLont());
                    pr.setInt(8, majlisGroupEvent.getEventCategory().getCategoryId());

                    String date = majlisGroupEvent.getEventDate();
                    SimpleDateFormat d = new SimpleDateFormat("yyyy-MM-dd");
                    java.util.Date dd = d.parse(date);
                    java.sql.Date da = new java.sql.Date(dd.getTime());
                    pr.setDate(9, da);

                    String time = majlisGroupEvent.getEventTime() + ":00";
                    SimpleDateFormat tf = new SimpleDateFormat("HH:MM:SS");
                    long tt = tf.parse(time).getTime();
                    java.sql.Time t = new java.sql.Time(tt);
                    pr.setTime(10, t);

                    pr.setInt(11, majlisGroupEvent.getEventStatus().getCodeId());
                    pr.setString(12, userId);
                    pr.setInt(13, majlisGroupEvent.getManualCount());

                    System.out.println("Going to insert ");

                    pr.executeUpdate();

                    ResultSet gen = pr.getGeneratedKeys();

                    while (gen.next()) {
                        groupId = gen.getInt(1);
                    }

                    System.out.println("Going to update....");

                    systemId = generateSystemId(groupId);

                    String updateSql = "UPDATE majlis_group_event SET EVENT_SYSTEM_ID = " + systemId + " WHERE EVENT_ID = " + groupId;

                    System.out.println("System ID" + systemId + "Gr");
                    dbCon.save(connection, updateSql);

                    transaction.commit();

                    result = 1;
                    responseData.setResponseData(groupId);

                } catch (Exception ex) {
                    result = 999;
                    transaction.rollback();
                    logger.info("SQL exception");
                    ex.printStackTrace();
                }

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
            result = 999;
            logger.info("Null entries");

        }

        responseData.setResponseCode(result);
        return responseData;
    }

    private int generateSystemId(int groupId) {
        int systemId = 0;
        Date currentDate = new Date();

        Calendar cal = Calendar.getInstance();
        cal.setTime(currentDate);
        String year = String.valueOf(cal.get(Calendar.YEAR));
        String month = String.valueOf(cal.get(Calendar.MONTH) + 1);
        systemId = Integer.parseInt(year + month + groupId);
        return systemId;

    }

    public ResponseData updateLikes(int commentId) {

        int commnt_like = 0;
        System.out.println("Update Likes ");
        ResponseData rw = new ResponseData();
        int result = -1;
        DbCon dbCon = new DbCon();
        Connection con = dbCon.getCon();
        try {
            List<Comments> comntList = new ArrayList<>();

            ResultSet comment = dbCon.search(con, "SELECT COALESCE(COMMENT_LIKE,0)+1 as MAX "
                    + "from majlis_event_comment where COMMENT_ID= '" + commentId + "'");

            while (comment.next()) {
                commnt_like = comment.getInt("MAX");
            }

//            PreparedStatement ps = dbCon.prepare(con,"update majlis_event_comment set COMMENT_LIKE = '"+commnt_like+"' where "
//                    + "COMMENT_ID = ?");
            String updateLikeQry = "update majlis_event_comment set COMMENT_LIKE = '" + commnt_like + "' where "
                    + "COMMENT_ID = '" + commentId + "'";

            dbCon.save(con, updateLikeQry);

            rw.setResponseData(comntList);
            result = 1;
        } catch (Exception e) {
            e.printStackTrace();
            result = -1;
        }
        rw.setResponseCode(result);
        return rw;
    }

    public ResponseData getUserEvents(int userId, String search, int eventIdInt, int start, int limit) {

        ResponseData responseData = new ResponseData();
        int result = -1;
        List<com.dmssw.majlis.models.MajlisGroupEvent> eventList = new ArrayList<com.dmssw.majlis.models.MajlisGroupEvent>();

        String where = (search.toUpperCase().equals("ALL") || search.equals("")) ? " " : "AND ge.EVENT_CAPTION LIKE '%" + search + "%' ";
        where += (eventIdInt == 0) ? "  " : " AND ge.EVENT_ID ='" + eventIdInt + "' ";

        String user = (userId == 0) ? " ug.USER_ID=ug.USER_ID " : " ug.USER_ID='" + userId + "' ";

        DbCon dbCon = new DbCon();
        Connection connection = dbCon.getCon();

        try {
            String sql = "SELECT * "
                    + "  FROM majlis_group_event ge, majlis_category mc, majlis_group mg "
                    + " WHERE ge.GROUP_ID IN (SELECT ug.GROUP_ID "
                    + "                         FROM majlis_mobile_user_group ug "
                    + "                        WHERE " + user + ") "
                    + " AND mg.GROUP_ID = ge.GROUP_ID " + where
                    + " AND mc.CATEGORY_ID = ge.EVENT_CATEGORY LIMIT " + start + " , " + limit;

            System.out.println("SQL Query.... " + sql);

            ResultSet rs = dbCon.search(connection, sql);
            while (rs.next()) {
                com.dmssw.majlis.models.MajlisGroupEvent majlisGroupEvent = new com.dmssw.majlis.models.MajlisGroupEvent();
                MajlisGroup majlisGroup = new MajlisGroup();

                MajlisCategory majlisCategory = new MajlisCategory();

                List<MajlisGroupEventStatus> statusList = new ArrayList<MajlisGroupEventStatus>();

                int eventId = rs.getInt("EVENT_ID");

                majlisGroupEvent.setEventId(rs.getInt("EVENT_ID"));
                majlisGroupEvent.setEventCaption(rs.getString("EVENT_CAPTION"));
                majlisGroupEvent.setEventIconPath(validateGroupIconPath(rs.getString("EVENT_ICON_PATH")));
                majlisGroupEvent.setEventTime(rs.getDate("EVENT_TIME"));
                majlisGroupEvent.setEventTimeString(rs.getTime("EVENT_TIME"));
                majlisGroupEvent.setEventDate(rs.getDate("EVENT_DATE"));
                majlisGroupEvent.setEventLocation(rs.getString("EVENT_LOCATION"));
                majlisGroupEvent.setEventMessage(rs.getString("EVENT_MESSAGE"));
                majlisGroupEvent.setEventLocationLat(rs.getString("EVENT_LOCATION_LAT"));
                majlisGroupEvent.setEventLocationLont(rs.getString("EVENT_LOCATION_LONT"));

                String sqlForGetCount = "SELECT ";

                majlisGroup.setGroupId(rs.getInt("GROUP_ID"));
                majlisGroup.setGroupIconPath(validateGroupIconPath(rs.getString("GROUP_ICON_PATH")));
                majlisGroup.setGroupTitle(rs.getString("GROUP_TITLE"));
                majlisGroup.setGroupDescription(rs.getString("GROUP_DESCRIPTION"));

                majlisCategory.setCategoryTitle(rs.getString("CATEGORY_TITLE"));

                String sqlForStatus = "SELECT * "
                        + "  FROM majlis_mobile_users_event es, majlis_group_event_status gs "
                        + " WHERE     gs.STATUS_ID = es.EVENT_STATUS "
                        + "       AND es.EVENT_ID = " + eventId + " "
                        + "       AND es.USER_ID = " + userId;

                DbCon db2 = new DbCon();

                Connection conn2 = db2.getCon();

                System.out.println("SQL for status.... " + sqlForStatus);

                ResultSet rsStatus = db2.search(conn2, sqlForStatus);

                while (rsStatus.next()) {

                    MajlisGroupEventStatus status = new MajlisGroupEventStatus();
                    status.setStatus(rsStatus.getString("STATUS"));
                    status.setStatusId(rsStatus.getInt("STATUS_ID"));
                    statusList.add(status);
                }

                if (statusList.size() == 0) {
                    MajlisGroupEventStatus status = new MajlisGroupEventStatus();
                    status.setStatus("Not Attending");
                    status.setStatusId(2);
                    statusList.add(status);
                }

                majlisGroupEvent.setGroupId(majlisGroup);
                majlisGroupEvent.setEventCategory(majlisCategory);

                majlisGroupEvent.setMajlisGroupEventStatusList(statusList);

                eventList.add(majlisGroupEvent);

                conn2.close();

                //resultSet.add(statusList);
            }

//            for (int i = 0; i < eventList.size(); i++) {
//                System.out.println(eventList.get(i).getMajlisGroupEventStatusList().size());
//                for (int j = 0; j < eventList.get(i).getMajlisGroupEventStatusList().size(); j++) {
//                    System.out.println(eventList.get(i).getMajlisGroupEventStatusList().get(j).getStatus());
//                }
//            }
            result = 1;
            responseData.setResponseData(eventList);

        } catch (Exception ex) {
            result = 999;
            ex.printStackTrace();
        } finally {

            try {
                connection.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }

        responseData.setResponseCode(result);
        return responseData;

    }

    private String validateGroupIconPath(String path) {

        String ipath = "";
        if (path != null) {
            return AppParams.MEDIA_SERVER + "/" + path;
        }
        return ipath;
    }

    public ResponseData saveRsvpStatus(int eventId, int userId, int statusId) {
        logger.info("saveRsvpStatus method call.........");

        ResponseData rd = new ResponseData();
        DbCon db = new DbCon();
        Connection conn = db.getCon();
        int flag = 0;

        try {
            String insertSql = "INSERT INTO majlis_mobile_users_event (EVENT_ID,USER_ID,EVENT_STATUS) "
                    + " VALUES(?,?,?)";

            logger.info("insertSql" + insertSql);

            PreparedStatement ps = db.prepare(conn, insertSql);

            ps.setInt(1, eventId);
            ps.setInt(2, userId);
            ps.setInt(3, statusId);

            db.executePreparedStatement(conn, ps);
            flag = 1;
            rd.setResponseData("successfull");

        } catch (Exception e) {

            logger.error("Error in saveRsvpStatus method.." + e);
            rd.setResponseData("Error in saveRsvpStatus method..");
            flag = -1;

        }
        rd.setResponseCode(flag);
        return rd;

    }

    public ResponseData getStatus(int eventId) {
        logger.info("getEventStatus method call.........");

        ResponseData rd = new ResponseData();
        DbCon db = new DbCon();
        Connection conn = db.getCon();
        int flag = 0;

        List<MajlisGroupEventStatus> EventStatusList = new ArrayList<>();

        try {

            String selectQuery = "Select s.STATUS_ID,s.STATUS "
                    + "from majlis_group_event_status s "
                    + "where s.EVENT_ID =" + eventId;

            ResultSet rs = db.search(conn, selectQuery);

            while (rs.next()) {

                MajlisGroupEventStatus s = new MajlisGroupEventStatus();

                s.setStatusId(rs.getInt("STATUS_ID"));
                s.setStatus(rs.getString("STATUS"));

                EventStatusList.add(s);
                flag = 1;
                rd.setResponseData("Successfull");

            }
            rd.setResponseData(EventStatusList);

        } catch (Exception e) {

            logger.error("Error in getEventStatus method.." + e);
            flag = -1;
            rd.setResponseData("Error in getEventStatus method..");

        }
        rd.setResponseCode(flag);
        return rd;
    }

    public ResponseData getAllEventStatus() {
        logger.info("getAllEventStatus method call.........");

        ResponseData rd = new ResponseData();
        DbCon db = new DbCon();
        Connection conn = db.getCon();
        int flag = 0;

        List<MajlisGroupEventStatus> EventStatusList = new ArrayList<>();

        try {

            String selectQuery = "Select s.STATUS_ID,s.STATUS "
                    + "from majlis_group_event_status s ";

            ResultSet rs = db.search(conn, selectQuery);

            while (rs.next()) {

                MajlisGroupEventStatus s = new MajlisGroupEventStatus();

                s.setStatusId(rs.getInt("STATUS_ID"));
                s.setStatus(rs.getString("STATUS"));

                EventStatusList.add(s);
                flag = 1;
                rd.setResponseData("Successfull");

            }
            rd.setResponseData(EventStatusList);

        } catch (Exception e) {

            logger.error("Error in getAllEventStatus method.." + e);
            flag = -1;
            rd.setResponseData("Error in getAllEventStatus method..");

        }
        rd.setResponseCode(flag);
        return rd;
    }

    public ResponseData updateRsvpStatus(RSVPStatus status) {
        logger.info("updateRsvpStatus method call.........");

        int eventId = status.getEventId();
        int statusId = status.getStatusId();
        int userId = status.getUserId();

        ResponseData rd = new ResponseData();
        DbCon db = new DbCon();
        Connection conn = db.getCon();
        int flag = -1;
        int count;

        try {
            String selectQuery = "Select count(*) as COUNT  from majlis_mobile_users_event s "
                    + "where s.EVENT_ID='" + eventId + "'"
                    + " AND s.USER_ID='" + userId + "'";

            ResultSet rs = db.search(conn, selectQuery);

            while (rs.next()) {

                count = rs.getInt("COUNT");

                if (count > 0) {
                    String deleteQuery = "Delete from majlis_mobile_users_event "
                            + " where EVENT_ID='" + eventId + "'"
                            + " AND USER_ID='" + userId + "'";

                    db.save(conn, deleteQuery);

                }
                rd = saveRsvpStatus(eventId, userId, statusId);

            }
            conn.close();

            Runnable run = new Runnable() {
                @Override
                public void run() {
                    System.out.println(">>>>>>>>>>>>>>>");
                    sendEmailToAdmin(eventId, userId, statusId);

                }
            };
            
            Thread th = new Thread(run);
            
            th.isDaemon();
            
            th.start();
            

            flag = 1;

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error in updateRsvpStatus method.." + e);
            rd.setResponseData("Error in updateRsvpStatus method..");

            rd.setResponseCode(999);
        }

        return rd;
    }

    public ResponseData sendEmailToAdmin(int eventId, int userId, int statusId) {
        logger.info("sendEmailToAdmin method call.........");

        ResponseData rd = new ResponseData();
        DbCon db = new DbCon();
        Connection conn = db.getCon();
        int flag = 0;

        try {

            String selectQuery = " select g.GROUP_ADMIN,u.EMAIL_ADDRESS,u.USER_FULL_NAME "
                    + " from majlis_group g,majlis_cms_users u"
                    + " where g.GROUP_ID = ( select e.GROUP_ID"
                    + " from majlis_group_event e"
                    + " where e.EVENT_ID ='" + eventId + "')"
                    + "AND u.USER_ID = g.GROUP_ADMIN";

            ResultSet rs = db.search(conn, selectQuery);

            while (rs.next()) {
                String email = rs.getString("EMAIL_ADDRESS");
                String adminName = rs.getString("USER_FULL_NAME");
                ResponseData data = loadMobileUserDetails(eventId, userId, statusId);
                logger.info("before Email send method call...........");
                boolean b = EmailSender.sendHTMLMail(email, adminName, data);
                logger.info("boolean msg:" + b);

            }

        } catch (Exception e) {
            logger.error("Error in sendEmailToAdmin method.." + e);
            rd.setResponseData("Error in sendEmailToAdmin method..");
            flag = -1;
            rd.setResponseCode(flag);
        }

        return rd;
    }

    public ResponseData loadMobileUserDetails(int eventId, int userId, int statusId) {
        logger.info("loadMobileUserDetails method....................");

        DbCon db = new DbCon();
        Connection conn = db.getCon();

        ResponseData rd = new ResponseData();
        int flag = 0;

        EmailDetails details = new EmailDetails();

        try {
            // get mobile user details
            String getUserDetails = "select u.MOBILE_USER_NAME,u.MOBILE_USER_EMAIL,u.MOBILE_USER_COUNTRY,"
                    + "u.MOBILE_USER_MOBILE_NO "
                    + "from majlis_mobile_users u "
                    + "where u.MOBILE_USER_ID='" + userId + "'";

            ResultSet rs = db.search(conn, getUserDetails);

            while (rs.next()) {
                details.setMobileUserName(rs.getString("MOBILE_USER_NAME"));
                details.setMobilUserEmail(rs.getString("MOBILE_USER_EMAIL"));
                details.setMobileUserCounty(rs.getString("MOBILE_USER_COUNTRY"));
                details.setMobileNo(rs.getString("MOBILE_USER_MOBILE_NO"));

            }

            //get group details
            String getGroupDetails = "select m.GROUP_TITLE,m.GROUP_DESCRIPTION, g.EVENT_CAPTION,g.EVENT_ICON_PATH,"
                    + " g.EVENT_MESSAGE,g.EVENT_LOCATION"
                    + " from majlis_group_event g,majlis_group m"
                    + " where g.EVENT_ID = '" + eventId + "'"
                    + "  AND m.GROUP_ID = g.GROUP_ID";

            ResultSet rs2 = db.search(conn, getGroupDetails);

            while (rs2.next()) {
                details.setGroupTitle(rs2.getString("GROUP_TITLE"));
                details.setGroupDescription(rs2.getString("GROUP_DESCRIPTION"));
                details.setEventCaption(rs2.getString("EVENT_CAPTION"));
                details.setEventIconPath(rs2.getString("EVENT_ICON_PATH"));
                details.setEventMessage(rs2.getString("EVENT_MESSAGE"));
                details.setEventLocation(rs2.getString("EVENT_LOCATION"));

            }

            String getStatus = "select s.STATUS"
                    + " from majlis_group_event_status s"
                    + " where s.STATUS_ID = '" + statusId + "'";

            ResultSet rs3 = db.search(conn, getStatus);

            while (rs3.next()) {
                details.setStatus(rs3.getString("STATUS"));

            }
            flag = 1;
            rd.setResponseCode(flag);
            rd.setResponseData(details);

        } catch (Exception e) {
            logger.error("Error loadUserDetails..." + e);
            flag = -1;
            rd.setResponseCode(flag);
            rd.setResponseData("Error loadUserDetails...");

        }
        return rd;
    }

    public ResponseData updateEventFeedback(EventFeedback feedback) {
        logger.info("updateEventFeedback method call.........");

        ResponseData rd = new ResponseData();
        DbCon db = new DbCon();
        Connection conn = db.getCon();
        int flag = 0;
        int count;

        try {
            String selectQuery = "Select count(*) as COUNT  from majlis_event_feedback f "
                    + "where f.EVENT_ID='" + feedback.getEventId() + "'"
                    + " AND f.USER_ID='" + feedback.getUserId() + "'";

            ResultSet rs = db.search(conn, selectQuery);

            while (rs.next()) {

                count = rs.getInt("COUNT");

                if (count > 0) {
                    String deleteQuery = "Delete from majlis_event_feedback "
                            + " where EVENT_ID='" + feedback.getEventId() + "'"
                            + " AND USER_ID='" + feedback.getUserId() + "'";

                    db.save(conn, deleteQuery);

                }

                String insertSql = "INSERT INTO majlis_event_feedback (EVENT_ID,USER_ID,RATINGS) "
                        + " VALUES(?,?,?)";

                logger.info("insertSql" + insertSql);

                PreparedStatement ps = db.prepare(conn, insertSql);

                ps.setInt(1, feedback.getEventId());
                ps.setInt(2, feedback.getUserId());
                ps.setInt(3, feedback.getFeedbackCnt());

                db.executePreparedStatement(conn, ps);
                flag = 1;
                rd.setResponseData("successfull");
                rd.setResponseCode(flag);

            }
            logger.info("before insert............");

        } catch (Exception ex) {
            logger.info("error in updateEventFeedback ...." + ex);
            flag = -1;
            rd.setResponseData("Error in updateEventFeedback ....");
            rd.setResponseCode(flag);
        }

        return rd;
    }

    public ResponseData getEventFeedback(int eventId, int userId) {
        logger.info("getEventFeedback method.....................");

        ResponseData rd = new ResponseData();
        DbCon db = new DbCon();
        Connection conn = db.getCon();
        int flag = 0;

        List<MajlisEventFeedback> EventFeedbackList = new ArrayList<>();
        try {
            String selectQuery = "Select f.FEEDBACK_ID,f.EVENT_ID,f.USER_ID,f.RATINGS"
                    + " from majlis_event_feedback f"
                    + " where f.EVENT_ID ='" + eventId + "'"
                    + " AND f.USER_ID='" + userId + "'";
            logger.info("selectQuery:" + selectQuery);

            ResultSet rs = db.search(conn, selectQuery);

            while (rs.next()) {

                MajlisEventFeedback feedback = new MajlisEventFeedback();
                feedback.setEventId(rs.getInt("EVENT_ID"));
                feedback.setUserId(rs.getInt("USER_ID"));
                feedback.setRatings(rs.getInt("RATINGS"));
                feedback.setFeedbackId(rs.getInt("FEEDBACK_ID"));

                EventFeedbackList.add(feedback);
                flag = 1;
                rd.setResponseCode(flag);
                rd.setResponseData(EventFeedbackList);

            }

        } catch (Exception ex) {
            logger.info("error in getEventFeedback ...." + ex);
            flag = -1;
            rd.setResponseCode(flag);
        }
        return rd;
    }

    public ResponseData postComments(MajlisEventComment comment) {
        logger.info("postComments method.....................");

        ResponseData rd = new ResponseData();
        DbCon db = new DbCon();
        Connection conn = db.getCon();
        int flag = 0;

        try {
            String insertSql = "Insert into majlis_event_comment (COMMENT_MESSAGE ,COMMENT_EVENT_ID,DATE_INSERTED,USER_INSERTED,COMMENT_LIKE)"
                    + "values(?,?,CURRENT_TIMESTAMP,?,?)";

            logger.info("insertSql" + insertSql);

            PreparedStatement ps = db.prepare(conn, insertSql);

            int eventId = (comment.getCommentEventId()).getEventId();

            ps.setString(1, comment.getCommentMessage());
            ps.setInt(2, eventId);
            ps.setInt(3, comment.getUserInserted());
            ps.setInt(4, 0);

            ps.executeUpdate();
            flag = 1;
            rd.setResponseData("successfull");
            rd.setResponseCode(flag);

        } catch (Exception ex) {
            logger.info("error in postComments ...." + ex);
            flag = -1;
            rd.setResponseCode(flag);
            ex.printStackTrace();
        }
        return rd;
    }

    public ResponseData saveEventImages(EventImage eventImage) {

        int result = -1;

        ResponseData rd = new ResponseData();

        try {

            transaction.begin();

            try {

                if (eventImage != null) {

                    List<String> imageList = eventImage.getImageData();

                    for (String img : imageList) {

                        int eventId = eventImage.getImageEvent();

                        String saveLocation = com.dmssw.majlis.config.AppParams.IMG_PATH;

                        Random rnd = new Random();

                        int randomId = 100000 + rnd.nextInt(900000);

                        String pathForDb = com.dmssw.majlis.config.AppParams.IMG_PATH_EVENT + "/uploads/" + eventId + "/" + eventId + "_IMG_" + eventImage.getImageOwner() + "_" + randomId + ".jpg";

                        /**
                         * check the image dir already exists or not.
                         */
                        File dir = new File(saveLocation + com.dmssw.majlis.config.AppParams.IMG_PATH_EVENT + "/uploads/" + eventId);

                        dir.mkdir();

                        logger.info("image path is " + saveLocation);

                        byte[] bb = new sun.misc.BASE64Decoder().decodeBuffer(img.split(",")[1]);
                        final BufferedImage bufferedImage = ImageIO.read(new ByteArrayInputStream(bb));

                        try {
                            File f = new File(saveLocation + pathForDb);
                            ImageIO.write(bufferedImage, "JPG", f);

                            DbCon db = new DbCon();

                            Connection conn = db.getCon();

                            String sql = "INSERT INTO majlis_group_event_images (EVENT_ID,EVENT_IMAGE_PATH,USER_INSERTED,DATE_INSERTED) VALUES(?,?,?,NOW())";

                            PreparedStatement pr = db.prepare(conn, sql);
                            System.out.println("eventid " + eventId);
                            pr.setInt(1, eventId);
                            pr.setString(2, pathForDb);
                            pr.setInt(3, eventImage.getImageOwner());

                            pr.executeUpdate();

                            result = 1;

                        } catch (Exception ex) {
                            transaction.rollback();
                            logger.info(" saveEventImages error " + ex.getMessage());

                        }
                    }

                }
                transaction.commit();

            } catch (Exception e) {

                transaction.rollback();
                result = 999;

                logger.error("Error on save image " + e.getMessage());

            }

            result = 1;

            logger.info("Event image has been saved......");

        } catch (Exception ex) {
            result = 999;
            logger.error("EJB Exception " + ex.getMessage());

        }

        rd.setResponseCode(result);
        return rd;
    }

    public ResponseData saveEventVideos(MultipartFormDataInput input, int eventId, int eventOwner) {

        int result = -1;

        System.out.println("event id " + eventId + " event owner " + eventOwner);

        ResponseData rd = new ResponseData();

        Map<String, List<InputPart>> uploadForm = input.getFormDataMap();

        DbCon db = new DbCon();

        Connection conn = db.getCon();

        try {

            logger.info("start saveEventVideo method............");

            transaction.begin();

            List<InputPart> inputParts = uploadForm.get("file");

            for (InputPart inputPart : inputParts) {

                MultivaluedMap<String, String> headers = inputPart.getHeaders();
                try {
                    InputStream inputStream = inputPart.getBody(InputStream.class, null);
                    byte[] bytes = IOUtils.toByteArray(inputStream);
//                String filename = getFileName(headers);

                    String saveLocation = com.dmssw.majlis.config.AppParams.IMG_PATH;

                    Random rnd = new Random();

                    int randomId = 100000 + rnd.nextInt(900000);

                    String pathForDb = com.dmssw.majlis.config.AppParams.IMG_PATH_EVENT + "/uploads/videos/" + eventId + "/" + eventId + "_VID_" + eventOwner + "_" + randomId + ".mp4";

                    /**
                     * check the image dir already exists or not.
                     */
                    File dir = new File(saveLocation + com.dmssw.majlis.config.AppParams.IMG_PATH_EVENT + "/uploads/videos/" + eventId);

                    dir.mkdir();

                    logger.info("image path is " + saveLocation);

                    FileOutputStream stream = new FileOutputStream(saveLocation + pathForDb);

                    try {
                        stream.write(bytes);
                    } finally {
                        stream.close();
                    }

                    String sql = "INSERT INTO majlis_group_event_videos (EVENT_ID,USER_INSERTED,VIDEO_FILE_PATH,DATE_INSERTED) VALUES(?,?,?,NOW())";

                    PreparedStatement pr = conn.prepareStatement(sql);

                    pr.setInt(1, eventId);
                    pr.setInt(2, eventOwner);
                    pr.setString(3, pathForDb);

                    pr.executeUpdate();

                    result = 1;

                    transaction.commit();
                } catch (IOException e) {
                    transaction.rollback();
                    result = 999;
                    logger.error("Cannot save the video " + e.getMessage());

                }
            }

        } catch (Exception ex) {

            logger.error("EJB Exception " + ex.getMessage());

        }
        rd.setResponseCode(result);

        return rd;

    }

//        private String getFileName(MultivaluedMap<String, String> headers) {
//        String[] contentDisposition = headers.getFirst("Content-Disposition").split(";");
//
//        for (String filename : contentDisposition) {
//            if ((filename.trim().startsWith("filename"))) {
//
//                String[] name = filename.split("=");
//
//                String finalFileName = sanitizeFilename(name[1]);
//                return finalFileName;
//            }
//        }
//        return "unknown";
//    }
//    private String sanitizeFilename(String s) {
//        return s.trim().replaceAll("\"", "");
//    }
    public ResponseData getEventImageList(int eventId) {

        ResponseData rd = new ResponseData();

        ORMConnection orm = new ORMConnection();

        int result = -1;

        List<MajlisGroupEventImages> imagesList = new ArrayList<>();

        String where = (eventId == 0) ? " WHERE I.eventId = I.eventId" : " WHERE I.eventId='" + eventId + "'";

        try {

            String hql = "SELECT I FROM MajlisGroupEventImages I " + where;

            List<Object> resultList = orm.hqlGetResults(hql);

            for (Object obj : resultList) {
                MajlisGroupEventImages img = (MajlisGroupEventImages) obj;
                String fullUrl = com.dmssw.majlis.config.AppParams.MEDIA_SERVER + img.getEventImagePath();
                img.setEventImagePath(fullUrl);
                imagesList.add(img);
            }
            rd.setResponseData(imagesList);
            result = 1;
        } catch (Exception e) {
            e.printStackTrace();
            result = 999;
        }

        rd.setResponseCode(result);
        return rd;

    }

    public ResponseData getEventImagesCMS(int eventId) {
        ResponseData rd = new ResponseData();

        int result = -1;

        DbCon db = new DbCon();

        Connection conn = db.getCon();

        List<MajlisGroupEventImages> imgList = new ArrayList<>();

        try {

            String where = (eventId == 0) ? "" : "WHERE i.EVENT_ID = '" + eventId + "'";

            String sqlForImages = "SELECT i.EVENT_IMAGE_ID, i.EVENT_IMAGE_PATH, i.EVENT_ID "
                    + "FROM majlis_group_event_images i "
                    + where;

            ResultSet rs = db.search(conn, sqlForImages);

            while (rs.next()) {
                MajlisGroupEventImages img = new MajlisGroupEventImages();

                img.setEventImageId(rs.getInt("EVENT_IMAGE_ID"));

                MajlisGroupEvent ev = new MajlisGroupEvent();
                ev.setEventId(rs.getInt("EVENT_ID"));

                img.setEventId(ev);

                String path = com.dmssw.majlis.config.AppParams.MEDIA_SERVER + "/" + rs.getString("EVENT_IMAGE_PATH");

                img.setEventImagePath(path);

                imgList.add(img);

            }
            rd.setResponseData(imgList);
            conn.close();
            result = 1;
        } catch (Exception e) {
            result = 999;
            e.printStackTrace();
        }

        rd.setResponseCode(result);
        return rd;
    }

    public ResponseData getEventVideoList(int eventId) {

        ResponseData rd = new ResponseData();

        DbCon db = new DbCon();
        
        Connection conn = db.getCon();

        int result = -1;

        List<MajlisGroupEventVideos> imagesList = new ArrayList<>();

        String where = (eventId == 0) ? " WHERE I.EVENT_ID = I.EVENT_ID" : " WHERE I.EVENT_ID='" + eventId + "'";

        try {

            String hql = "SELECT * FROM majlis_group_event_videos I " + where;
            
            System.out.println("Video list "+hql);

            ResultSet resultList = db.search(conn, hql);

            while (resultList.next()) {
                MajlisGroupEventVideos img = new MajlisGroupEventVideos();
                
                String fullUrl = com.dmssw.majlis.config.AppParams.MEDIA_SERVER + img.getVideoFilePath();
                img.setVideoFilePath(fullUrl);
                
                
                
                imagesList.add(img);
            }
            rd.setResponseData(imagesList);
            result = 1;
        } catch (Exception e) {
            e.printStackTrace();
            result = 999;
        }finally{
            try {
                conn.close();
            } catch (SQLException ex) {

            }
        }
        rd.setResponseCode(result);
        return rd;

    }

    /**
     * Returns a list of events.
     *
     * @param start -
     * @param limit
     * @return
     */
    public ResponseData getEventList(String userId, int start, int limit) {
        ResponseData responseData = new ResponseData();
        int result = -1;
        List<Event> eventList = new ArrayList<Event>();

        String user = (userId.equals("all")) ? "" : " AND mg.GROUP_ADMIN = '" + userId + "' ";

        try {
            DbCon dbCon = new DbCon();
            Connection connection = dbCon.getCon();

            String sql = "SELECT * FROM majlis_group_event mge,majlis_group mg,majlis_category mc,majlis_md_code mmd "
                    + "WHERE mge.EVENT_CATEGORY=mc.CATEGORY_ID "
                    + "AND mge.GROUP_ID = mg.GROUP_ID "
                    + user
                    + " AND mge.EVENT_STATUS = mmd.CODE_ID LIMIT " + limit + " OFFSET " + start;

            System.out.println("User event List sql" + sql);

            ResultSet rs = dbCon.search(connection, sql);

            while (rs.next()) {
                Event event = new Event();
                MajlisCategory majlisCategory = new MajlisCategory();
                MajlisMdCode mdCode = new MajlisMdCode();
                MajlisGroup majlisGroup = new MajlisGroup();
                event.setEventCaption(rs.getString("EVENT_CAPTION"));

                majlisCategory.setCategoryTitle(rs.getString("CATEGORY_TITLE"));
                majlisCategory.setCategoryId(rs.getInt("CATEGORY_ID"));
                event.setEventDate(rs.getDate("EVENT_DATE") + "");
                event.setEventIconPath(validateGroupIconPath(rs.getString("EVENT_ICON_PATH")));
                event.setEventLocation(rs.getString("EVENT_LOCATION"));
                event.setEventLocationLat(rs.getString("EVENT_LOCATION_LAT"));
                event.setEventLocationLont(rs.getString("EVENT_LOCATION_LONT"));
                event.setEventId(rs.getInt("EVENT_ID"));
                event.setEventMessage(rs.getString("EVENT_MESSAGE"));

                mdCode.setCodeId(rs.getInt("EVENT_STATUS"));
                mdCode.setCodeMessage("CODE_MESSAGE");
                event.setEventStatus(mdCode);

                event.setEventTime(rs.getTime("EVENT_TIME") + "");

                majlisGroup.setGroupId(rs.getInt("GROUP_ID"));
                event.setGroupId(majlisGroup);
                event.setManualCount(rs.getInt("MANUAL_COUNT"));
                event.setEventCategory(majlisCategory);
                eventList.add(event);
            }

            responseData.setResponseData(eventList);
            result = 1;
            connection.close();
        } catch (Exception ex) {
            result = 999;
            logger.info("Exception");
            ex.printStackTrace();
        } finally {

        }

        responseData.setResponseCode(result);
        return responseData;

    }

    public ResponseData updateEventDetails(String userId, Event event) {

        int result = -1;

        DbCon db = new DbCon();

        ResponseData rd = new ResponseData();

        Connection conn = db.getCon();

        String imgPath = "";

        try {

            if (event.getEventIconPath() != null) {
                String url = event.getEventIconPath();
                String[] arrayUrl = url.split(com.dmssw.majlis.config.AppParams.MEDIA_SERVER + "/");

                imgPath = arrayUrl[1];
            } else {
                imgPath = AppParams.IMG_PATH_EVENT + "/" + "images.jpeg";
                imgPath = imgPath.substring(1);
            }

            String sqlForUpdate = "UPDATE majlis_group_event e "
                    + "SET e.EVENT_CAPTION=?, "
                    + "EVENT_ICON_PATH=?, "
                    + "EVENT_MESSAGE=?, "
                    + "EVENT_LOCATION=?, "
                    + "EVENT_LOCATION_LAT=?, "
                    + "EVENT_LOCATION_LONT =?, "
                    + "EVENT_CATEGORY=?, "
                    + "EVENT_TIME=?, "
                    + "EVENT_STATUS=?, "
                    + "MANUAL_COUNT=?, "
                    + "EVENT_DATE=?,"
                    + "USER_MODIFIED=?, "
                    + "DATE_MODIFIED=NOW() "
                    + "WHERE EVENT_ID=?";

            System.out.println("State " + sqlForUpdate);

            PreparedStatement ps = db.prepare(conn, sqlForUpdate);

            ps.setString(1, event.getEventCaption());
            ps.setString(2, imgPath);
            ps.setString(3, event.getEventMessage());
            ps.setString(4, event.getEventLocation());
            ps.setString(5, event.getEventLocationLat());
            ps.setString(6, event.getEventLocationLont());
            ps.setInt(7, event.getEventCategory().getCategoryId());
            ps.setString(8, event.getEventTime());
            ps.setInt(9, event.getEventStatus().getCodeId());
            ps.setInt(10, event.getManualCount());
            ps.setString(11, event.getEventDate());
            ps.setString(12, userId);
            ps.setInt(13, event.getEventId());

            ps.executeUpdate();

            conn.close();

            result = 1;
        } catch (Exception e) {
            result = 999;
            e.printStackTrace();
        }

        rd.setResponseCode(result);
        return rd;
    }

    public ResponseData getEventsListForDetails(int eventId, int start, int limit) {
        ResponseData responseData = new ResponseData();
        int result = -1;
        List<Event> eventList = new ArrayList<Event>();

        try {
            DbCon dbCon = new DbCon();
            Connection connection = dbCon.getCon();

            String sql = "SELECT * FROM majlis_group_event mge,majlis_group mg,majlis_category mc,majlis_md_code mmd "
                    + "WHERE mge.EVENT_CATEGORY=mc.CATEGORY_ID "
                    + "AND mge.GROUP_ID = mg.GROUP_ID "
                    + " AND mge.EVENT_ID = '" + eventId + "'"
                    + " AND mge.EVENT_STATUS = mmd.CODE_ID LIMIT " + limit + " OFFSET " + start;

            ResultSet rs = dbCon.search(connection, sql);

            while (rs.next()) {
                Event event = new Event();
                MajlisCategory majlisCategory = new MajlisCategory();
                MajlisMdCode mdCode = new MajlisMdCode();
                MajlisGroup majlisGroup = new MajlisGroup();
                event.setEventCaption(rs.getString("EVENT_CAPTION"));

                majlisCategory.setCategoryTitle(rs.getString("CATEGORY_TITLE"));

                event.setEventDate(rs.getDate("EVENT_DATE") + "");
                event.setEventIconPath(validateGroupIconPath(rs.getString("EVENT_ICON_PATH")));
                event.setEventLocation(rs.getString("EVENT_LOCATION"));
                event.setEventLocationLat(rs.getString("EVENT_LOCATION_LAT"));
                event.setEventLocationLont(rs.getString("EVENT_LOCATION_LONT"));
                event.setEventId(rs.getInt("EVENT_ID"));
                event.setEventMessage(rs.getString("EVENT_MESSAGE"));

                mdCode.setCodeId(rs.getInt("EVENT_STATUS"));
                mdCode.setCodeMessage("CODE_MESSAGE");
                event.setEventStatus(mdCode);

                event.setEventTime(rs.getTime("EVENT_TIME") + "");

                majlisGroup.setGroupId(rs.getInt("GROUP_ID"));
                event.setGroupId(majlisGroup);
                event.setManualCount(rs.getInt("MANUAL_COUNT"));
                event.setEventCategory(majlisCategory);
                eventList.add(event);
            }

            responseData.setResponseData(eventList);
            result = 1;

        } catch (Exception ex) {
            result = 999;
            logger.info("Exception");
            ex.printStackTrace();
        } finally {

        }

        responseData.setResponseCode(result);
        return responseData;

    }

    public ResponseData getStatus(String type) {

        ResponseData rd = new ResponseData();

        DbCon db = new DbCon();

        Connection conn = db.getCon();

        ORMConnection orm = new ORMConnection();

        List<MajlisMdCode> mdList = new ArrayList<>();

        int result = -1;

        try {

            String sql = "SELECT U FROM MajlisMdCode U WHERE U.codeType= '" + type + "'  AND U.codeLocale='" + com.dmssw.majlis.config.AppParams.LOCALE + "' ORDER BY CODE_TYPE ASC";

            System.out.println("sql.." + sql);

            List<Object> resultSet = orm.hqlGetResults(sql);

            for (Object object : resultSet) {
                MajlisMdCode md = (MajlisMdCode) object;

                mdList.add(md);

            }
            result = 1;
            rd.setResponseData(mdList);

        } catch (Exception e) {
            e.printStackTrace();
            result = 999;

        }

        rd.setResponseCode(result);

        return rd;
    }

    public ResponseData getPublicLevels(String subType) {

        ResponseData rd = new ResponseData();

        DbCon db = new DbCon();

        Connection conn = db.getCon();

        ORMConnection orm = new ORMConnection();

        List<MajlisMdCode> mdList = new ArrayList<>();

        int result = -1;

        try {

            System.out.println(com.dmssw.orm.config.AppParams.LOCALE);
            String sql = "SELECT U FROM MajlisMdCode U WHERE U.codeSubType= '" + subType + "'  AND U.codeLocale='" + com.dmssw.majlis.config.AppParams.LOCALE + "' ORDER BY CODE_SUB_TYPE ASC";

            System.out.println("sql.." + sql);

            List<Object> resultSet = orm.hqlGetResults(sql);

            for (Object object : resultSet) {
                MajlisMdCode md = (MajlisMdCode) object;

                mdList.add(md);

            }
            result = 1;
            rd.setResponseData(mdList);

        } catch (Exception e) {
            e.printStackTrace();
            result = 999;

        } finally {

            try {
                conn.close();
            } catch (SQLException ex) {
                java.util.logging.Logger.getLogger(EventController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        rd.setResponseCode(result);

        return rd;
    }

    public ResponseData getAllEventsByGroupId(int groupId) {
        System.out.println("getAllEventsByGroupId method call.................");

        ResponseData rd = new ResponseData();
        ORMConnection conn = new ORMConnection();

        MajlisGroupEvent event = new MajlisGroupEvent();
        List<MajlisGroupEvent> eventList = new ArrayList<>();

        int result = -1;
        int i = 0;

        try {
            if (groupId != 0) {

                String selectQuery = "SELECT E FROM MajlisGroupEvent E "
                        + "WHERE E.groupId = '" + groupId + "'";

                System.out.println("query " + selectQuery);

                List<Object> list = conn.hqlGetResults(selectQuery);

                for (Object obj : list) {
                    i++;
                    event = (MajlisGroupEvent) obj;
                    eventList.add(event);
                    result = 1;

                }

                rd.setResponseData(eventList);

            } else {

                rd.setResponseData("group admin id not provided.");

            }
            result = 1;
        } catch (Exception e) {
            result = 999;
            rd.setResponseData("Error in getAllGroups method");
            e.printStackTrace();
        }
        rd.setResponseCode(result);
        return rd;
    }

    public ResponseData getCount() {

        ResponseData responseData = new ResponseData();
        int result = -1;
        int counts[] = new int[2];

        DbCon dbCon = new DbCon();
        Connection connection = dbCon.getCon();

        try {

            String sql = "SELECT (SELECT count(*) FROM majlis_group_event mg,majlis_md_code mmc WHERE mg.EVENT_STATUS=mmc.CODE_ID AND mmc.CODE_SEQ = 1) AS count1 ,"
                    + "(SELECT count(*) AS count1 FROM majlis_group_event mg,majlis_md_code mmc WHERE mg.EVENT_STATUS=mmc.CODE_ID AND mmc.CODE_SEQ = 2) AS count2 ";

            ResultSet rs = dbCon.search(connection, sql);

            while (rs.next()) {

                counts[0] = rs.getInt("count1");
                counts[1] = rs.getInt("count2");
            }

            responseData.setResponseData(counts);
        } catch (Exception ex) {
            result = 999;
            ex.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {

            }
        }

        responseData.setResponseCode(result);
        return responseData;
    }

}
