/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dmssw.majlis.controllers;

import com.dmssw.majlis.entities.CMSUser;
import com.dmssw.orm.models.MajlisCmsUsers;
import com.dmssw.orm.models.MajlisMobileUsers;
import com.dmssw.majlis.entities.Group;
import com.dmssw.majlis.entities.MobileUsers;
import com.dmssw.majlis.entities.User;
import com.dmssw.majlis.util.EmailSender;
import java.util.ArrayList;
import java.util.HashMap;
import com.dmssw.majlis.util.LdapUser;
import com.dmssw.majlis.util.ResponseData;
import com.dmssw.majlis.util.ResponseWrapper;
import com.dmssw.orm.config.AppParams;
import com.dmssw.orm.controllers.DbCon;
import com.dmssw.orm.controllers.Generators;
import com.dmssw.orm.controllers.ORMConnection;
import com.dmssw.orm.models.MajlisCategory;
import com.dmssw.orm.models.MajlisGroup;
import com.dmssw.orm.models.MajlisMdCode;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.mail.MessagingException;
import javax.transaction.RollbackException;
import javax.transaction.UserTransaction;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @version 1.0 / 20 March 2017
 * @author Nadishan Amarasekara
 */
@Stateless
@LocalBean
@TransactionManagement(TransactionManagementType.BEAN)
public class UserController {

    @Resource
    UserTransaction transaction;

    Logger logger;

    public UserController() {

        logger = AppParams.logger;

    }

    public ResponseWrapper validateCmsUser(LdapUser user, String system) {

        System.out.println("validateCmsUser method call.................");

        return LDAPWSController.login(user.getUserId(), user.getPassword(), "MajlisCMS");

    }
    
    
    public ResponseData getCmsUserGroup(String userId) {

        ResponseData rd = new ResponseData();

        DbCon db = new DbCon();

        Connection conn = db.getCon();
        int result = -1;

        Map<Object, Object> mp = new HashMap<>();

        List<MajlisGroup> listGroup = new ArrayList<>();

        List<MajlisGroup> listAllGroup = new ArrayList<>();

        try {

            String where = (userId == "all") ? "" : "WHERE g.GROUP_ADMIN='" + userId + "'";

            String sqlForGetUserGroup = "SELECT GROUP_ID "
                    + "FROM majlis_group g " + where;
       
            ResultSet rs = db.search(conn, sqlForGetUserGroup);

            while (rs.next()) {

                MajlisGroup group = new MajlisGroup();

                group.setGroupId(rs.getInt("GROUP_ID"));
                listGroup.add(group);
            }

            String sqlForAllGroups = "SELECT * "
                    + "FROM majlis_group g "
                    + "WHERE IFNULL(g.GROUP_ADMIN,'AA') IN( 'AA', '" + userId + "') "
                    + "ORDER BY g.GROUP_TITLE ASC";
            
            System.out.println("Query 2 "+sqlForAllGroups);

            ResultSet rs2 = db.search(conn, sqlForAllGroups);

            while (rs2.next()) {
                MajlisGroup g = new MajlisGroup();

                g.setGroupId(rs2.getInt("GROUP_ID"));
                g.setGroupTitle(rs2.getString("GROUP_TITLE"));
                String path = com.dmssw.majlis.config.AppParams.MEDIA_SERVER + "/" + rs2.getString("GROUP_ICON_PATH");
                g.setGroupIconPath(path);

                listAllGroup.add(g);
            }

            mp.put("allGroups", listAllGroup);
            mp.put("userGroups", listGroup);

            result = 1;

            rd.setResponseData(mp);
        } catch (Exception e) {

            result = 999;
            e.printStackTrace();
        }
        

        rd.setResponseCode(result);
        return rd;

    }
    
    
    public ResponseWrapper modifyCmsUser(CMSUser userinfo, String user_id, String room, String department, String branch, String countryCode, String division, String organaization, String system) {

        logger.info("addUser Method Call.............................");

        int result = -1;

        ResponseWrapper urw = null;
        String eMail = null;
        String password = null;
        String firname = null;
        String username = null;
        List<HashMap> userList = new ArrayList<>();

        DbCon db = new DbCon();

        Connection conn = db.getCon();

        try {

            logger.info("Call LDAP addUserWithGroups Method....");

            urw = LDAPWSController.modifyUser(userinfo.getUser(), user_id, room, department, branch, countryCode, division, organaization, system);

            logger.info("-----------------OutPut------------------------------------");

            ObjectMapper mapper = new ObjectMapper();

            logger.info(mapper.writeValueAsString(urw));

//            if (urw.getFlag().equals("1")) {

                String sqlForUpdate = "UPDATE majlis_cms_users "
                        + " SET USER_FULL_NAME=?, EMAIL_ADDRESS=?, NO_ALLOWED_NOTIFICATIONS=?, STATUS=?, USER_MODIFIED=? , ADDRESS=?, DATE_MODIFIED=NOW() "
                        + "WHERE USER_ID=?";

                PreparedStatement ps = db.prepare(conn, sqlForUpdate);

                String fullName = userinfo.getUser().getFirstName() + " " + userinfo.getUser().getLastName();

                ps.setString(1, fullName);

                ps.setString(2, userinfo.getUser().getEmail());
                ps.setInt(3, userinfo.getAllowedNotificationCnt());
                ps.setString(4, userinfo.getUser().getStatus());
                ps.setString(5, user_id);
                ps.setString(6, userinfo.getUser().getAddress());
                ps.setString(7, userinfo.getUser().getUserId());

                ps.executeUpdate();
                result = 1;
//            } else {
//                result = 999;
//            }

        } catch (IOException ex) {
            logger.error("Error Add User Method..:" + ex.getMessage());

            result = 999;

        } catch (Exception ex) {
            logger.error("Error Add User Method..:" + ex.getMessage());

            result = 999;
        }

        if (urw.getData() == null) {
            result = 999;
        } else {

            userList = urw.getData();

            logger.info("-----------E Mail Send-------------------");

            for (HashMap us : userList) {

                /**
                 * add user to table
                 */
                String sql = "INSERT INTO majlis_cms_users (DATE_INSERTED, EXPIRY_DATE,NO_ALLOWED_NOTIFICATIONS,STATUS,USER_FULL_NAME,  "
                        + "USER_ID, USER_INSERTED,EMAIL_ADDRESS) VALUES(NOW(),?,?,?,?,?,?,?)";

                try {

                    PreparedStatement pr = db.prepare(conn, sql);

                    String fullName = userinfo.getUser().getFirstName() + " " + userinfo.getUser().getLastName();

                    pr.setDate(1, userinfo.getExpiryDate());
                    pr.setInt(2, userinfo.getAllowedNotificationCnt());
                    pr.setInt(3, Integer.parseInt(userinfo.getUser().getStatus()));
                    pr.setString(4, fullName);
                    pr.setString(5, userinfo.getUser().getUserId());
                    pr.setString(6, user_id);
                    pr.setString(7, userinfo.getUser().getEmail());

                    pr.executeUpdate();

                    String sqlForDelete = "DELETE FROM majlis_user_groups WHERE userId='" + userinfo.getUser().getUserId() + "'";

                    PreparedStatement psDelete = db.prepare(conn, sqlForDelete);

                    psDelete.executeUpdate();

                    String sqlForUpdateTitle = "INSERT INTO majlis_user_groups (userId,title) VALUES(?,?)";

                    PreparedStatement ps = db.prepare(conn, sqlForUpdateTitle);

                    ps.setString(1, userinfo.getUser().getUserId());

                    ps.setString(2, "GroupAdminGrp");

                    ps.executeUpdate();

                } catch (Exception ex) {

                    logger.error("User cannot save to the database " + ex.getMessage());

                }

                eMail = (String) us.get("email");

                password = (String) us.get("password");
                firname = (String) us.get("firstName");
                username = (String) us.get("userId");

                logger.info("Password    " + password);

                try {

                    logger.info("Send Genarated Password to -" + eMail);

                    EmailSender.sendHTMLMail(eMail, password, username, firname);

                } catch (MessagingException ex) {
                    logger.error("Error in E-mail Sending........" + ex.getMessage());
                }

            }
            result = 1;
        }

        urw.setTotalRecords(result);

        logger.info("Successfully Return Value.............");
        logger.info("-----------------------------------------------------");
        return urw;
    }
    
     public ResponseData changeGroupUser(String userId, int groupId, boolean operation) {

        int result = -1;

        ResponseData rd = new ResponseData();

        DbCon db = new DbCon();

        Connection conn = db.getCon();

        try {

            String sqlForGrants = "UPDATE majlis_group "
                    + "SET GROUP_ADMIN=? "
                    + "WHERE GROUP_ID=?";

            PreparedStatement ps = db.prepare(conn, sqlForGrants);

            if (operation) {
                ps.setString(1, userId);

            } else {
                ps.setString(1, null);
            }

            ps.setInt(2, groupId);

            ps.executeUpdate();

            result = 1;
        } catch (Exception e) {
            result = 999;
            e.printStackTrace();
        }
        rd.setResponseCode(result);
        return rd;
    }


    public ResponseWrapper addUser(CMSUser userinfo, String user_id, String room, String department, String branch, String countryCode, String division, String organaization, String system) {

        logger.info("addUser Method Call.............................");

        int result = -1;

        ResponseWrapper urw = null;
        String eMail = null;
        String password = null;
        String firname = null;
        String username = null;
        List<HashMap> userList = new ArrayList<>();

        DbCon db = new DbCon();

        Connection conn = db.getCon();

        try {

            logger.info("Call LDAP addUserWithGroups Method....");

            urw = LDAPWSController.addUserWithGroups(userinfo.getUser(), user_id, room, department, branch, countryCode, division, organaization, system);

            logger.info("-----------------OutPut------------------------------------");

            ObjectMapper mapper = new ObjectMapper();

            logger.info(mapper.writeValueAsString(urw));

            result = 1;

        } catch (IOException ex) {
            logger.error("Error Add User Method..:" + ex.getMessage());

            result = 999;

        } catch (Exception ex) {
            logger.error("Error Add User Method..:" + ex.getMessage());

            result = 999;
        }

        if (urw.getData() == null) {
            result = 999;
        } else {

            userList = urw.getData();

            logger.info("-----------E Mail Send-------------------");

            for (HashMap us : userList) {

                /**
                 * add user to table
                 */
                String sql = "INSERT INTO majlis_cms_users (DATE_INSERTED, EXPIRY_DATE,NO_ALLOWED_NOTIFICATIONS,STATUS,USER_FULL_NAME,  "
                        + "USER_ID, USER_INSERTED,EMAIL_ADDRESS) VALUES(NOW(),?,?,?,?,?,?,?)";

                
                try {

                    PreparedStatement pr = db.prepare(conn, sql);

                    String fullName = userinfo.getUser().getFirstName() + " " + userinfo.getUser().getLastName();

                    pr.setDate(1, userinfo.getExpiryDate());
                    pr.setInt(2, userinfo.getAllowedNotificationCnt());
                    pr.setInt(3, Integer.parseInt(userinfo.getUser().getStatus()));
                    pr.setString(4, fullName);
                    pr.setString(5, userinfo.getUser().getUserId());
                    pr.setString(6, user_id);
                    pr.setString(7, userinfo.getUser().getEmail());

                    pr.executeUpdate();
                    
                    String sqlForDelete = "DELETE FROM majlis_user_groups WHERE userId='"+userinfo.getUser().getUserId()+"'";
                    
                    PreparedStatement psDelete = db.prepare(conn, sqlForDelete);
                    
                    psDelete.executeUpdate();
                    
                    
                    String sqlForUpdateTitle = "INSERT INTO majlis_user_groups (userId,title) VALUES(?,?)";
                    
                    PreparedStatement ps = db.prepare(conn, sqlForUpdateTitle);
                    
                    ps.setString(1, userinfo.getUser().getUserId());
                    
                    ps.setString(2, "GroupAdminGrp");
                    
                    ps.executeUpdate();

                } catch (Exception ex) {

                    logger.error("User cannot save to the database " + ex.getMessage());

                }

                eMail = (String) us.get("email");

                password = (String) us.get("password");
                firname = (String) us.get("firstName");
                username = (String) us.get("userId");

                logger.info("Password    " + password);

                try {

                    logger.info("Send Genarated Password to -" + eMail);

                    EmailSender.sendHTMLMail(eMail, password, username, firname);

                } catch (MessagingException ex) {
                    logger.error("Error in E-mail Sending........" + ex.getMessage());
                }

            }
            result = 1;
        }

        urw.setTotalRecords(result);

        logger.info("Successfully Return Value.............");
        logger.info("-----------------------------------------------------");
        return urw;
    }

    public ResponseWrapper getAllUsers(String userId,
            String firstName,
            String lastName,
            String commonName,
            String designation,
            String telephoneNumber,
            String email,
            String createdOn,
            String inactivedOn,
            String status,
            String extraparm,
            String start,
            String limit,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization,
            String system) {

        logger.info("getUsersMethod Call.....");

        ResponseWrapper urs = null;

        ArrayList<CMSUser> userInfo = new ArrayList<>();

        DbCon db = new DbCon();

        Connection conn = db.getCon();

        int result = -1;

        try {

            HashMap<String, String> searchParams = new HashMap<>();
            if (userId.equals("all")) {

            } else {
                searchParams.put("uid", userId);
            }

            if (firstName.equals("all")) {

            } else {
                searchParams.put("cn", firstName);
            }

            if (lastName.equals("all")) {

            } else {
                searchParams.put("sn", lastName);
            }

            if (commonName.equals("all")) {

            } else {
                searchParams.put("displayName", commonName);
            }

            if (designation.equals("all")) {

            } else {
                searchParams.put("title", designation);
            }

            if (telephoneNumber.equals("all")) {

            } else {
                searchParams.put("telephoneNumber", telephoneNumber);
            }

            if (email.equals("all")) {

            } else {
                searchParams.put("email", email);
            }

            if (createdOn.equals("all")) {

            } else {
                searchParams.put("createdOn", createdOn);
            }

            if (inactivedOn.equals("all")) {

            } else {
                searchParams.put("inactivedOn", inactivedOn);
            }

            if (status.equals("all")) {

            } else {
                searchParams.put("status", status);
            }

            if (extraparm.equals("all")) {

            } else {
                searchParams.put("extraParams", extraparm);
            }

            if (searchParams.isEmpty()) {
                searchParams.put("all", "all");
            }

            LDAPWSController ldap = new LDAPWSController();

            urs = ldap.getAllUsers(status, start, limit, searchParams, user_id, room, department, branch, countryCode, division, organaization, system);
            ArrayList<HashMap> userList = urs.getData();

            for (HashMap us : userList) {

                User u = new User();
                u.setUserId((String) us.get("userId"));
                u.setFirstName((String) us.get("firstName"));
                u.setLastName((String) us.get("lastName"));
                u.setCommonName((String) us.get("commonName"));
                u.setDesignation((String) us.get("designation"));
                u.setTelephoneNumber((String) us.get("telephoneNumber"));
                u.setEmail((String) us.get("email"));
                u.setCreatedOn((String) us.get("createdOn"));
                u.setInactivedOn((String) us.get("inactivedOn"));
                u.setStatus((String) us.get("status"));
                u.setExtraParams((String) us.get("extraParams"));
                u.setExtraParams((String) us.get("extraParams"));

                CMSUser cmsUser = new CMSUser();

                cmsUser.setUser(u);

                String sql = "SELECT EXPIRY_DATE,NO_ALLOWED_NOTIFICATIONS FROM majlis_cms_users WHERE USER_ID='" + userId + "'";

                
                ResultSet rs = db.search(conn, sql);

                while (rs.next()) {
                    cmsUser.setExpiryDate(rs.getDate("EXPIRY_DATE"));
                    cmsUser.setAllowedNotificationCnt(rs.getInt("NO_ALLOWED_NOTIFICATIONS"));
                }

                userInfo.add(cmsUser);
            }

            urs.setData(userInfo);
            result = 1;
        } catch (Exception ex) {
            logger.info("Error  :" + ex.getMessage());
            result = 999;
        }
        urs.setFlag(String.valueOf(result));

        logger.info("Successfully Return Value.............");

        logger.info("-----------------------------------------------------");
        return urs;
    }

    public ResponseWrapper getGroups(String start,
            String limit,
            String user_id,
            String room,
            String department,
            String branch,
            String countryCode,
            String division,
            String organaization, String system) {

        logger.info("get Groups Method Call...........");
        ResponseWrapper rw = null;
        String g = null;
        ArrayList<Group> gp = new ArrayList<>();
        try {
            logger.info("Call LDAP Get Groups.............");
            rw = LDAPWSController.getAllGroupsInSystem(start, limit, user_id, room, department, branch, countryCode, division, organaization, system);

        } catch (Exception ex) {
            logger.info("Error in get Groups Method :  " + ex.getMessage());
        }

        logger.info("Successfully Return Value.............");

        logger.info("-----------------------------------------------------");
        return rw;

    }

    public ResponseData getAllUserActiveStatus() {

        ResponseData rd = new ResponseData();
        
        DbCon db = new DbCon();

        Connection conn = db.getCon();
        
        ORMConnection orm = new ORMConnection();
        
        List<MajlisMdCode> mdList = new ArrayList<>();
        
        int result = -1;
        
        try {
            
            String sql = "SELECT U FROM MajlisMdCode U WHERE U.codeType='USERS' AND U.codeSubType='ACTIVE_STATUS' AND U.codeLocale='"+AppParams.LOCALE+"'";
            
            List<Object> resultSet = orm.hqlGetResults(sql);
            
            for (Object object : resultSet) {
                MajlisMdCode md = (MajlisMdCode) object;
                
                mdList.add(md);
                
            }
            result = 1;
            rd.setResponseData(mdList);
            
        } catch (Exception e) {
            e.printStackTrace();
            result =999;
            logger.error("getAllUserActiveStatus error "+e.getMessage());
        }
        
        rd.setResponseCode(result);
        
        return rd;
    }
    
    public ResponseData finalVerification(String userMobileNumber, String userVerification){
        
        DbCon db = new DbCon();
        
        Connection conn = db.getCon();
        
       int result = -1;
        
        ResponseData rd = new ResponseData();
        
        try {
            
            String sqlForVerification = "SELECT COUNT(*) CNT "
                    + "FROM tmp_mobileuser_code "
                    + "WHERE MOBILE_NUMBER = '"+userMobileNumber+"' "
                    + "AND CODE='"+userVerification+"'";
            
            ResultSet rs = db.search(conn, sqlForVerification);
            
            int count = 0;
            int count2 = 0;
            
            while (rs.next()) {                
                count = rs.getInt("CNT");
            }
            
            
            if(count >0){
                
                  String sqlForSelectCnt = "SELECT COUNT(*) CNT "
                        + "FROM majlis_mobile_users u "
                        + "WHERE u.MOBILE_USER_MOBILE_NO ='"+userMobileNumber+"'";
                  
                  
                  ResultSet rsCount = db.search(conn, sqlForSelectCnt);
                  
                  while (rsCount.next()) {
                      
                      count2 = rsCount.getInt("CNT");
                    
                }
                  
                  if(count2 >0){
                
                String sqlForSelect = "SELECT * "
                        + "FROM majlis_mobile_users u "
                        + "WHERE u.MOBILE_USER_MOBILE_NO ='"+userMobileNumber+"'";
                
                ResultSet rs2 = db.search(conn, sqlForSelect);
                
                while (rs2.next()) {      
                    
                   MajlisMobileUsers u = new MajlisMobileUsers();
                   
                   u.setMobileUserCity(rs2.getString("MOBILE_USER_CITY"));
                   u.setMobileUserCountry(rs2.getString("MOBILE_USER_COUNTRY"));
                   u.setMobileUserDateModified(rs2.getDate("MOBILE_USER_DATE_MODIFIED"));
                   u.setMobileUserDateRegistration(rs2.getDate("MOBILE_USER_DATE_REGISTRATION"));
                   u.setMobileUserDob(rs2.getDate("MOBILE_USER_DATE_REGISTRATION"));
                   u.setMobileUserEmail(rs2.getString("MOBILE_USER_EMAIL"));
                   u.setMobileUserId(rs2.getInt("MOBILE_USER_ID"));
                   u.setMobileUserMobileNo(rs2.getString("MOBILE_USER_MOBILE_NO"));
                   u.setMobileUserName(rs2.getString("MOBILE_USER_NAME"));
                   u.setMobileUserPassCode(rs2.getString("MOBILE_USER_PASS_CODE"));
                   u.setMobileUserProvince(rs2.getString("MOBILE_USER_PROVINCE"));
                   
                   rd.setResponseData(u);
                    
                }
                
                result =1;
            }else{
                result =2;
            }
                      
            
            }else{
                result =3;
            }
            
            
        } catch (Exception e) {
            result = 999;
        }
        
        rd.setResponseCode(result);
        
        return rd;
    }
    
    public ResponseData verifyMobileUserNumber(String userMobileNumber){
    
    
       DbCon db = new DbCon();
       
       int result = -1;
       
       Connection conn = db.getCon();
       
       ResponseData rd = new ResponseData();
       
         Generators gen = new Generators();
         
         String key = gen.generateDigest();
         
         try{
             
             transaction.begin();
       
        try {
     
                
                String sqlForDelExisting = "DELETE FROM tmp_mobileuser_code "
                        + "WHERE MOBILE_NUMBER='"+userMobileNumber+"'";
                
                PreparedStatement psDel = db.prepare(conn, sqlForDelExisting);
                
                psDel.executeUpdate();
                
                String insertTmp = "INSERT INTO tmp_mobileuser_code(MOBILE_NUMBER,CODE) VALUES(?,?)";
                
                PreparedStatement psTmp = db.prepare(conn, insertTmp);
                
                psTmp.setString(1, userMobileNumber);
                psTmp.setString(2, key);
                
                psTmp.executeUpdate();
              
            
            transaction.commit();
            result = 1;
            
            rd.setResponseData(key);
            
             
        } catch (Exception e) {
            
            result = 999;
            transaction.commit();
            
            e.printStackTrace();
            
        }
        
         }catch(Exception ex){
         
             result = 999;
             
             ex.printStackTrace();
         
         }
        
         rd.setResponseCode(result);
        
        return rd;
    
    }

    public ResponseWrapper getUserGroups(String userid, String user_id, String room, String department, String branch, String countryCode, String division, String organaization, String system) {
        logger.info("getUserGroups Call.....");
        ResponseWrapper urs = null;

        try {
            logger.info("Call LDAP getAllUserGroups Method.......");
            urs = LDAPWSController.getUserGroups(userid, user_id, room, department, branch, countryCode, division, organaization, system);

        } catch (Exception ex) {
            logger.info("Error in getUserGroups Method  :" + ex.getMessage());
        }

        logger.info("Successfully Return Value.............");

        logger.info("-----------------------------------------------------");
        return urs;
    }

////////////////////////////sandali//////////////////////////////////////////////////////////
//    public static ResponseWrapper getGroups(String start,
//            String limit,
//            String user_id,
//            String room,
//            String department,
//            String branch,
//            String countryCode,
//            String division,
//            String organaization, String system) {
//
//        System.out.println("get Groups Method Call...........");
//        ResponseWrapper rw = null;
//        String g = null;
//        ArrayList<Group> gp = new ArrayList<>();
//        try {
//            System.out.println("Call LDAP Get Groups.............");
//            rw = LDAPWSController.getAllGroupsInSystem(start, limit, user_id, room, department, branch, countryCode, division, organaization, system);
//
//        } catch (Exception ex) {
//            System.out.println("Error in get Groups Method :  " + ex.getMessage());
//        }
//
//        System.out.println("Successfully Return Value.............");
//
//        System.out.println("-----------------------------------------------------");
//        return rw;
//    }
//
//    
//    public ResponseWrapper getUsers(String key,
//            String value,
//            String status,
//            String start,
//            String limit,
//            String user_id,
//            String room,
//            String department,
//            String branch,
//            String countryCode,
//            String division,
//            String organaization, String system) {
//        
//        logger.info("getUsersMethod Call.....");
//        
//        ResponseWrapper urs = null;
//        
//        DbCon db = new DbCon();
//        
//        Connection conn = db.getCon();
//
//        ArrayList<CMSUser> userInfo = new ArrayList<>();
//        try {
//
//            urs = LDAPWSController.getAllUsers(status, start, limit, key, value, user_id, room, department, branch, countryCode, division, organaization, system);
//            ArrayList<HashMap> userList = urs.getData();
//
//            for (HashMap us : userList) {
//
//                
//                
//                User u = new User();
//                
//                String userId = (String) us.get("userId");
//                u.setUserId(userId);
//                u.setFirstName((String) us.get("firstName"));
//                u.setLastName((String) us.get("lastName"));
//                u.setCommonName((String) us.get("commonName"));
//                u.setDesignation((String) us.get("designation"));
//                u.setTelephoneNumber((String) us.get("telephoneNumber"));
//                u.setEmail((String) us.get("email"));
//                u.setCreatedOn((String) us.get("createdOn"));
//                u.setInactivedOn((String) us.get("inactivedOn"));
//                u.setStatus((String) us.get("status"));
//                u.setExtraParams((String) us.get("extraParams"));
//                u.setExtraParams((String) us.get("extraParams"));
//                
//                CMSUser cmsUser = new CMSUser();
//                
//                cmsUser.setUser(u);
//                
//                String sql = "SELECT EXPIRY_DATE,NO_ALLOWED_NOTIFICATIONS FROM majlis_cms_users WHERE USER_ID='"+userId+"'";
//                
//                ResultSet rs = db.search(conn, sql);
//                
//                while (rs.next()) {                    
//                    cmsUser.setExpiryDate(rs.getDate("EXPIRY_DATE"));
//                    cmsUser.setAllowedNotificationCnt(rs.getInt("NO_ALLOWED_NOTIFICATIONS"));
//                }
//                
//                userInfo.add(cmsUser);
//            }
//
//        } catch (Exception ex) {
//            logger.info("Error  :" + ex.getMessage());
//        }
//
//        logger.info("Successfully Return Value.............");
//
//        logger.info("-----------------------------------------------------");
//        return urs;
//    }
//    public static ResponseWrapper getUsers(String key,
//            String value,
//            String status,
//            String start,
//            String limit,
//            String user_id,
//            String room,
//            String department,
//            String branch,
//            String countryCode,
//            String division,
//            String organaization, String system) {
//        System.out.println("getUsersMethod Call.....");
//        ResponseWrapper urs = null;
//        //int totalrecode = 0;
//        List<MajlisCmsUsers> userInfo = new ArrayList<>();
//
//        try {
//
//            urs = LDAPWSController.getAllUsers(status, start, limit, key, value, user_id, room, department, branch, countryCode, division, organaization, system);
//            ArrayList<HashMap> userList = urs.getData();
//
//            for (HashMap us : userList) {
//
//                MajlisCmsUsers u = new MajlisCmsUsers();
//                userInfo.add(u);
//            }
//
//        } catch (Exception ex) {
//            System.out.println("Error  :" + ex.getMessage());
//        }
//
//        System.out.println("Successfully Return Value.............");
//
//        System.out.println("-----------------------------------------------------");
//        return urs;
//    }
    public ResponseData getAllMobileUsers(int start, int limit, String mobileUserId,
            String mobileUserMobileNo,
            String mobileUserName,
            String mobileUserEmail,
            String mobileUserDob,
            String mobileUserCountry,
            String mobileUserProvince,
            String mobileUserCity,
            String mobileUserDateRegistration) {

        DbCon db = new DbCon();

        Connection conn = db.getCon();

        ResponseData rd = new ResponseData();

        int result = -1;

        String where = mobileUserId = (mobileUserId.toUpperCase().equals("ALL")) ? " MOBILE_USER_ID = MOBILE_USER_ID " : " MOBILE_USER_ID = '" + mobileUserId + "'";
        where += (mobileUserMobileNo.toUpperCase().equals("ALL")) ? " AND IFNULL(MOBILE_USER_MOBILE_NO,0) =IFNULL(MOBILE_USER_MOBILE_NO,0) " : " AND MOBILE_USER_MOBILE_NO LIKE '%" + mobileUserMobileNo + "%' ";
        where += (mobileUserName.toUpperCase().equals("ALL")) ? " AND IFNULL(MOBILE_USER_NAME,'AA')=IFNULL(MOBILE_USER_NAME,'AA') " : " AND MOBILE_USER_NAME LIKE '%" + mobileUserName + "%' ";
        where += (mobileUserEmail.toUpperCase().equals("ALL")) ? " AND IFNULL(MOBILE_USER_EMAIL,'AA') = IFNULL(MOBILE_USER_EMAIL,'AA') " : " AND MOBILE_USER_EMAIL LIKE '%" + mobileUserEmail + "%' ";
        where += (mobileUserDob.toUpperCase().equals("ALL")) ? " AND IFNULL(MOBILE_USER_DOB,'AA')=IFNULL(MOBILE_USER_DOB,'AA') " : " AND MOBILE_USER_DOB = '" + mobileUserDob + "' ";
        where += (mobileUserCountry.toUpperCase().equals("ALL")) ? " AND IFNULL(MOBILE_USER_COUNTRY,'AA')=IFNULL(MOBILE_USER_COUNTRY,'AA') " : " AND MOBILE_USER_COUNTRY LIKE '%" + mobileUserCountry + "%' ";
        where += (mobileUserProvince.toUpperCase().equals("ALL")) ? " AND IFNULL(MOBILE_USER_PROVINCE,'AA')=IFNULL(MOBILE_USER_PROVINCE,'AA') " : " AND MOBILE_USER_PROVINCE LIKE '%" + mobileUserProvince + "%' ";
        where += (mobileUserCity.toUpperCase().equals("ALL")) ? " AND IFNULL(MOBILE_USER_CITY,'AA')=IFNULL(MOBILE_USER_CITY,'AA') " : " AND MOBILE_USER_CITY LIKE '%" + mobileUserCity + "%' ";
        where += (mobileUserDateRegistration.toUpperCase().equals("ALL")) ? " AND IFNULL(MOBILE_USER_DATE_REGISTRATION,0)=IFNULL(MOBILE_USER_DATE_REGISTRATION,0) " : " AND MOBILE_USER_DATE_REGISTRATION LIKE '%" + mobileUserDateRegistration + "%' ";

        where += " LIMIT " + limit + " OFFSET " + start;

        List<MobileUsers> userList = new ArrayList<>();

        try {

            String sql = "SELECT * "
                    + "FROM majlis_mobile_users WHERE " + where;

            ResultSet rs = db.search(conn, sql);

            while (rs.next()) {

                Connection conn2 = db.getCon();
                MobileUsers m = new MobileUsers();

                int userId = rs.getInt("MOBILE_USER_ID");

                m.setMobileUserId(userId);
                m.setMobileUserMobileNo(rs.getString("MOBILE_USER_MOBILE_NO"));
                m.setMobileUserName(rs.getString("MOBILE_USER_NAME"));
                m.setMobileUserEmail(rs.getString("MOBILE_USER_EMAIL"));
                m.setMobileUserDob(rs.getDate("MOBILE_USER_DOB"));
                m.setMobileUserCountry(rs.getString("MOBILE_USER_COUNTRY"));
                m.setMobileUserProvince(rs.getString("MOBILE_USER_PROVINCE"));
                m.setMobileUserCity(rs.getString("MOBILE_USER_CITY"));
                m.setMobileUserDateRegistration(rs.getDate("MOBILE_USER_DATE_REGISTRATION"));

                String sqlForCategory = "SELECT C.CATEGORY_ID,C.CATEGORY_TITLE "
                        + "FROM majlis_mobile_users_category UC, majlis_category C "
                        + "WHERE UC.CATEGORY_ID = C.CATEGORY_ID"
                        + " AND UC.USER_ID = '" + userId + "'";

                ResultSet rs2 = db.search(conn2, sqlForCategory);

                List<MajlisCategory> catList = new ArrayList<>();

                while (rs2.next()) {

                    MajlisCategory cat = new MajlisCategory();

                    cat.setCategoryId(rs2.getInt("CATEGORY_ID"));
                    cat.setCategoryTitle(rs2.getString("CATEGORY_TITLE"));

                    catList.add(cat);

                }

                m.setMajlisCategoryList(catList);
                
                 List<MajlisGroup> groupList = new ArrayList<>();
                 
                 String sqlForGetGroups = "SELECT g.*  " +
                  "FROM majlis_group g, majlis_mobile_user_group ug " +
                  "WHERE g.GROUP_ID = ug.GROUP_ID " +
                  "AND ug.USER_ID='"+userId+"'";
                 
                    System.out.println("sql for group "+sqlForGetGroups);
                 
                    ResultSet rs3 = db.search(conn2, sqlForGetGroups);
                    
                    while (rs3.next()) {  
                        
                        MajlisGroup g = new MajlisGroup();
                        
                        g.setGroupId(rs3.getInt("GROUP_ID"));
                        g.setGroupTitle(rs3.getString("GROUP_TITLE"));
                        
                        groupList.add(g);
                        
                    }
                
                    m.setMajlisGroupList(groupList);
                
                userList.add(m);

                conn2.close();
            }

            rd.setResponseData(userList);
            result = 1;

        } catch (Exception e) {
            result = 999;

            logger.error("Error in getAllMobileUsers " + e.getMessage());

        }

        rd.setResponseCode(result);
        return rd;
    }
    
    
    public ResponseData getAllMobileUserByGroupAdmin(String adminUserId, int start, int limit){
        
        int result = -1;
        
        List<MobileUsers> userList = new ArrayList<>();
        
        DbCon db = new DbCon();
        
        Connection conn = db.getCon();
        
        ResponseData rd = new ResponseData();
        
        try{
       
            
            String sqlForCategory = "SELECT title "
                    + "FROM majlis_user_groups "
                    + " WHERE userId='"+adminUserId+"'";
            
            
            ResultSet rsUserGroupTitile = db.search(conn, sqlForCategory);
            String titile ="SuperAdminGrp";
            while (rsUserGroupTitile.next()) {                
                titile = rsUserGroupTitile.getString("title");
            }
            
            
            if(titile == "SuperAdminGrp"){
                
                System.out.println("User is super admin");
                
                rd= this.getAllMobileUsers(start, limit,"all", "all", "all", "all", "all", "all", "all", "all", "all");
                
            }else{
                
                
                  System.out.println("User is group admin");
                
                String sqlForGrupUsers = " SELECT mu.* " +
             "FROM majlis_group g, majlis_mobile_user_group ug, majlis_mobile_users mu " +
             "WHERE ug.GROUP_ID = g.GROUP_ID " +
             "AND mu.MOBILE_USER_ID = ug.USER_ID " +
             "AND g.GROUP_ADMIN = '"+adminUserId+"' LIMIT "+start + ", "+ limit;
                
                ResultSet rs = db.search(conn, sqlForGrupUsers);
                
                
                while (rs.next()) {                    
                       Connection conn2 = db.getCon();
                MobileUsers m = new MobileUsers();

                int userId = rs.getInt("MOBILE_USER_ID");

                m.setMobileUserId(userId);
                m.setMobileUserMobileNo(rs.getString("MOBILE_USER_MOBILE_NO"));
                m.setMobileUserName(rs.getString("MOBILE_USER_NAME"));
                m.setMobileUserEmail(rs.getString("MOBILE_USER_EMAIL"));
                m.setMobileUserDob(rs.getDate("MOBILE_USER_DOB"));
                m.setMobileUserCountry(rs.getString("MOBILE_USER_COUNTRY"));
                m.setMobileUserProvince(rs.getString("MOBILE_USER_PROVINCE"));
                m.setMobileUserCity(rs.getString("MOBILE_USER_CITY"));
                m.setMobileUserDateRegistration(rs.getDate("MOBILE_USER_DATE_REGISTRATION"));

                String sqlForCategory2 = "SELECT C.CATEGORY_ID,C.CATEGORY_TITLE "
                        + "FROM majlis_mobile_users_category UC, majlis_category C "
                        + "WHERE UC.CATEGORY_ID = C.CATEGORY_ID"
                        + " AND UC.USER_ID = '" + userId + "'";

                ResultSet rs2 = db.search(conn2, sqlForCategory2);

                List<MajlisCategory> catList = new ArrayList<>();

                while (rs2.next()) {

                    MajlisCategory cat = new MajlisCategory();

                    cat.setCategoryId(rs2.getInt("CATEGORY_ID"));
                    cat.setCategoryTitle(rs2.getString("CATEGORY_TITLE"));

                    catList.add(cat);

                }

                m.setMajlisCategoryList(catList);
                
                 List<MajlisGroup> groupList = new ArrayList<>();
                 
                 String sqlForGetGroups = "SELECT g.*  " +
                  "FROM majlis_group g, majlis_mobile_user_group ug " +
                  "WHERE g.GROUP_ID = ug.GROUP_ID " +
                  "AND ug.USER_ID='"+userId+"'";
                 
                 
                    ResultSet rs3 = db.search(conn2, sqlForGetGroups);
                    
                    while (rs3.next()) {  
                        
                        MajlisGroup g = new MajlisGroup();
                        
                        g.setGroupId(rs3.getInt("GROUP_ID"));
                        g.setGroupTitle(rs3.getString("GROUP_TITLE"));
                        
                        groupList.add(g);
                        
                    }
                
                    m.setMajlisGroupList(groupList);
                
                userList.add(m);

                conn2.close();
                
                }
                
                rd.setResponseData(userList);
               result =1;
            }
            
            result = 1;
            conn.close();
        }catch(Exception ex){
            result = 999;
        
            ex.printStackTrace();
            
        }
    
    return rd;
    
    }

    public ResponseData createMobileUser(MobileUsers user) {

        System.out.println("Method createMobileUser call....");

        DbCon db = new DbCon();
        Connection conn = db.getCon();
        int result = -1;
        ResponseData rd = new ResponseData();

        try {

            try {
                
                String sqlForCheckMobileNo = "SELECT COUNT(*) AS CNT FROM majlis_mobile_users WHERE MOBILE_USER_MOBILE_NO='"+user.getMobileUserMobileNo()+"'";
                
                ResultSet rs = db.search(conn, sqlForCheckMobileNo);
               
                int cnt = 0;
                
                while (rs.next()) {                    
                    cnt = rs.getInt("CNT");
                }
                
                if(cnt == 0){

                transaction.begin();

                MajlisMobileUsers mob = new MajlisMobileUsers();

                mob.setMobileUserCity(user.getMobileUserCity());
                mob.setMobileUserCountry(user.getMobileUserCountry());
                mob.setMobileUserDob(user.getMobileUserDob());
                mob.setMobileUserEmail(user.getMobileUserEmail());
                mob.setMobileUserMobileNo(user.getMobileUserMobileNo());
                mob.setMobileUserName(user.getMobileUserName());
                mob.setMobileUserProvince(user.getMobileUserProvince());

                String prepareSql = "INSERT INTO majlis_mobile_users (MOBILE_USER_MOBILE_NO,MOBILE_USER_NAME,MOBILE_USER_EMAIL,MOBILE_USER_DOB,MOBILE_USER_COUNTRY,"
                        + " MOBILE_USER_PROVINCE,MOBILE_USER_CITY,MOBILE_USER_DATE_REGISTRATION,MOBILE_USER_PASS_CODE) "
                        + " VALUES(?,?,?,?,?,?,?,CURRENT_TIMESTAMP,?)";

                PreparedStatement ps = db.prepareAutoId(conn, prepareSql);

                Generators gen = new Generators();

                ps.setString(1, user.getMobileUserMobileNo());
                ps.setString(2, user.getMobileUserName());
                ps.setString(3, user.getMobileUserEmail());
                ps.setDate(4, user.getMobileUserDob());
                ps.setString(5, user.getMobileUserCountry());
                ps.setString(6, user.getMobileUserProvince());
                ps.setString(7, user.getMobileUserCity());
                ps.setString(8, gen.generateDigest());

                db.executePreparedStatement(conn, ps);

                int lastInsertedUserId = 0;

                ResultSet rsAuto = ps.getGeneratedKeys();

                while (rsAuto.next()) {
                    lastInsertedUserId = rsAuto.getInt(1);
                }

                String sqlForCat = "INSERT INTO majlis_mobile_users_category (USER_ID,CATEGORY_ID) VALUES(?,?)";

                PreparedStatement psCat = db.prepare(conn, sqlForCat);

                for (MajlisCategory mcat : user.getMajlisCategoryList()) {

                    psCat.setInt(1, lastInsertedUserId);
                    psCat.setInt(2, mcat.getCategoryId());

                    psCat.addBatch();

                }
                
                psCat.executeBatch();
                
                
                String sqlForGrp = "INSERT INTO majlis_mobile_user_group (USER_ID,GROUP_ID) VALUES(?,?)";
                
                String sqlForGrpStatus = "INSERT INTO tmp_group_request (GROUP_ID,STATUS,USER_ID) VALUES(?,?,?)";

                PreparedStatement psGrp = db.prepare(conn, sqlForGrp);
                
                PreparedStatement psGrpStatus = db.prepare(conn, sqlForGrpStatus);

                for (MajlisGroup mgrp : user.getMajlisGroupList()) {
                    
                    DbCon db2 = new DbCon();
                    
                    Connection conn2 = db2.getCon();
                    
                    String sqlForPrivate = "SELECT COUNT(*) CNT  " +
                                "FROM majlis_group g " +
                                "WHERE g.GROUP_PUBLIC_LEVEL = 5 " +
                                "AND g.GROUP_ID ='"+mgrp.getGroupId()+"'  ";
                    
                    
                    ResultSet rsPrivate = db2.search(conn2, sqlForPrivate);
                    
                     
                    
                    int count = 0;
                    
                    while (rsPrivate.next()) {                        
                        count = rsPrivate.getInt("CNT");
                    }
                    
                    if(count == 0){
                        
                        
                        psGrp.setInt(1, lastInsertedUserId);
                        psGrp.setInt(2, mgrp.getGroupId());
                        
                        psGrp.addBatch();
                        
                    }else{
                          
                        psGrpStatus.setInt(1, mgrp.getGroupId());
                        psGrpStatus.setInt(2, 1);
                        psGrpStatus.setInt(3, lastInsertedUserId);
                       psGrpStatus.addBatch(); 
                    }

                    conn2.close();
                }

                psGrpStatus.executeBatch();
                psGrp.executeBatch();
                
                result = lastInsertedUserId;

                transaction.commit();
                
                
            }else{
                    result = 200;
            }

            } catch (SQLException e) {
                result = 999;
                transaction.rollback();
                e.printStackTrace();
            } catch (RollbackException ex) {
                result = 999;
                transaction.rollback();
                System.out.println("EJB Transaction rollback on method createMobileUser........");
                ex.printStackTrace();
            }

        } catch (Exception e) {

            e.printStackTrace();
        }
   
        rd.setResponseCode(result);
        
        return rd;
    }
    

    public ResponseData modifyMobileUser(MobileUsers user) {
        System.out.println("Method modifyMobileUser call....");

        DbCon db = new DbCon();
        Connection conn = db.getCon();

        ResponseData rd = new ResponseData();
        
        try{
            
            transaction.begin();

        try {

            String updateQuery = "Update majlis_mobile_users SET"
                    + "  MOBILE_USER_MOBILE_NO = ?"
                    + "  ,MOBILE_USER_NAME = ?"
                    + "  ,MOBILE_USER_EMAIL = ?"
                    + "  ,MOBILE_USER_DOB =  ?"
                    + "  ,MOBILE_USER_COUNTRY = ?"
                    + "  ,MOBILE_USER_PROVINCE = ?"
                    + "  ,MOBILE_USER_CITY = ?"
                    + "  ,MOBILE_USER_DATE_MODIFIED = CURRENT_TIMESTAMP"
                    + " WHERE MOBILE_USER_ID = ?";

            System.out.println("updateQuery:" + updateQuery);

            PreparedStatement psUpdate = db.prepare(conn, updateQuery);

            psUpdate.setString(1, user.getMobileUserMobileNo());
            psUpdate.setString(2, user.getMobileUserName());
            psUpdate.setString(3, user.getMobileUserEmail());;
            psUpdate.setDate(4, user.getMobileUserDob());
            psUpdate.setString(5, user.getMobileUserCountry());
            psUpdate.setString(6, user.getMobileUserProvince());
            psUpdate.setString(7, user.getMobileUserCity());
            psUpdate.setInt(8, user.getMobileUserId());

            psUpdate.executeUpdate();
            System.out.println("modifyMobileUser.................");

            String deleteQuery = "Delete from majlis_mobile_users_category where USER_ID ='" + user.getMobileUserId() + "'";

            db.save(conn, deleteQuery);
            
            conn.close();

            List<MajlisCategory> li = user.getMajlisCategoryList();
            
            DbCon db2 = new DbCon();
            
            Connection conn2 = db2.getCon();

            for (MajlisCategory majlisCategory : li) {

                String insertSql = "INSERT INTO majlis_mobile_users_category (USER_ID,CATEGORY_ID) "
                        + " VALUES(?,?)";

                PreparedStatement ps = db2.prepare(conn2, insertSql);

                ps.setInt(1, user.getMobileUserId());
                ps.setInt(2, majlisCategory.getCategoryId());

                db.executePreparedStatement(conn, ps);

            }
            
            conn2.close();
            
             DbCon db3 = new DbCon();
            
            Connection conn3 = db3.getCon();
            
            String deleteGroups  = "DELETE FROM majlis_mobile_user_group WHERE USER_ID='"+user.getMobileUserId()+"'";
            
            PreparedStatement ps0 = db3.prepare(conn3, deleteGroups);
            
            ps0.executeUpdate();
            
            List<MajlisGroup> groupList= user.getMajlisGroupList();
           
            
            for (MajlisGroup majlisGroup : groupList) {
                
                String sqlForInsertUser = "INSERT INTO majlis_mobile_user_group (GROUP_ID,USER_ID) VALUES(?,?)";
                
                PreparedStatement psu = db3.prepare(conn3, sqlForInsertUser);
                
                psu.setInt(1, majlisGroup.getGroupId());
                psu.setInt(2, user.getMobileUserId());
                
                psu.executeUpdate();
                
            }
            
            conn3.close();
                    
            rd.setResponseCode(1);

            conn.close();
            transaction.commit();
        } catch (Exception e) {
            
            transaction.rollback();
            
            rd.setResponseCode(999);

           e.printStackTrace();
        }
        
        
        
        }catch(Exception ex){
            
            rd.setResponseCode(999);
            logger.error("EJB Exception "+ex.getMessage());
        }
        return rd;
    }

    public ResponseData validateUserPasscode(int userId, String passcode) {

        System.out.println("Method createMobileUser call....");
        ResponseData rd = new ResponseData();
        DbCon db = new DbCon();
        Connection conn = db.getCon();

        try {
            String sqlSelectPasscode = "SELECT U.MOBILE_USER_PASS_CODE"
                    + " FROM majlis_mobile_users U WHERE U.MOBILE_USER_ID = '" + userId + "'";

            ResultSet rs = db.search(conn, sqlSelectPasscode);

            while (rs.next()) {

                if (rs.getString("MOBILE_USER_PASS_CODE").equals(passcode)) {
                    rd.setResponseData("passcode matched");
                    rd.setResponseCode("1");
                } else {
                    rd.setResponseData("passcode not matched");
                    rd.setResponseCode("2");
                }

            }
        } catch (Exception e) {
            System.out.println("Error in validateUserPasscode ");
            rd.setResponseData("Error in validateUserPasscode ");
            rd.setResponseCode("-1");
            e.printStackTrace();
        }
        return rd;
    }

    public ResponseData getUserById(int userId) {

        ResponseData rd = new ResponseData();
        int result = -1;
        int i = 0;

        DbCon db = new DbCon();
        Connection conn = db.getCon();

        MobileUsers mUser = new MobileUsers();

        try {

            System.out.println("user Id :" + userId);

            if (userId != -1) {
                System.out.println("user Id not equal -1:" + userId);
                String selectQuery = " Select m.MOBILE_USER_ID, m.MOBILE_USER_MOBILE_NO, m.MOBILE_USER_NAME,"
                        + " m.MOBILE_USER_EMAIL, m.MOBILE_USER_DOB, m.MOBILE_USER_COUNTRY, "
                        + "m.MOBILE_USER_PROVINCE, m.MOBILE_USER_CITY, m.MOBILE_USER_DATE_REGISTRATION,"
                        + " m.MOBILE_USER_DATE_MODIFIED,m.MOBILE_USER_PASS_CODE"
                        + " from majlis_mobile_users m "
                        + "where m.MOBILE_USER_ID ='" + userId + "'";

                System.out.println("selectQuery:" + selectQuery);

                ResultSet rs = db.search(conn, selectQuery);

                while (rs.next()) {

                    mUser.setMobileUserId(rs.getInt("MOBILE_USER_ID"));
                    mUser.setMobileUserMobileNo(rs.getString("MOBILE_USER_MOBILE_NO"));
                    mUser.setMobileUserName(rs.getString("MOBILE_USER_NAME"));
                    mUser.setMobileUserEmail(rs.getString("MOBILE_USER_EMAIL"));
                    mUser.setMobileUserDob(rs.getDate("MOBILE_USER_DOB"));
                    mUser.setMobileUserCountry(rs.getString("MOBILE_USER_COUNTRY"));
                    mUser.setMobileUserProvince(rs.getString("MOBILE_USER_PROVINCE"));
                    mUser.setMobileUserCity(rs.getString("MOBILE_USER_CITY"));
                    mUser.setMobileUserDateRegistration(rs.getDate("MOBILE_USER_DATE_REGISTRATION"));
                    mUser.setMobileUserDateModified(rs.getDate("MOBILE_USER_DATE_MODIFIED"));
                    mUser.setMobileUserPassCode(rs.getString("MOBILE_USER_PASS_CODE"));

                    //String mUser.setMajlisCategoryList(majlisCategoryList);
                    //int catId = rs.getInt("CATEGORY_ID");
                    String selectCategory = "select c.CATEGORY_ID from majlis_mobile_users_category c "
                            + "where c.USER_ID ='" + mUser.getMobileUserId() + "'";

                    System.out.println("selectCategory" + selectCategory);

                    ResultSet rs2 = db.search(conn, selectCategory);

                    List<MajlisCategory> catList = new ArrayList<>();
                    while (rs2.next()) {

                        MajlisCategory cat = new MajlisCategory();

                        cat.setCategoryId(rs2.getInt("CATEGORY_ID"));
                        System.out.println("cat id:" + cat.getCategoryId());

                        catList.add(cat);
                    }

                    mUser.setMajlisCategoryList(catList);
                    
                    
                          List<MajlisGroup> groupList = new ArrayList<>();
                 
                 String sqlForGetGroups = "SELECT g.*  " +
                  "FROM majlis_group g, majlis_mobile_user_group ug " +
                  "WHERE g.GROUP_ID = ug.GROUP_ID " +
                  "AND ug.USER_ID='"+userId+"'";
                 
                 
                    ResultSet rs3 = db.search(conn, sqlForGetGroups);
                    
                    while (rs3.next()) {  
                        
                        MajlisGroup g = new MajlisGroup();
                        
                        g.setGroupId(rs3.getInt("GROUP_ID"));
                        g.setGroupTitle(rs3.getString("GROUP_TITLE"));
                        
                        groupList.add(g);
                        
                    }
                
                    mUser.setMajlisGroupList(groupList);
                

                }

                rd.setResponseData(mUser);
                rd.setResponseCode(1);

                //success
            } else {
                // group id not provided

                rd.setResponseData("Group id not provided.");
                rd.setResponseCode(2);
            }

        } catch (Exception e) {
            System.out.println("Error in getUserById ");
            rd.setResponseData("Error in getUserById ");
            rd.setResponseCode(999);
            e.printStackTrace();
        }
        rd.setResponseCode(result);
        return rd;
    }
    
    
     public ResponseWrapper resetUserPassword(String userid, String e_mail, String user_id, String room, String department, String branch, String countryCode, String division, String organaization, String system) {
      //  logger.info("resetUserPassword Method Call.....................");
        ResponseWrapper urw = null;
        String eMail = null;
        String password = null;
        String frsname = null;
        String username = null;
        try {

            urw = LDAPWSController.generateResetPassowrd(userid, e_mail, user_id, room, department, branch, countryCode, division, organaization, system);

            ArrayList<HashMap> userList = urw.getData();

            for (HashMap us : userList) {

                eMail = (String) us.get("email");
                password = (String) us.get("password");
                System.out.println("password:"+password);
                username = (String) us.get("userId");
               
            }
     
            System.out.println("Fname   " + frsname);
            try {
               
                EmailSender.sendHTMLMail(eMail, password,username,username); //username, username
            } catch (Exception ex) {
               // logger.info("Error in E-mail Sending........");
            }

        } catch (Exception ex) {
            //logger.info("Error resetUserPassword Method...........");
        }

        //logger.info("Successfully Return Value.............");

       // logger.info("-----------------------------------------------------");
        return urw;
    }


}
