package com.dmssw.majlis.entities;

import org.codehaus.jackson.annotate.JsonIgnore;

public class CMSUser {

    private java.sql.Date expiryDate;
    
    private int allowedNotificationCnt;   
    
    private User user;
    
    @JsonIgnore
    private String userId;
    @JsonIgnore
    private String username;

    
    public java.sql.Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(java.sql.Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    public int getAllowedNotificationCnt() {
        return allowedNotificationCnt;
    }

    public void setAllowedNotificationCnt(int allowedNotificationCnt) {
        this.allowedNotificationCnt = allowedNotificationCnt;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    
    
    
}
