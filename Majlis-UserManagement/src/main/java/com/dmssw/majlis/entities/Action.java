/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.dmssw.majlis.entities;

import java.util.ArrayList;


/**
 *
 * @author SDU
 */
public class Action {
    private String actionId;
    private String btnAction;
    private String btnHtmlId;
    private String btnIcon;
    private String btnUi;
    private String parentDN;
    private String dn;
    private ArrayList<Action> actions;

    /**
     * @return the actionId
     */
    public String getActionId() {
        return actionId;
    }

    /**
     * @param actionId the actionId to set
     */
    public void setActionId(String actionId) {
        this.actionId = actionId;
    }

    /**
     * @return the btnAction
     */
    public String getBtnAction() {
        return btnAction;
    }

    /**
     * @param btnAction the btnAction to set
     */
    public void setBtnAction(String btnAction) {
        this.btnAction = btnAction;
    }

    /**
     * @return the btnHtmlId
     */
    public String getBtnHtmlId() {
        return btnHtmlId;
    }

    /**
     * @param btnHtmlId the btnHtmlId to set
     */
    public void setBtnHtmlId(String btnHtmlId) {
        this.btnHtmlId = btnHtmlId;
    }

    /**
     * @return the btnIcon
     */
    public String getBtnIcon() {
        return btnIcon;
    }

    /**
     * @param btnIcon the btnIcon to set
     */
    public void setBtnIcon(String btnIcon) {
        this.btnIcon = btnIcon;
    }

    /**
     * @return the btnUi
     */
    public String getBtnUi() {
        return btnUi;
    }

    /**
     * @param btnUi the btnUi to set
     */
    public void setBtnUi(String btnUi) {
        this.btnUi = btnUi;
    }

    /**
     * @return the parentDN
     */
    public String getParentDN() {
        return parentDN;
    }

    /**
     * @param parentDN the parentDN to set
     */
    public void setParentDN(String parentDN) {
        this.parentDN = parentDN;
    }

    /**
     * @return the dn
     */
    public String getDn() {
        return dn;
    }

    /**
     * @param dn the dn to set
     */
    public void setDn(String dn) {
        this.dn = dn;
    }

    /**
     * @return the actions
     */
    public ArrayList<Action> getActions() {
        return actions;
    }

    /**
     * @param actions the actions to set
     */
    public void setActions(ArrayList<Action> actions) {
        this.actions = actions;
    }
}
