/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dmssw.majlis.entities;

import com.dmssw.orm.models.MajlisCategory;
import com.dmssw.orm.models.MajlisGroup;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Shanka
 */
public class MobileUsers {

    private Integer mobileUserId;

    private String mobileUserMobileNo;

    private String mobileUserName;

    private String mobileUserEmail;

    private java.sql.Date mobileUserDob;

    private String mobileUserCountry;

    private String mobileUserProvince;

    private String mobileUserCity;

    private Date mobileUserDateRegistration;

    private Date mobileUserDateModified;

    private String mobileUserPassCode;

    private List<MajlisCategory> majlisCategoryList;

    private List<MajlisGroup> majlisGroupList;

    public Integer getMobileUserId() {
        return mobileUserId;
    }

    public void setMobileUserId(Integer mobileUserId) {
        this.mobileUserId = mobileUserId;
    }

    public String getMobileUserMobileNo() {
        return mobileUserMobileNo;
    }

    public void setMobileUserMobileNo(String mobileUserMobileNo) {
        this.mobileUserMobileNo = mobileUserMobileNo;
    }

    public String getMobileUserName() {
        return mobileUserName;
    }

    public void setMobileUserName(String mobileUserName) {
        this.mobileUserName = mobileUserName;
    }

    public String getMobileUserEmail() {
        return mobileUserEmail;
    }

    public void setMobileUserEmail(String mobileUserEmail) {
        this.mobileUserEmail = mobileUserEmail;
    }

    public java.sql.Date getMobileUserDob() {
        return mobileUserDob;
    }

    public void setMobileUserDob(java.sql.Date mobileUserDob) {
        this.mobileUserDob = mobileUserDob;
    }

    public String getMobileUserCountry() {
        return mobileUserCountry;
    }

    public void setMobileUserCountry(String mobileUserCountry) {
        this.mobileUserCountry = mobileUserCountry;
    }

    public String getMobileUserProvince() {
        return mobileUserProvince;
    }

    public void setMobileUserProvince(String mobileUserProvince) {
        this.mobileUserProvince = mobileUserProvince;
    }

    public String getMobileUserCity() {
        return mobileUserCity;
    }

    public void setMobileUserCity(String mobileUserCity) {
        this.mobileUserCity = mobileUserCity;
    }

    public Date getMobileUserDateRegistration() {
        return mobileUserDateRegistration;
    }

    public void setMobileUserDateRegistration(Date mobileUserDateRegistration) {
        this.mobileUserDateRegistration = mobileUserDateRegistration;
    }

    public Date getMobileUserDateModified() {
        return mobileUserDateModified;
    }

    public void setMobileUserDateModified(Date mobileUserDateModified) {
        this.mobileUserDateModified = mobileUserDateModified;
    }

    public String getMobileUserPassCode() {
        return mobileUserPassCode;
    }

    public void setMobileUserPassCode(String mobileUserPassCode) {
        this.mobileUserPassCode = mobileUserPassCode;
    }

    public List<MajlisCategory> getMajlisCategoryList() {
        return majlisCategoryList;
    }

    public void setMajlisCategoryList(List<MajlisCategory> majlisCategoryList) {
        this.majlisCategoryList = majlisCategoryList;
    }

    public List<MajlisGroup> getMajlisGroupList() {
        return majlisGroupList;
    }

    public void setMajlisGroupList(List<MajlisGroup> majlisGroupList) {
        this.majlisGroupList = majlisGroupList;
    }

}
