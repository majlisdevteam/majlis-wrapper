/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.dmssw.majlis.entities;

import java.util.ArrayList;

/**
 *
 * @author SDU
 */
public class User {
    
    private String userId;
    private String firstName;
    private String lastName;
    private String commonName;
    private String designation;
    private String extraParams;
    private String nic;
    private String telephoneNumber;
    private String email;
    private String status;
    private String password;
    private String newPassword;
    private String createdOn;
    private String createdBy;
    private String modifiedOn;
    private String modifiedBy;
    private String inactivedOn;
    private String inactivedBy;

    //New attributes introduction begins
    private String remarks;
    private String address;
    private String loginStatus;
    private String loginFailAttempts;
    private String lastLoginAccessedOn;
    private String lastLoggedoutOn;
    private String lastLoggedIPAddress;
    private String lastLoggedInSystem;
    private String lastLoggedInOn;
    private String lastAccessedUri;
    private String activateBy;
    private String activateOn;
    private String lastGeneratedKey;
    private String pwdChangedBy;
    private String pwdChangedOn;
    private String pwdFailureAttemptsAll;
    private String pwdResetBy;
    private String pwdResetOn;
    private String securityAnswer;
    private String securityQuestion;
    private String accountUnlockedBy;
    private String accountUnlockedOn;
    private String accountUnlockTime;

    //New attributes introduction ends
    private String dn;
    
    private String organization;
    private String division;
    private String countryCode;
    private String branch;
    private String department;
    private String room;
    
   
    
    
    
//    private String returnFlag;
//    private String returnMsg;
    

    
    
    private ArrayList<Group> groups;

    private ArrayList<Function> functions;

    /**
     * @return the userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName the firstName to set
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName the lastName to set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the commonName
     */
    public String getCommonName() {
        return commonName;
    }

    /**
     * @param commonName the commonName to set
     */
    public void setCommonName(String commonName) {
        this.commonName = commonName;
    }

    /**
     * @return the designation
     */
    public String getDesignation() {
        return designation;
    }

    /**
     * @param designation the designation to set
     */
    public void setDesignation(String designation) {
        this.designation = designation;
    }

    /**
     * @return the extraParams
     */
    public String getExtraParams() {
        return extraParams;
    }

    /**
     * @param extraParams the extraParams to set
     */
    public void setExtraParams(String extraParams) {
        this.extraParams = extraParams;
    }

    /**
     * @return the nic
     */
    public String getNic() {
        return nic;
    }

    /**
     * @param nic the nic to set
     */
    public void setNic(String nic) {
        this.nic = nic;
    }

    /**
     * @return the telephoneNumber
     */
    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    /**
     * @param telephoneNumber the telephoneNumber to set
     */
    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the newPassword
     */
    public String getNewPassword() {
        return newPassword;
    }

    /**
     * @param newPassword the newPassword to set
     */
    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    /**
     * @return the createdOn
     */
    public String getCreatedOn() {
        return createdOn;
    }

    /**
     * @param createdOn the createdOn to set
     */
    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * @return the createdBy
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * @param createdBy the createdBy to set
     */
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    /**
     * @return the modifiedOn
     */
    public String getModifiedOn() {
        return modifiedOn;
    }

    /**
     * @param modifiedOn the modifiedOn to set
     */
    public void setModifiedOn(String modifiedOn) {
        this.modifiedOn = modifiedOn;
    }

    /**
     * @return the modifiedBy
     */
    public String getModifiedBy() {
        return modifiedBy;
    }

    /**
     * @param modifiedBy the modifiedBy to set
     */
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    /**
     * @return the inactivedOn
     */
    public String getInactivedOn() {
        return inactivedOn;
    }

    /**
     * @param inactivedOn the inactivedOn to set
     */
    public void setInactivedOn(String inactivedOn) {
        this.inactivedOn = inactivedOn;
    }

    /**
     * @return the inactivedBy
     */
    public String getInactivedBy() {
        return inactivedBy;
    }

    /**
     * @param inactivedBy the inactivedBy to set
     */
    public void setInactivedBy(String inactivedBy) {
        this.inactivedBy = inactivedBy;
    }

    /**
     * @return the remarks
     */
    public String getRemarks() {
        return remarks;
    }

    /**
     * @param remarks the remarks to set
     */
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return the loginStatus
     */
    public String getLoginStatus() {
        return loginStatus;
    }

    /**
     * @param loginStatus the loginStatus to set
     */
    public void setLoginStatus(String loginStatus) {
        this.loginStatus = loginStatus;
    }

    /**
     * @return the loginFailAttempts
     */
    public String getLoginFailAttempts() {
        return loginFailAttempts;
    }

    /**
     * @param loginFailAttempts the loginFailAttempts to set
     */
    public void setLoginFailAttempts(String loginFailAttempts) {
        this.loginFailAttempts = loginFailAttempts;
    }

    /**
     * @return the lastLoginAccessedOn
     */
    public String getLastLoginAccessedOn() {
        return lastLoginAccessedOn;
    }

    /**
     * @param lastLoginAccessedOn the lastLoginAccessedOn to set
     */
    public void setLastLoginAccessedOn(String lastLoginAccessedOn) {
        this.lastLoginAccessedOn = lastLoginAccessedOn;
    }

    /**
     * @return the lastLoggedoutOn
     */
    public String getLastLoggedoutOn() {
        return lastLoggedoutOn;
    }

    /**
     * @param lastLoggedoutOn the lastLoggedoutOn to set
     */
    public void setLastLoggedoutOn(String lastLoggedoutOn) {
        this.lastLoggedoutOn = lastLoggedoutOn;
    }

    /**
     * @return the lastLoggedIPAddress
     */
    public String getLastLoggedIPAddress() {
        return lastLoggedIPAddress;
    }

    /**
     * @param lastLoggedIPAddress the lastLoggedIPAddress to set
     */
    public void setLastLoggedIPAddress(String lastLoggedIPAddress) {
        this.lastLoggedIPAddress = lastLoggedIPAddress;
    }

    /**
     * @return the lastLoggedInSystem
     */
    public String getLastLoggedInSystem() {
        return lastLoggedInSystem;
    }

    /**
     * @param lastLoggedInSystem the lastLoggedInSystem to set
     */
    public void setLastLoggedInSystem(String lastLoggedInSystem) {
        this.lastLoggedInSystem = lastLoggedInSystem;
    }

    /**
     * @return the lastLoggedInOn
     */
    public String getLastLoggedInOn() {
        return lastLoggedInOn;
    }

    /**
     * @param lastLoggedInOn the lastLoggedInOn to set
     */
    public void setLastLoggedInOn(String lastLoggedInOn) {
        this.lastLoggedInOn = lastLoggedInOn;
    }

    /**
     * @return the lastAccessedUri
     */
    public String getLastAccessedUri() {
        return lastAccessedUri;
    }

    /**
     * @param lastAccessedUri the lastAccessedUri to set
     */
    public void setLastAccessedUri(String lastAccessedUri) {
        this.lastAccessedUri = lastAccessedUri;
    }

    /**
     * @return the activateBy
     */
    public String getActivateBy() {
        return activateBy;
    }

    /**
     * @param activateBy the activateBy to set
     */
    public void setActivateBy(String activateBy) {
        this.activateBy = activateBy;
    }

    /**
     * @return the activateOn
     */
    public String getActivateOn() {
        return activateOn;
    }

    /**
     * @param activateOn the activateOn to set
     */
    public void setActivateOn(String activateOn) {
        this.activateOn = activateOn;
    }

    /**
     * @return the lastGeneratedKey
     */
    public String getLastGeneratedKey() {
        return lastGeneratedKey;
    }

    /**
     * @param lastGeneratedKey the lastGeneratedKey to set
     */
    public void setLastGeneratedKey(String lastGeneratedKey) {
        this.lastGeneratedKey = lastGeneratedKey;
    }

    /**
     * @return the pwdChangedBy
     */
    public String getPwdChangedBy() {
        return pwdChangedBy;
    }

    /**
     * @param pwdChangedBy the pwdChangedBy to set
     */
    public void setPwdChangedBy(String pwdChangedBy) {
        this.pwdChangedBy = pwdChangedBy;
    }

    /**
     * @return the pwdChangedOn
     */
    public String getPwdChangedOn() {
        return pwdChangedOn;
    }

    /**
     * @param pwdChangedOn the pwdChangedOn to set
     */
    public void setPwdChangedOn(String pwdChangedOn) {
        this.pwdChangedOn = pwdChangedOn;
    }

    /**
     * @return the pwdFailureAttemptsAll
     */
    public String getPwdFailureAttemptsAll() {
        return pwdFailureAttemptsAll;
    }

    /**
     * @param pwdFailureAttemptsAll the pwdFailureAttemptsAll to set
     */
    public void setPwdFailureAttemptsAll(String pwdFailureAttemptsAll) {
        this.pwdFailureAttemptsAll = pwdFailureAttemptsAll;
    }

    /**
     * @return the pwdResetBy
     */
    public String getPwdResetBy() {
        return pwdResetBy;
    }

    /**
     * @param pwdResetBy the pwdResetBy to set
     */
    public void setPwdResetBy(String pwdResetBy) {
        this.pwdResetBy = pwdResetBy;
    }

    /**
     * @return the pwdResetOn
     */
    public String getPwdResetOn() {
        return pwdResetOn;
    }

    /**
     * @param pwdResetOn the pwdResetOn to set
     */
    public void setPwdResetOn(String pwdResetOn) {
        this.pwdResetOn = pwdResetOn;
    }

    /**
     * @return the securityAnswer
     */
    public String getSecurityAnswer() {
        return securityAnswer;
    }

    /**
     * @param securityAnswer the securityAnswer to set
     */
    public void setSecurityAnswer(String securityAnswer) {
        this.securityAnswer = securityAnswer;
    }

    /**
     * @return the securityQuestion
     */
    public String getSecurityQuestion() {
        return securityQuestion;
    }

    /**
     * @param securityQuestion the securityQuestion to set
     */
    public void setSecurityQuestion(String securityQuestion) {
        this.securityQuestion = securityQuestion;
    }

    /**
     * @return the accountUnlockedBy
     */
    public String getAccountUnlockedBy() {
        return accountUnlockedBy;
    }

    /**
     * @param accountUnlockedBy the accountUnlockedBy to set
     */
    public void setAccountUnlockedBy(String accountUnlockedBy) {
        this.accountUnlockedBy = accountUnlockedBy;
    }

    /**
     * @return the accountUnlockedOn
     */
    public String getAccountUnlockedOn() {
        return accountUnlockedOn;
    }

    /**
     * @param accountUnlockedOn the accountUnlockedOn to set
     */
    public void setAccountUnlockedOn(String accountUnlockedOn) {
        this.accountUnlockedOn = accountUnlockedOn;
    }

    /**
     * @return the accountUnlockTime
     */
    public String getAccountUnlockTime() {
        return accountUnlockTime;
    }

    /**
     * @param accountUnlockTime the accountUnlockTime to set
     */
    public void setAccountUnlockTime(String accountUnlockTime) {
        this.accountUnlockTime = accountUnlockTime;
    }

    /**
     * @return the dn
     */
    public String getDn() {
        return dn;
    }

    /**
     * @param dn the dn to set
     */
    public void setDn(String dn) {
        this.dn = dn;
    }

    /**
     * @return the organization
     */
    public String getOrganization() {
        return organization;
    }

    /**
     * @param organization the organization to set
     */
    public void setOrganization(String organization) {
        this.organization = organization;
    }

    /**
     * @return the division
     */
    public String getDivision() {
        return division;
    }

    /**
     * @param division the division to set
     */
    public void setDivision(String division) {
        this.division = division;
    }

    /**
     * @return the countryCode
     */
    public String getCountryCode() {
        return countryCode;
    }

    /**
     * @param countryCode the countryCode to set
     */
    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    /**
     * @return the branch
     */
    public String getBranch() {
        return branch;
    }

    /**
     * @param branch the branch to set
     */
    public void setBranch(String branch) {
        this.branch = branch;
    }

    /**
     * @return the department
     */
    public String getDepartment() {
        return department;
    }

    /**
     * @param department the department to set
     */
    public void setDepartment(String department) {
        this.department = department;
    }

    /**
     * @return the room
     */
    public String getRoom() {
        return room;
    }

    /**
     * @param room the room to set
     */
    public void setRoom(String room) {
        this.room = room;
    }

    /**
     * @return the groups
     */
    public ArrayList<Group> getGroups() {
        return groups;
    }

    /**
     * @param groups the groups to set
     */
    public void setGroups(ArrayList<Group> groups) {
        this.groups = groups;
    }

    /**
     * @return the functions
     */
    public ArrayList<Function> getFunctions() {
        return functions;
    }

    /**
     * @param functions the functions to set
     */
    public void setFunctions(ArrayList<Function> functions) {
        this.functions = functions;
    }
    
    

//    /**
//     * @return the returnFlag
//     */
//    public String getReturnFlag() {
//        return returnFlag;
//    }
//
//    /**
//     * @param returnFlag the returnFlag to set
//     */
//    public void setReturnFlag(String returnFlag) {
//        this.returnFlag = returnFlag;
//    }
//
//    /**
//     * @return the returnMsg
//     */
//    public String getReturnMsg() {
//        return returnMsg;
//    }
//
//    /**
//     * @param returnMsg the returnMsg to set
//     */
//    public void setReturnMsg(String returnMsg) {
//        this.returnMsg = returnMsg;
//    }

   
    
   
    
}
