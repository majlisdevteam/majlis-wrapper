/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.dmssw.majlis.entities;

import java.util.ArrayList;



/**
 *
 * @author SDU
 */
public class Function {
    
    private String functionId;
    private String functionName;
    private String menuHtmlId;
    private String menuAction;
    private String menuUi;
    private String menuIcon;
    private String parentDN;
    private String dn;
    private ArrayList<Action> actions;
    private ArrayList<Function> functions;

    /**
     * @return the functionId
     */
    public String getFunctionId() {
        return functionId;
    }

    /**
     * @param functionId the functionId to set
     */
    public void setFunctionId(String functionId) {
        this.functionId = functionId;
    }

    /**
     * @return the functionName
     */
    public String getFunctionName() {
        return functionName;
    }

    /**
     * @param functionName the functionName to set
     */
    public void setFunctionName(String functionName) {
        this.functionName = functionName;
    }

    /**
     * @return the menuHtmlId
     */
    public String getMenuHtmlId() {
        return menuHtmlId;
    }

    /**
     * @param menuHtmlId the menuHtmlId to set
     */
    public void setMenuHtmlId(String menuHtmlId) {
        this.menuHtmlId = menuHtmlId;
    }

    /**
     * @return the menuAction
     */
    public String getMenuAction() {
        return menuAction;
    }

    /**
     * @param menuAction the menuAction to set
     */
    public void setMenuAction(String menuAction) {
        this.menuAction = menuAction;
    }

    /**
     * @return the menuUi
     */
    public String getMenuUi() {
        return menuUi;
    }

    /**
     * @param menuUi the menuUi to set
     */
    public void setMenuUi(String menuUi) {
        this.menuUi = menuUi;
    }

    /**
     * @return the menuIcon
     */
    public String getMenuIcon() {
        return menuIcon;
    }

    /**
     * @param menuIcon the menuIcon to set
     */
    public void setMenuIcon(String menuIcon) {
        this.menuIcon = menuIcon;
    }

    /**
     * @return the parentDN
     */
    public String getParentDN() {
        return parentDN;
    }

    /**
     * @param parentDN the parentDN to set
     */
    public void setParentDN(String parentDN) {
        this.parentDN = parentDN;
    }

    /**
     * @return the dn
     */
    public String getDn() {
        return dn;
    }

    /**
     * @param dn the dn to set
     */
    public void setDn(String dn) {
        this.dn = dn;
    }

    /**
     * @return the actions
     */
    public ArrayList<Action> getActions() {
        return actions;
    }

    /**
     * @param actions the actions to set
     */
    public void setActions(ArrayList<Action> actions) {
        this.actions = actions;
    }

    /**
     * @return the functions
     */
    public ArrayList<Function> getFunctions() {
        return functions;
    }

    /**
     * @param functions the functions to set
     */
    public void setFunctions(ArrayList<Function> functions) {
        this.functions = functions;
    }
       
    
}
