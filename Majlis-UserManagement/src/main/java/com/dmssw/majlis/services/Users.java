/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dmssw.majlis.services;

import com.dmssw.majlis.config.AppParams;
import com.dmssw.majlis.controllers.LDAPWSController;
import com.dmssw.majlis.controllers.UserController;
import com.dmssw.majlis.entities.CMSUser;
import com.dmssw.majlis.entities.MobileUsers;
import com.dmssw.majlis.entities.User;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import com.dmssw.majlis.util.LdapUser;
import com.dmssw.majlis.util.ResponseData;
import com.dmssw.majlis.util.ResponseWrapper;
import com.dmssw.orm.models.MajlisMobileUsers;
import java.util.Date;
import javax.ejb.EJB;

/**
 *
 * This service describes about the user based services
 *
 * @version 1.0 / 20 March 2017
 * @author Nadishan Amarasekara
 */
@Path("/users")
public class Users {

    @EJB
    UserController controller;

    /**
     * Validate user with LDAP
     *
     * @param user
     * @param system
     * @return
     */
    @POST
    @Path("/validateCmsUser")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response validateCmsUser(LdapUser user,
            @HeaderParam("system") String system) {

        return Response.status(Response.Status.OK).entity(controller.validateCmsUser(user, system)).build();
    }

    @POST
    @Path("/changeGroupUser")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response changeGroupUser(@HeaderParam("userId") String userId,
            @HeaderParam("groupId") int groupId,
            @HeaderParam("operation") boolean operation,
            CMSUser user) {

        return Response.status(Response.Status.OK).entity(controller.changeGroupUser(userId, groupId, operation)).build();
    }
    
      
    @POST
    @Path("/modifyCmsUser")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response modifyCmsUser(CMSUser us,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization,
            @HeaderParam("system") String system) {

        return Response.status(Response.Status.OK).entity(controller.modifyCmsUser(us, userId, room, department, branch, countryCode, division, organization, system)).build();
    }

    @GET
    @Path("/getCmsUserGroup")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getCmsUserGroup(@QueryParam("userId") @DefaultValue("all") String userId) {
        return Response.ok().status(Response.Status.OK).entity(controller.getCmsUserGroup(userId)).build();
    }

    /**
     *
     * user add to LDAP
     *
     * @param us
     * @param userId
     * @param room
     * @param department
     * @param branch
     * @param countryCode
     * @param division
     * @param organization
     * @param system
     * @return
     */
    @POST
    @Path("/addUser")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response addUser(CMSUser us,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization,
            @HeaderParam("system") String system) {

        return Response.status(Response.Status.OK).entity(controller.addUser(us, userId, room, department, branch, countryCode, division, organization, system)).build();
    }

    @GET
    @Path("/getLdapGroups")
    @Produces({MediaType.APPLICATION_JSON})
    public ResponseWrapper getGroupsDetials(@QueryParam("start") @DefaultValue("all") String start,
            @QueryParam("limit") @DefaultValue("all") String limit,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization,
            @HeaderParam("system") String system) {
        return controller.getGroups(start, limit, userId, room, department, branch, countryCode, division, organization, system);
    }
    
    
   

    @GET
    @Path("/getUserGroups")
    @Produces({MediaType.APPLICATION_JSON})
    public ResponseWrapper getUserGroups(
            @QueryParam("userId") @DefaultValue("all") String userId,
            @HeaderParam("userId") String user_id,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization,
            @HeaderParam("system") String system) {

        return controller.getUserGroups(userId, userId, room, department, branch, countryCode, division, organization, system);
    }

    @GET
    @Path("/verifyMobileUserNumber")
    @Produces(MediaType.APPLICATION_JSON)
    public Response verifyMobileUserNumber(@QueryParam("userMobileNumber") @DefaultValue("all") String userMobileNumber) {

        return Response.status(Response.Status.OK).entity(controller.verifyMobileUserNumber(userMobileNumber)).build();
    }

    @GET
    @Path("/finalVerification")
    @Produces(MediaType.APPLICATION_JSON)
    public Response finalVerification(@QueryParam("userMobileNumber") @DefaultValue("all") String userMobileNumber,
            @QueryParam("userVerification") String userVerification) {

        return Response.status(Response.Status.OK).entity(controller.finalVerification(userMobileNumber, userVerification)).build();
    }

    @GET
    @Path("/getAllUsers")
    @Produces({MediaType.APPLICATION_JSON})
    public ResponseWrapper getAllUsers(
            @QueryParam("userId") @DefaultValue("all") String usId,
            @QueryParam("firstName") @DefaultValue("all") String firstName,
            @QueryParam("lastName") @DefaultValue("all") String lastName,
            @QueryParam("commonName") @DefaultValue("all") String commonName,
            @QueryParam("designation") @DefaultValue("all") String designation,
            @QueryParam("telephoneNumber") @DefaultValue("all") String telephoneNumber,
            @QueryParam("email") @DefaultValue("all") String email,
            @QueryParam("createdOn") @DefaultValue("all") String createdOn,
            @QueryParam("inactivedOn") @DefaultValue("all") String inactivedOn,
            @QueryParam("status") @DefaultValue("all") String status,
            @QueryParam("extraParams") @DefaultValue("all") String extraParams,
            @QueryParam("start") @DefaultValue("0") String start,
            @QueryParam("limit") @DefaultValue("25") String limit,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization,
            @HeaderParam("system") String system) {

        return controller.getAllUsers(usId, firstName, lastName, commonName, designation, telephoneNumber, email, createdOn, inactivedOn, status, extraParams, start, limit, userId, room, department, branch, countryCode, division, organization, system);
    }

    @GET
    @Path("/getAllUserActiveStatus")
    @Produces({MediaType.APPLICATION_JSON})
    public ResponseData getAllUserActiveStatus() {

        return controller.getAllUserActiveStatus();
    }

//    @GET
//    @Path("/getAllGroups")
//    @Produces({MediaType.APPLICATION_JSON})
//    public ResponseWrapper getGroupsDetials(@QueryParam("start") @DefaultValue("all") String start,
//            @QueryParam("limit") @DefaultValue("all") String limit,
//            @HeaderParam("userId") String userId,
//            @HeaderParam("room") String room,
//            @HeaderParam("department") String department,
//            @HeaderParam("branch") String branch,
//            @HeaderParam("countryCode") String countryCode,
//            @HeaderParam("division") String division,
//            @HeaderParam("organization") String organization,
//            @HeaderParam("system") String system) {
//        return UserController.getGroups(start, limit, userId, room, department, branch, countryCode, division, organization, system);
//    }
//    @GET
//    @Path("/getUserGroups")
//    @Produces({MediaType.APPLICATION_JSON})
//    public ResponseWrapper getUserGroups(@QueryParam("uid") @DefaultValue("all") String uid,
//            @HeaderParam("userId") String userId,
//            @HeaderParam("room") String room,
//            @HeaderParam("department") String department,
//            @HeaderParam("branch") String branch,
//            @HeaderParam("countryCode") String countryCode,
//            @HeaderParam("division") String division,
//            @HeaderParam("organization") String organization,
//            @HeaderParam("system") String system) {
//        return UserController.getUsers("", "", "", "", uid, userId, room, department, branch, countryCode, division, organization, system);
//    }
//    @GET
//    @Path("/getAllUsers")
//    @Produces({MediaType.APPLICATION_JSON})
//    public ResponseWrapper getAllUsers(@HeaderParam("system") String system) {
//        LDAPWSController ldapcontroller = new LDAPWSController();
//
//        return ldapcontroller.getAllUsers(system);
//    }
    @GET
    @Path("/getAllMobileUsers")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllMobileUsers(
            @QueryParam("start") @DefaultValue("0") int start,
            @QueryParam("limit") @DefaultValue("25") int limit,
            @QueryParam("mobileUserId") @DefaultValue("all") String mobileUserId,
            @QueryParam("mobileUserMobileNo") @DefaultValue("all") String mobileUserMobileNo,
            @QueryParam("mobileUserName") @DefaultValue("all") String mobileUserName,
            @QueryParam("mobileUserEmail") @DefaultValue("all") String mobileUserEmail,
            @QueryParam("mobileUserDob") @DefaultValue("all") String mobileUserDob,
            @QueryParam("mobileUserCountry") @DefaultValue("all") String mobileUserCountry,
            @QueryParam("mobileUserProvince") @DefaultValue("all") String mobileUserProvince,
            @QueryParam("mobileUserCity") @DefaultValue("all") String mobileUserCity,
            @QueryParam("mobileUserDateRegistration") @DefaultValue("all") String mobileUserDateRegistration
    ) {

        ResponseData data = controller.getAllMobileUsers(start, limit, mobileUserId,
                mobileUserMobileNo,
                mobileUserName,
                mobileUserEmail,
                mobileUserDob,
                mobileUserCountry,
                mobileUserProvince,
                mobileUserCity,
                mobileUserDateRegistration);

        return Response.status(Response.Status.OK).entity(data).build();
    }

    @GET
    @Path("getAllMobileUserByGroupAdmin")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllMobileUserByGroupAdmin(@QueryParam("adminUserId") @DefaultValue("all") String adminUserId,
            @QueryParam("start") @DefaultValue("0") int start,
            @QueryParam("limit") @DefaultValue("25") int limit) {

        return Response.status(Response.Status.OK).entity(controller.getAllMobileUserByGroupAdmin(adminUserId, start, limit)).build();
    }

    @POST
    @Path("/createMobileUser")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createMobileUser(MobileUsers user) {

        return Response.status(Response.Status.OK).entity(controller.createMobileUser(user)).build();
    }

    @POST
    @Path("/modifyMobileUser")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response modifyMobileUser(MobileUsers user) {

        return Response.status(Response.Status.OK).entity(controller.modifyMobileUser(user)).build();
    }

    @GET
    @Path("/validatePassCode")
    @Produces(MediaType.APPLICATION_JSON)
    public Response validatePassCode(@QueryParam("userId") @DefaultValue("-1") int userId,
            @QueryParam("passcode") @DefaultValue("-1") String passcode) {

        return Response.status(Response.Status.OK).entity(controller.validateUserPasscode(userId, passcode)).build();
    }

    @GET
    @Path("/getMobileUserById")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMobileUserById(@QueryParam("userId") @DefaultValue("-1") int userId) {

        return Response.status(Response.Status.OK).entity(controller.getUserById(userId)).build();
    }

    //Sandali
    @GET
    @Path("/resetUserPassword")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response resetUserPassword(@QueryParam("user") String user,
            @QueryParam("email") String email,
            @HeaderParam("userId") String userId,
            @HeaderParam("room") String room,
            @HeaderParam("department") String department,
            @HeaderParam("branch") String branch,
            @HeaderParam("countryCode") String countryCode,
            @HeaderParam("division") String division,
            @HeaderParam("organization") String organization,
            @HeaderParam("system") String system) {

        return Response.status(Response.Status.OK).entity(controller.resetUserPassword(user, email, userId, room, department, branch, countryCode, division, organization, system)).build();

    }

}
