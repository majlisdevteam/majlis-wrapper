/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dmssw.majlis.config;

import java.io.FileInputStream;
import java.util.Properties;

/**
 *
 * @version 1.0 / 20 March 2017
 * @author Nadishan Amarasekara
 */
public class AppParams {

    static Properties props = new Properties();
    static String wspath = System.getenv("WS_HOME");

    public static final String MEDIA_SERVER;

    static String pathset = wspath.replace("\\", "/");
    static String ippath = pathset + "/Majlis_Conn.properties";

    public static final String SYSTEM_NAME;
    public static final String CONNECTION_MYSQL;
    public static final String DRIVER;

    public static final String DB_USER_NAME;
    public static final String DB_PASSWORD;

    public static final String LDAP_PROTOCAL;
    public static final String LDAP_HOST;
    public static final String LDAP_SYSTEM_SID;
    public static final String LDAP_WS_PORT;
    public static final String LDAP_WS_CONTEXT_URL;
    public static final String LoggedInDN;

    public static final String MAIL_TRANSPORT;
    public static final String MAIL_CONNECT_IP;
    public static final String MAIL_SMTPS_HOST;
    public static final String MAIL_ADDRESS;
    public static final String MAIL_SMTPS_AUTH;
    public static final String MAIL_SENT_FROM;
    public static final String MAIL_SENT_FROM_PASSWORD;
    public static final String MAIL_PORT;

    public static final String LDAP_WS_URL;

    public static final int start;
    public static final int limit;

    static {

        System.out.println("Majlis property path " + ippath);

        try {

            props.load(new FileInputStream(ippath));

        } catch (Exception e) {
            System.out.println("Majlis cannot read properties.....");
            e.printStackTrace();
        }

        SYSTEM_NAME = props.getProperty("SYSTEM_NAME");
        MEDIA_SERVER = props.getProperty("MEDIA_SERVER");
        DRIVER = props.getProperty("DRIVER");
        CONNECTION_MYSQL = props.getProperty("CONNECTION_MYSQL");
        DB_USER_NAME = props.getProperty("DB_USER_NAME");
        DB_PASSWORD = props.getProperty("DB_PASSWORD");

        LDAP_PROTOCAL = props.getProperty("LDAP_PROTOCAL");
        LDAP_HOST = props.getProperty("LDAP_HOST");
        LDAP_SYSTEM_SID = props.getProperty("LDAP_SYSTEM_SID");
        LDAP_WS_PORT = props.getProperty("LDAP_WS_PORT");
        LDAP_WS_CONTEXT_URL = props.getProperty("LDAP_WS_CONTEXT_URL");
        LoggedInDN = props.getProperty("LoggedInDN");

        MAIL_TRANSPORT = props.getProperty("MAIL_TRANSPORT");
        MAIL_CONNECT_IP = props.getProperty("MAIL_CONNECT_IP");
        MAIL_SMTPS_HOST = props.getProperty("MAIL_SMTPS_HOST");
        MAIL_ADDRESS = props.getProperty("MAIL_ADDRESS");
        MAIL_SMTPS_AUTH = props.getProperty("MAIL_SMTPS_AUTH");
        MAIL_SENT_FROM = props.getProperty("MAIL_SENT_FROM");
        MAIL_SENT_FROM_PASSWORD = props.getProperty("MAIL_SENT_FROM_PASSWORD");
        MAIL_PORT = props.getProperty("MAIL_PORT");

        LDAP_WS_URL = LDAP_PROTOCAL + "://" + LDAP_HOST + ":" + LDAP_WS_PORT + "/" + LDAP_WS_CONTEXT_URL;
        start = 0;
        limit = 25;
    }

}
