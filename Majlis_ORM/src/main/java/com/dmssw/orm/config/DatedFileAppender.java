/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dmssw.orm.config;

import java.io.File;
import java.sql.Date;
import java.util.Calendar;
import org.apache.log4j.FileAppender;
import org.apache.log4j.spi.LoggingEvent;

/**
 *
 * @author GRTZ
 */
public class DatedFileAppender extends FileAppender {
    
       private String m_directory;
   private String m_prefix;
   private String m_suffix;
   private File m_path;
   private Calendar m_calendar;
   private long m_tomorrow;
   
   public DatedFileAppender()
   {
     String jbossPath = System.getenv("WS_HOME");
     String pathSep = jbossPath.replace("\\", "/");
     
     String log4jPath = pathSep + "/Log4J";
     
     this.m_directory = log4jPath;
     this.m_prefix = "tomcat.";
     this.m_suffix = ".log";
     this.m_path = null;
     this.m_calendar = null;
     this.m_tomorrow = 0L;
   }
   
   public DatedFileAppender(String s, String s1, String s2)
   {
     String jbossPath = System.getenv("WS_HOME");
     String pathSep = jbossPath.replace("\\", "/");
     
     String log4jPath = pathSep + "/Log4J";
     
     this.m_directory = log4jPath;
     this.m_prefix = "tomcat.";
     this.m_suffix = ".log";
     this.m_path = null;
     this.m_calendar = null;
     this.m_tomorrow = 0L;
     this.m_directory = s;
     this.m_prefix = s1;
     this.m_suffix = s2;
     activateOptions();
   }
   
   public String getDirectory()
   {
     return this.m_directory;
   }
   
   public void setDirectory(String s)
   {
     this.m_directory = s;
   }
   
   public String getPrefix()
   {
     return this.m_prefix;
   }
  
   public void setPrefix(String s)
   {
     this.m_prefix = s;
   }
   
   public String getSuffix()
   {
     return this.m_suffix;
   }
   
   public void setSuffix(String s)
   {
     this.m_suffix = s;
   }
   
   public void activateOptions()
   {
     if (this.m_prefix == null) {
       this.m_prefix = "";
     }
     if (this.m_suffix == null) {
       this.m_suffix = "";
     }
     if ((this.m_directory == null) || (this.m_directory.length() == 0)) {
       this.m_directory = ".";
     }
     this.m_path = new File(this.m_directory);
     if (!this.m_path.isAbsolute())
     {
       String s = System.getProperty("catalina.base");
       if (s != null) {
         this.m_path = new File(s, this.m_directory);
       }
     }
     this.m_path.mkdirs();
     if (this.m_path.canWrite()) {
       this.m_calendar = Calendar.getInstance();
     }
   }
   
   public void append(LoggingEvent loggingevent)
     {
     if (this.layout == null)
     {
       this.errorHandler.error("No layout set for the appender named [" + this.name + "].");
       return;
     }
     if (this.m_calendar == null)
     {
       this.errorHandler.error("Improper initialization for the appender named [" + this.name + "].");
      return;
     }
     long l = System.currentTimeMillis();
    if (l >= this.m_tomorrow)
     {
       this.m_calendar.setTime(new Date(l));
       String s = datestamp(this.m_calendar);
       tomorrow(this.m_calendar);
       this.m_tomorrow = this.m_calendar.getTime().getTime();
       File file = new File(this.m_path, this.m_prefix + s + this.m_suffix);
       this.fileName = file.getAbsolutePath();
       super.activateOptions();
     }
    if (this.qw == null)
     {
       this.errorHandler.error("No output stream or file set for the appender named [" + this.name + "].");
       return;
     }
     subAppend(loggingevent);
   }
   
   public static String datestamp(Calendar calendar)
   {
     int i = calendar.get(1);
     int j = calendar.get(2) + 1;
     int k = calendar.get(5);
     StringBuffer stringbuffer = new StringBuffer();
     if (i < 1000)
     {
       stringbuffer.append('0');
       if (i < 100)
       {
         stringbuffer.append('0');
         if (i < 10) {
           stringbuffer.append('0');
         }
       }
     }
     stringbuffer.append(Integer.toString(i));
     stringbuffer.append('-');
     if (j < 10) {
       stringbuffer.append('0');
     }
     stringbuffer.append(Integer.toString(j));
     stringbuffer.append('-');
    if (k < 10) {
       stringbuffer.append('0');
     }
    stringbuffer.append(Integer.toString(k));
     return stringbuffer.toString();
  }
  
   public static void tomorrow(Calendar calendar)
   {
     int i = calendar.get(1);
     int j = calendar.get(2);
    int k = calendar.get(5) + 1;
     calendar.clear();
     calendar.set(i, j, k);
   }
    
}
