/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dmssw.orm.config;

import java.io.FileInputStream;
import java.util.Properties;
import org.apache.log4j.Logger;

/**
 *
 * @author Nadishan Amarasekara
 */
public class AppParams {
    
    static Properties props = new Properties();
    static String wspath = System.getenv("WS_HOME");
    
    static String pathset = wspath.replace("\\", "/");
    static String ippath = pathset + "/Majlis_DB_Conn.properties";
    
    public static final String CONNECTION_MYSQL;
    public static final String DRIVER;
    public static final String DB_CONNECTION_TYPE;
    
    public static final String LOG_LEVEL;
    
    public static final String JNDI_DB;
    public static String systemName;
    public static String LOCALE;
    
    public static Logger logger;
    
    static {
        
        try {
            
            props.load(new FileInputStream(ippath));
            
        } catch (Exception e) {
            logger.error("AppParams error " + e.getMessage());
        }
        
        DRIVER = props.getProperty("DRIVER");
        DB_CONNECTION_TYPE = props.getProperty("DB_CONNECTION_TYPE");
        CONNECTION_MYSQL = props.getProperty("CONNECTION_MYSQL");
        JNDI_DB = props.getProperty("JNDI_DB_CONN");
        
        LOG_LEVEL = props.getProperty("LOG_LEVEL");
        
        LOCALE = props.getProperty("LOCALE");
        
        logger = new LogHelper().createLogger(systemName, wspath, LOG_LEVEL);
        
    }
}
