/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dmssw.orm.models;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Shanka
 */
@Entity
@Table(name = "majlis_notification")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MajlisNotification.findAll", query = "SELECT m FROM MajlisNotification m"),
    @NamedQuery(name = "MajlisNotification.findByNotificationId", query = "SELECT m FROM MajlisNotification m WHERE m.notificationId = :notificationId"),
    @NamedQuery(name = "MajlisNotification.findByNotificationSystemId", query = "SELECT m FROM MajlisNotification m WHERE m.notificationSystemId = :notificationSystemId"),
    @NamedQuery(name = "MajlisNotification.findByNotificationIconPath", query = "SELECT m FROM MajlisNotification m WHERE m.notificationIconPath = :notificationIconPath"),
    @NamedQuery(name = "MajlisNotification.findByNotificationMessage", query = "SELECT m FROM MajlisNotification m WHERE m.notificationMessage = :notificationMessage"),
    @NamedQuery(name = "MajlisNotification.findByDateInserted", query = "SELECT m FROM MajlisNotification m WHERE m.dateInserted = :dateInserted")})
public class MajlisNotification implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "NOTIFICATION_ID")
    private Integer notificationId;
    @Size(max = 50)
    @Column(name = "NOTIFICATION_SYSTEM_ID")
    private String notificationSystemId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2000)
    @Column(name = "NOTIFICATION_ICON_PATH")
    private String notificationIconPath;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5000)
    @Column(name = "NOTIFICATION_MESSAGE")
    private String notificationMessage;
    @Column(name = "DATE_INSERTED")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateInserted;
    @JoinColumn(name = "NOTIFICATION_EVENT_ID", referencedColumnName = "EVENT_ID")
    @ManyToOne(optional = false)
    private MajlisGroupEvent notificationEventId;
    @JoinColumn(name = "NOTIFICATION_GROUP_ID", referencedColumnName = "GROUP_ID")
    @ManyToOne(optional = false)
    private MajlisGroup notificationGroupId;
    @JoinColumn(name = "NOTIFICATION_STATUS", referencedColumnName = "CODE_ID")
    @ManyToOne(optional = false)
    private MajlisMdCode notificationStatus;
    @JoinColumn(name = "NOTIFICATION_USER_ID", referencedColumnName = "USER_ID")
    @ManyToOne(optional = false)
    private MajlisCmsUsers notificationUserId;

    public MajlisNotification() {
    }

    public MajlisNotification(Integer notificationId) {
        this.notificationId = notificationId;
    }

    public MajlisNotification(Integer notificationId, String notificationIconPath, String notificationMessage) {
        this.notificationId = notificationId;
        this.notificationIconPath = notificationIconPath;
        this.notificationMessage = notificationMessage;
    }

    public Integer getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(Integer notificationId) {
        this.notificationId = notificationId;
    }

    public String getNotificationSystemId() {
        return notificationSystemId;
    }

    public void setNotificationSystemId(String notificationSystemId) {
        this.notificationSystemId = notificationSystemId;
    }

    public String getNotificationIconPath() {
        return notificationIconPath;
    }

    public void setNotificationIconPath(String notificationIconPath) {
        this.notificationIconPath = notificationIconPath;
    }

    public String getNotificationMessage() {
        return notificationMessage;
    }

    public void setNotificationMessage(String notificationMessage) {
        this.notificationMessage = notificationMessage;
    }

    public Date getDateInserted() {
        return dateInserted;
    }

    public void setDateInserted(Date dateInserted) {
        this.dateInserted = dateInserted;
    }

    public MajlisGroupEvent getNotificationEventId() {
        return notificationEventId;
    }

    public void setNotificationEventId(MajlisGroupEvent notificationEventId) {
        this.notificationEventId = notificationEventId;
    }

    public MajlisGroup getNotificationGroupId() {
        return notificationGroupId;
    }

    public void setNotificationGroupId(MajlisGroup notificationGroupId) {
        this.notificationGroupId = notificationGroupId;
    }

    public MajlisMdCode getNotificationStatus() {
        return notificationStatus;
    }

    public void setNotificationStatus(MajlisMdCode notificationStatus) {
        this.notificationStatus = notificationStatus;
    }

    public MajlisCmsUsers getNotificationUserId() {
        return notificationUserId;
    }

    public void setNotificationUserId(MajlisCmsUsers notificationUserId) {
        this.notificationUserId = notificationUserId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (notificationId != null ? notificationId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MajlisNotification)) {
            return false;
        }
        MajlisNotification other = (MajlisNotification) object;
        if ((this.notificationId == null && other.notificationId != null) || (this.notificationId != null && !this.notificationId.equals(other.notificationId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.dmssw.orm.models.MajlisNotification[ notificationId=" + notificationId + " ]";
    }
    
}
