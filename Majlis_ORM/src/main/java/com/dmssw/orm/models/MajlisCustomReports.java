/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dmssw.orm.models;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Shanka
 */
@Entity
@Table(name = "majlis_custom_reports")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MajlisCustomReports.findAll", query = "SELECT m FROM MajlisCustomReports m"),
    @NamedQuery(name = "MajlisCustomReports.findByReportId", query = "SELECT m FROM MajlisCustomReports m WHERE m.reportId = :reportId"),
    @NamedQuery(name = "MajlisCustomReports.findByReportName", query = "SELECT m FROM MajlisCustomReports m WHERE m.reportName = :reportName"),
    @NamedQuery(name = "MajlisCustomReports.findByReportQuery", query = "SELECT m FROM MajlisCustomReports m WHERE m.reportQuery = :reportQuery")})
public class MajlisCustomReports implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "REPORT_ID")
    private Integer reportId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "REPORT_NAME")
    private String reportName;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5000)
    @Column(name = "REPORT_QUERY")
    private String reportQuery;

    public MajlisCustomReports() {
    }

    public MajlisCustomReports(Integer reportId) {
        this.reportId = reportId;
    }

    public MajlisCustomReports(Integer reportId, String reportName, String reportQuery) {
        this.reportId = reportId;
        this.reportName = reportName;
        this.reportQuery = reportQuery;
    }

    public Integer getReportId() {
        return reportId;
    }

    public void setReportId(Integer reportId) {
        this.reportId = reportId;
    }

    public String getReportName() {
        return reportName;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    public String getReportQuery() {
        return reportQuery;
    }

    public void setReportQuery(String reportQuery) {
        this.reportQuery = reportQuery;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (reportId != null ? reportId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MajlisCustomReports)) {
            return false;
        }
        MajlisCustomReports other = (MajlisCustomReports) object;
        if ((this.reportId == null && other.reportId != null) || (this.reportId != null && !this.reportId.equals(other.reportId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.dmssw.orm.models.MajlisCustomReports[ reportId=" + reportId + " ]";
    }
    
}
