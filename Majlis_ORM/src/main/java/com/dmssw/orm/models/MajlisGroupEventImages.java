/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dmssw.orm.models;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Shanka
 */
@Entity
@Table(name = "majlis_group_event_images")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MajlisGroupEventImages.findAll", query = "SELECT m FROM MajlisGroupEventImages m"),
    @NamedQuery(name = "MajlisGroupEventImages.findByEventImageId", query = "SELECT m FROM MajlisGroupEventImages m WHERE m.eventImageId = :eventImageId"),
    @NamedQuery(name = "MajlisGroupEventImages.findByEventImagePath", query = "SELECT m FROM MajlisGroupEventImages m WHERE m.eventImagePath = :eventImagePath"),
    @NamedQuery(name = "MajlisGroupEventImages.findByUserInserted", query = "SELECT m FROM MajlisGroupEventImages m WHERE m.userInserted = :userInserted"),
    @NamedQuery(name = "MajlisGroupEventImages.findByDateInserted", query = "SELECT m FROM MajlisGroupEventImages m WHERE m.dateInserted = :dateInserted")})
public class MajlisGroupEventImages implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "EVENT_IMAGE_ID")
    private Integer eventImageId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5000)
    @Column(name = "EVENT_IMAGE_PATH")
    private String eventImagePath;
    @Column(name = "USER_INSERTED")
    private Integer userInserted;
    @Column(name = "DATE_INSERTED")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateInserted;
    @JoinColumn(name = "EVENT_ID", referencedColumnName = "EVENT_ID")
    @ManyToOne(optional = false)
    private MajlisGroupEvent eventId;

    @Column(name = "EVENT_IMAGES_STATUS",nullable = true)
    private Integer eventImageStatus;

    public MajlisGroupEventImages() {
    }

    public MajlisGroupEventImages(Integer eventImageId) {
        this.eventImageId = eventImageId;
    }

    public MajlisGroupEventImages(Integer eventImageId, String eventImagePath) {
        this.eventImageId = eventImageId;
        this.eventImagePath = eventImagePath;
    }

    public Integer getEventImageId() {
        return eventImageId;
    }

    public void setEventImageId(Integer eventImageId) {
        this.eventImageId = eventImageId;
    }

    public String getEventImagePath() {
        return eventImagePath;
    }

    public void setEventImagePath(String eventImagePath) {
        this.eventImagePath = eventImagePath;
    }

    public Integer getUserInserted() {
        return userInserted;
    }

    public void setUserInserted(Integer userInserted) {
        this.userInserted = userInserted;
    }

    public Date getDateInserted() {
        return dateInserted;
    }

    public void setDateInserted(Date dateInserted) {
        this.dateInserted = dateInserted;
    }

    public MajlisGroupEvent getEventId() {
        return eventId;
    }

    public void setEventId(MajlisGroupEvent eventId) {
        this.eventId = eventId;
    }

    public Integer getEventImageStatus() {
        return eventImageStatus;
    }

    public void setEventImageStatus(Integer eventImageStatus) {
        this.eventImageStatus = eventImageStatus;
    }

    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (eventImageId != null ? eventImageId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MajlisGroupEventImages)) {
            return false;
        }
        MajlisGroupEventImages other = (MajlisGroupEventImages) object;
        if ((this.eventImageId == null && other.eventImageId != null) || (this.eventImageId != null && !this.eventImageId.equals(other.eventImageId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.dmssw.orm.models.MajlisGroupEventImages[ eventImageId=" + eventImageId + " ]";
    }
    

}
