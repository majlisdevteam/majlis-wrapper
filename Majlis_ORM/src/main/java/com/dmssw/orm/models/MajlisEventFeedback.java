/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dmssw.orm.models;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Shanka
 */
@Entity
@Table(name = "majlis_event_feedback")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MajlisEventFeedback.findAll", query = "SELECT m FROM MajlisEventFeedback m"),
    @NamedQuery(name = "MajlisEventFeedback.findByFeedbackId", query = "SELECT m FROM MajlisEventFeedback m WHERE m.feedbackId = :feedbackId"),
    @NamedQuery(name = "MajlisEventFeedback.findByEventId", query = "SELECT m FROM MajlisEventFeedback m WHERE m.eventId = :eventId"),
    @NamedQuery(name = "MajlisEventFeedback.findByUserId", query = "SELECT m FROM MajlisEventFeedback m WHERE m.userId = :userId"),
    @NamedQuery(name = "MajlisEventFeedback.findByRatings", query = "SELECT m FROM MajlisEventFeedback m WHERE m.ratings = :ratings")})
public class MajlisEventFeedback implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "FEEDBACK_ID")
    private Integer feedbackId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "EVENT_ID")
    private int eventId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "USER_ID")
    private int userId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "RATINGS")
    private int ratings;

    public MajlisEventFeedback() {
    }

    public MajlisEventFeedback(Integer feedbackId) {
        this.feedbackId = feedbackId;
    }

    public MajlisEventFeedback(Integer feedbackId, int eventId, int userId, int ratings) {
        this.feedbackId = feedbackId;
        this.eventId = eventId;
        this.userId = userId;
        this.ratings = ratings;
    }

    public Integer getFeedbackId() {
        return feedbackId;
    }

    public void setFeedbackId(Integer feedbackId) {
        this.feedbackId = feedbackId;
    }

    public int getEventId() {
        return eventId;
    }

    public void setEventId(int eventId) {
        this.eventId = eventId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getRatings() {
        return ratings;
    }

    public void setRatings(int ratings) {
        this.ratings = ratings;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (feedbackId != null ? feedbackId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MajlisEventFeedback)) {
            return false;
        }
        MajlisEventFeedback other = (MajlisEventFeedback) object;
        if ((this.feedbackId == null && other.feedbackId != null) || (this.feedbackId != null && !this.feedbackId.equals(other.feedbackId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.dmssw.orm.models.MajlisEventFeedback[ feedbackId=" + feedbackId + " ]";
    }
    
}
