/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dmssw.orm.models;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Shanka
 */
@Entity
@Table(name = "majlis_group_event_status")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MajlisGroupEventStatus.findAll", query = "SELECT m FROM MajlisGroupEventStatus m"),
    @NamedQuery(name = "MajlisGroupEventStatus.findByStatusId", query = "SELECT m FROM MajlisGroupEventStatus m WHERE m.statusId = :statusId"),
    @NamedQuery(name = "MajlisGroupEventStatus.findByStatus", query = "SELECT m FROM MajlisGroupEventStatus m WHERE m.status = :status")})
public class MajlisGroupEventStatus implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "STATUS_ID")
    private Integer statusId;
    @Size(max = 20)
    @Column(name = "STATUS")
    private String status;
    @JoinColumn(name = "EVENT_ID", referencedColumnName = "EVENT_ID")
    @ManyToOne
    private MajlisGroupEvent eventId;
    @OneToMany(mappedBy = "eventStatus")
    private List<MajlisMobileUsersEvent> majlisMobileUsersEventList;

    public MajlisGroupEventStatus() {
    }

    public MajlisGroupEventStatus(Integer statusId) {
        this.statusId = statusId;
    }

    public Integer getStatusId() {
        return statusId;
    }

    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public MajlisGroupEvent getEventId() {
        return eventId;
    }

    public void setEventId(MajlisGroupEvent eventId) {
        this.eventId = eventId;
    }

    @XmlTransient
    public List<MajlisMobileUsersEvent> getMajlisMobileUsersEventList() {
        return majlisMobileUsersEventList;
    }

    public void setMajlisMobileUsersEventList(List<MajlisMobileUsersEvent> majlisMobileUsersEventList) {
        this.majlisMobileUsersEventList = majlisMobileUsersEventList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (statusId != null ? statusId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MajlisGroupEventStatus)) {
            return false;
        }
        MajlisGroupEventStatus other = (MajlisGroupEventStatus) object;
        if ((this.statusId == null && other.statusId != null) || (this.statusId != null && !this.statusId.equals(other.statusId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.dmssw.orm.models.MajlisGroupEventStatus[ statusId=" + statusId + " ]";
    }
    
}
