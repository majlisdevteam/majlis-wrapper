/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dmssw.orm.models;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Shanka
 */
@Entity
@Table(name = "majlis_event_comment_reply")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MajlisEventCommentReply.findAll", query = "SELECT m FROM MajlisEventCommentReply m"),
    @NamedQuery(name = "MajlisEventCommentReply.findByCommentReplyId", query = "SELECT m FROM MajlisEventCommentReply m WHERE m.commentReplyId = :commentReplyId"),
    @NamedQuery(name = "MajlisEventCommentReply.findByReplyCommentMessage", query = "SELECT m FROM MajlisEventCommentReply m WHERE m.replyCommentMessage = :replyCommentMessage"),
    @NamedQuery(name = "MajlisEventCommentReply.findByDateInserted", query = "SELECT m FROM MajlisEventCommentReply m WHERE m.dateInserted = :dateInserted"),
    @NamedQuery(name = "MajlisEventCommentReply.findByUserInserted", query = "SELECT m FROM MajlisEventCommentReply m WHERE m.userInserted = :userInserted")})
public class MajlisEventCommentReply implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "COMMENT_REPLY_ID")
    private Integer commentReplyId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2000)
    @Column(name = "REPLY_COMMENT_MESSAGE")
    private String replyCommentMessage;
    @Column(name = "DATE_INSERTED")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateInserted;
    @Column(name = "USER_INSERTED")
    private Integer userInserted;
    @JoinColumn(name = "PARENT_COMMENT_ID", referencedColumnName = "COMMENT_ID")
    @ManyToOne(optional = false)
    private MajlisEventComment parentCommentId;

    public MajlisEventCommentReply() {
    }

    public MajlisEventCommentReply(Integer commentReplyId) {
        this.commentReplyId = commentReplyId;
    }

    public MajlisEventCommentReply(Integer commentReplyId, String replyCommentMessage) {
        this.commentReplyId = commentReplyId;
        this.replyCommentMessage = replyCommentMessage;
    }

    public Integer getCommentReplyId() {
        return commentReplyId;
    }

    public void setCommentReplyId(Integer commentReplyId) {
        this.commentReplyId = commentReplyId;
    }

    public String getReplyCommentMessage() {
        return replyCommentMessage;
    }

    public void setReplyCommentMessage(String replyCommentMessage) {
        this.replyCommentMessage = replyCommentMessage;
    }

    public Date getDateInserted() {
        return dateInserted;
    }

    public void setDateInserted(Date dateInserted) {
        this.dateInserted = dateInserted;
    }

    public Integer getUserInserted() {
        return userInserted;
    }

    public void setUserInserted(Integer userInserted) {
        this.userInserted = userInserted;
    }

    public MajlisEventComment getParentCommentId() {
        return parentCommentId;
    }

    public void setParentCommentId(MajlisEventComment parentCommentId) {
        this.parentCommentId = parentCommentId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (commentReplyId != null ? commentReplyId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MajlisEventCommentReply)) {
            return false;
        }
        MajlisEventCommentReply other = (MajlisEventCommentReply) object;
        if ((this.commentReplyId == null && other.commentReplyId != null) || (this.commentReplyId != null && !this.commentReplyId.equals(other.commentReplyId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.dmssw.orm.models.MajlisEventCommentReply[ commentReplyId=" + commentReplyId + " ]";
    }
    
}
