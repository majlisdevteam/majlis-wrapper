/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dmssw.orm.models;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Shanka
 */
@Entity
@Table(name = "majlis_birthday_template")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MajlisBirthdayTemplate.findAll", query = "SELECT m FROM MajlisBirthdayTemplate m"),
    @NamedQuery(name = "MajlisBirthdayTemplate.findByTemplateId", query = "SELECT m FROM MajlisBirthdayTemplate m WHERE m.templateId = :templateId"),
    @NamedQuery(name = "MajlisBirthdayTemplate.findByTemplateSystemId", query = "SELECT m FROM MajlisBirthdayTemplate m WHERE m.templateSystemId = :templateSystemId"),
    @NamedQuery(name = "MajlisBirthdayTemplate.findByTemplateTitle", query = "SELECT m FROM MajlisBirthdayTemplate m WHERE m.templateTitle = :templateTitle"),
    @NamedQuery(name = "MajlisBirthdayTemplate.findByTemplateIconPath", query = "SELECT m FROM MajlisBirthdayTemplate m WHERE m.templateIconPath = :templateIconPath"),
    @NamedQuery(name = "MajlisBirthdayTemplate.findByTemplateMessage", query = "SELECT m FROM MajlisBirthdayTemplate m WHERE m.templateMessage = :templateMessage"),
    @NamedQuery(name = "MajlisBirthdayTemplate.findByDateInserted", query = "SELECT m FROM MajlisBirthdayTemplate m WHERE m.dateInserted = :dateInserted"),
    @NamedQuery(name = "MajlisBirthdayTemplate.findByUserInserted", query = "SELECT m FROM MajlisBirthdayTemplate m WHERE m.userInserted = :userInserted"),
    @NamedQuery(name = "MajlisBirthdayTemplate.findByDateModified", query = "SELECT m FROM MajlisBirthdayTemplate m WHERE m.dateModified = :dateModified"),
    @NamedQuery(name = "MajlisBirthdayTemplate.findByUserModified", query = "SELECT m FROM MajlisBirthdayTemplate m WHERE m.userModified = :userModified")})
public class MajlisBirthdayTemplate implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "TEMPLATE_ID")
    private Integer templateId;
    @Size(max = 20)
    @Column(name = "TEMPLATE_SYSTEM_ID")
    private String templateSystemId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2000)
    @Column(name = "TEMPLATE_TITLE")
    private String templateTitle;
    @Size(max = 4000)
    @Column(name = "TEMPLATE_ICON_PATH")
    private String templateIconPath;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5000)
    @Column(name = "TEMPLATE_MESSAGE")
    private String templateMessage;
    @Column(name = "DATE_INSERTED")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateInserted;
    @Size(max = 50)
    @Column(name = "USER_INSERTED")
    private String userInserted;
    @Column(name = "DATE_MODIFIED")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateModified;
    @Size(max = 50)
    @Column(name = "USER_MODIFIED")
    private String userModified;
    @JoinColumn(name = "TEMPLATE_STATUS", referencedColumnName = "CODE_ID")
    @ManyToOne(optional = false)
    private MajlisMdCode templateStatus;

    public MajlisBirthdayTemplate() {
    }

    public MajlisBirthdayTemplate(Integer templateId) {
        this.templateId = templateId;
    }

    public MajlisBirthdayTemplate(Integer templateId, String templateTitle, String templateMessage) {
        this.templateId = templateId;
        this.templateTitle = templateTitle;
        this.templateMessage = templateMessage;
    }

    public Integer getTemplateId() {
        return templateId;
    }

    public void setTemplateId(Integer templateId) {
        this.templateId = templateId;
    }

    public String getTemplateSystemId() {
        return templateSystemId;
    }

    public void setTemplateSystemId(String templateSystemId) {
        this.templateSystemId = templateSystemId;
    }

    public String getTemplateTitle() {
        return templateTitle;
    }

    public void setTemplateTitle(String templateTitle) {
        this.templateTitle = templateTitle;
    }

    public String getTemplateIconPath() {
        return templateIconPath;
    }

    public void setTemplateIconPath(String templateIconPath) {
        this.templateIconPath = templateIconPath;
    }

    public String getTemplateMessage() {
        return templateMessage;
    }

    public void setTemplateMessage(String templateMessage) {
        this.templateMessage = templateMessage;
    }

    public Date getDateInserted() {
        return dateInserted;
    }

    public void setDateInserted(Date dateInserted) {
        this.dateInserted = dateInserted;
    }

    public String getUserInserted() {
        return userInserted;
    }

    public void setUserInserted(String userInserted) {
        this.userInserted = userInserted;
    }

    public Date getDateModified() {
        return dateModified;
    }

    public void setDateModified(Date dateModified) {
        this.dateModified = dateModified;
    }

    public String getUserModified() {
        return userModified;
    }

    public void setUserModified(String userModified) {
        this.userModified = userModified;
    }

    public MajlisMdCode getTemplateStatus() {
        return templateStatus;
    }

    public void setTemplateStatus(MajlisMdCode templateStatus) {
        this.templateStatus = templateStatus;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (templateId != null ? templateId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MajlisBirthdayTemplate)) {
            return false;
        }
        MajlisBirthdayTemplate other = (MajlisBirthdayTemplate) object;
        if ((this.templateId == null && other.templateId != null) || (this.templateId != null && !this.templateId.equals(other.templateId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.dmssw.orm.models.MajlisBirthdayTemplate[ templateId=" + templateId + " ]";
    }
    
}
