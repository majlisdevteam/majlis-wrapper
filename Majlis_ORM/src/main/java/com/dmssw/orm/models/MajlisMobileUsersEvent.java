/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dmssw.orm.models;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Shanka
 */
@Entity
@Table(name = "majlis_mobile_users_event")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MajlisMobileUsersEvent.findAll", query = "SELECT m FROM MajlisMobileUsersEvent m"),
    @NamedQuery(name = "MajlisMobileUsersEvent.findByEventId", query = "SELECT m FROM MajlisMobileUsersEvent m WHERE m.majlisMobileUsersEventPK.eventId = :eventId"),
    @NamedQuery(name = "MajlisMobileUsersEvent.findByUserId", query = "SELECT m FROM MajlisMobileUsersEvent m WHERE m.majlisMobileUsersEventPK.userId = :userId")})
public class MajlisMobileUsersEvent implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected MajlisMobileUsersEventPK majlisMobileUsersEventPK;
    @JoinColumn(name = "USER_ID", referencedColumnName = "MOBILE_USER_ID", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private MajlisMobileUsers majlisMobileUsers;
    @JoinColumn(name = "EVENT_ID", referencedColumnName = "EVENT_ID", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private MajlisGroupEvent majlisGroupEvent;
    @JoinColumn(name = "EVENT_STATUS", referencedColumnName = "STATUS_ID")
    @ManyToOne
    private MajlisGroupEventStatus eventStatus;

    public MajlisMobileUsersEvent() {
    }

    public MajlisMobileUsersEvent(MajlisMobileUsersEventPK majlisMobileUsersEventPK) {
        this.majlisMobileUsersEventPK = majlisMobileUsersEventPK;
    }

    public MajlisMobileUsersEvent(int eventId, int userId) {
        this.majlisMobileUsersEventPK = new MajlisMobileUsersEventPK(eventId, userId);
    }

    public MajlisMobileUsersEventPK getMajlisMobileUsersEventPK() {
        return majlisMobileUsersEventPK;
    }

    public void setMajlisMobileUsersEventPK(MajlisMobileUsersEventPK majlisMobileUsersEventPK) {
        this.majlisMobileUsersEventPK = majlisMobileUsersEventPK;
    }

    public MajlisMobileUsers getMajlisMobileUsers() {
        return majlisMobileUsers;
    }

    public void setMajlisMobileUsers(MajlisMobileUsers majlisMobileUsers) {
        this.majlisMobileUsers = majlisMobileUsers;
    }

    public MajlisGroupEvent getMajlisGroupEvent() {
        return majlisGroupEvent;
    }

    public void setMajlisGroupEvent(MajlisGroupEvent majlisGroupEvent) {
        this.majlisGroupEvent = majlisGroupEvent;
    }

    public MajlisGroupEventStatus getEventStatus() {
        return eventStatus;
    }

    public void setEventStatus(MajlisGroupEventStatus eventStatus) {
        this.eventStatus = eventStatus;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (majlisMobileUsersEventPK != null ? majlisMobileUsersEventPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MajlisMobileUsersEvent)) {
            return false;
        }
        MajlisMobileUsersEvent other = (MajlisMobileUsersEvent) object;
        if ((this.majlisMobileUsersEventPK == null && other.majlisMobileUsersEventPK != null) || (this.majlisMobileUsersEventPK != null && !this.majlisMobileUsersEventPK.equals(other.majlisMobileUsersEventPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.dmssw.orm.models.MajlisMobileUsersEvent[ majlisMobileUsersEventPK=" + majlisMobileUsersEventPK + " ]";
    }
    
}
