/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dmssw.orm.models;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Shanka
 */
@Entity
@Table(name = "majlis_event_comment")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MajlisEventComment.findAll", query = "SELECT m FROM MajlisEventComment m"),
    @NamedQuery(name = "MajlisEventComment.findByCommentId", query = "SELECT m FROM MajlisEventComment m WHERE m.commentId = :commentId"),
    @NamedQuery(name = "MajlisEventComment.findByCommentMessage", query = "SELECT m FROM MajlisEventComment m WHERE m.commentMessage = :commentMessage"),
    @NamedQuery(name = "MajlisEventComment.findByDateInserted", query = "SELECT m FROM MajlisEventComment m WHERE m.dateInserted = :dateInserted"),
    @NamedQuery(name = "MajlisEventComment.findByUserInserted", query = "SELECT m FROM MajlisEventComment m WHERE m.userInserted = :userInserted")})
public class MajlisEventComment implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "COMMENT_ID")
    private Integer commentId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2000)
    @Column(name = "COMMENT_MESSAGE")
    private String commentMessage;
    @Column(name = "DATE_INSERTED")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateInserted;
    @Column(name = "USER_INSERTED")
    private Integer userInserted;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "parentCommentId")
    private List<MajlisEventCommentReply> majlisEventCommentReplyList;
    @JoinColumn(name = "COMMENT_EVENT_ID", referencedColumnName = "EVENT_ID")
    @ManyToOne(optional = false)
    private MajlisGroupEvent commentEventId;

    @Column(name = "COMMENT_LIKE")
    private int commentLike;


    public MajlisEventComment() {
    }

    public MajlisEventComment(Integer commentId) {
        this.commentId = commentId;
    }

    public MajlisEventComment(Integer commentId, String commentMessage) {
        this.commentId = commentId;
        this.commentMessage = commentMessage;
    }

    public Integer getCommentId() {
        return commentId;
    }

    public void setCommentId(Integer commentId) {
        this.commentId = commentId;
    }

    public String getCommentMessage() {
        return commentMessage;
    }

    public void setCommentMessage(String commentMessage) {
        this.commentMessage = commentMessage;
    }

    public Date getDateInserted() {
        return dateInserted;
    }

    public void setDateInserted(Date dateInserted) {
        this.dateInserted = dateInserted;
    }

    public Integer getUserInserted() {
        return userInserted;
    }

    public void setUserInserted(Integer userInserted) {
        this.userInserted = userInserted;
    }

    @XmlTransient
    public List<MajlisEventCommentReply> getMajlisEventCommentReplyList() {
        return majlisEventCommentReplyList;
    }

    public void setMajlisEventCommentReplyList(List<MajlisEventCommentReply> majlisEventCommentReplyList) {
        this.majlisEventCommentReplyList = majlisEventCommentReplyList;
    }

    public MajlisGroupEvent getCommentEventId() {
        return commentEventId;
    }

    public void setCommentEventId(MajlisGroupEvent commentEventId) {
        this.commentEventId = commentEventId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (commentId != null ? commentId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MajlisEventComment)) {
            return false;
        }
        MajlisEventComment other = (MajlisEventComment) object;
        if ((this.commentId == null && other.commentId != null) || (this.commentId != null && !this.commentId.equals(other.commentId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.dmssw.orm.models.MajlisEventComment[ commentId=" + commentId + " ]";
    }

    public int getCommentLike() {
        return commentLike;
    }

    public void setCommentLike(int commentLike) {
        this.commentLike = commentLike;
    }
    
    

}
