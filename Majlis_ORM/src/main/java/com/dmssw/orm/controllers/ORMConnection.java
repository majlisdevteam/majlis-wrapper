/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dmssw.orm.controllers;

import com.dmssw.orm.config.AppParams;
import com.dmssw.orm.config.HibernateUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.query.Query;

/**
 *
 * @author Nadishan Amarasekara
 */
public class ORMConnection {

    public static Logger logger;

    public ORMConnection() {

        AppParams.systemName = "Majlis_ORM";
        logger = AppParams.logger;

    }

    /**
     * session start and begin transactions
     *
     * @return session variable with began transaction
     */
    public Session beginTransaction() {

        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
        } catch (HibernateException e) {
            logger.error("Cannot open session. Error : " + e.getMessage());
            
            throw new HibernateException(e.getMessage());
        }
        return session;
    }

    /**
     *
     * @param session started session
     * @param obj object that want to save
     * @return last return id
     */
    public int createObject(Session session, Object obj) {
        int lastRow = -1;
        try {
            if (session != null) {

                Serializable ser = session.save(obj);

                if (ser != null) {
                    lastRow = (Integer) ser;
                }

            } else {
                logger.error("Object cannot create. Session is null");
            }

        } catch (HibernateException e) {
            session.getTransaction().rollback();

            logger.error("Object cannot create. Error :  " + e.getMessage());

        }

        return lastRow;
    }

    /**
     *
     * @param session session that started
     * @param obj object that want to update
     * @return session
     */
    public Session updateObject(Session session, Object obj) {
        try {

            if (session != null) {

                session.saveOrUpdate(obj);
            } else {
                logger.error("Object cannot update. Session is null");

            }

        } catch (Exception e) {
            if (session != null) {
                session.getTransaction().rollback();
            }
            session = null;

            logger.error("Object cannot update. Error :  " + e.getMessage());
            
            throw new HibernateException(e.getMessage());

        }
        return session;
    }

    /**
     *
     * @param session session that created
     * @return value : 1 => complete, 2 => Session is null, 3=> Error occur
     */
    public int commitObject(Session session) {

        int result = 0;

        try {
            if (session != null) {
                session.getTransaction().commit();

                result = 1;

            } else {
                result = 2;
                logger.error("Object cannot commit. Session is null");

            }

        } catch (Exception e) {

            result = 3;
            if (session != null) {
                session.getTransaction().rollback();
            }
            logger.error("Object cannot commit. Error :  " + e.getMessage());
            
            throw new HibernateException(e.getMessage());

        } finally {
            if (session != null) {

                session.close();

            }
        }

        return result;
    }

    /**
     *
     * @param hql
     * @return list of result
     */
    public List<Object> hqlGetResults(String hql) {
        
        List<Object> list = new ArrayList<>();

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {

            Query query = session.createQuery(hql);

            list = query.list();
        } catch (HibernateException e) {
            logger.error("Cannot get the result. Error is "+ e.getMessage());
            throw new HibernateException(e.getMessage());
            
        }
        return list;
    }

    public List<Object> hqlGetResultsWithLimit(String hql, int start, int end) {
        
        List<Object> list = new ArrayList<>();

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {

            Query query = session.createQuery(hql);
            query.setFirstResult(start);
            query.setMaxResults(end);

            list= query.list();
        } catch (HibernateException e) {
            logger.error("Error with get results "+ e.getMessage());
            throw new HibernateException(e.getMessage());
        }
        return list;
    }

    /**
     *
     * @param hql
     * @param parameterList
     * @return List of result
     */
    public List<Object> hqlGetResults(String hql, Map<String, Object> parameterList) {

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {

            Query query = session.createQuery(hql);

            parameterList.forEach((key, value) -> {

                query.setParameter(key, value);
            });
            return query.list();
        } catch (HibernateException e) {
            logger.error("Error in hqlGetResults "+e.getMessage());
            throw new HibernateException(e.getMessage());
        }

    }

    /**
     *
     * @param session
     * @param hql
     * @return number of affected rows
     */
    public int hqlUpdate(Session session, String hql) {

        try {

            session = HibernateUtil.getSessionFactory().openSession();

            Query query = session.createQuery(hql);

            return query.executeUpdate();

        } catch (HibernateException e) {

           logger.error("error in hqlUpdate "+e.getMessage());

            throw new HibernateException(e.getMessage());

        } finally {

            if (session != null) {

                session.close();

            }

        }

    }

    /**
     *
     * @param session
     * @param hql
     * @param parameterList
     * @return number of affected rows
     */
    public int hqlUpdate(Session session, String hql, Map<String, Object> parameterList) {

        try {

            session = HibernateUtil.getSessionFactory().openSession();

            Query query = session.createQuery(hql);

            parameterList.forEach((key, value) -> {

                query.setParameter(key, value);
            });

            return query.executeUpdate();

        } catch (HibernateException e) {

            logger.error("Error in hqlUpdate "+e.getMessage());

            throw new HibernateException(e.getMessage());

        } finally {

            if (session != null) {

                session.close();

            }

        }

    }

    /**
     *
     * @param session
     * @param hql
     * @return number of affected rows
     */
    public int hqlDelete(Session session, String hql) {

        try {

            session = HibernateUtil.getSessionFactory().openSession();

            Query query = session.createQuery(hql);

            return query.executeUpdate();

        } catch (HibernateException e) {

            logger.error("Error in hqlDelete "+ e.getMessage());

            throw new HibernateException(e.getMessage());

        } finally {

            if (session != null) {

                session.close();

            }

        }

    }

    /**
     *
     * @param session
     * @param hql
     * @param parameterList
     * @return number of affected rows
     */
    public int hqlDelete(Session session, String hql, Map<String, Object> parameterList) {

        try {

            session = HibernateUtil.getSessionFactory().openSession();

            Query query = session.createQuery(hql);

            parameterList.forEach((key, value) -> {

                query.setParameter(key, value);
            });

            return query.executeUpdate();

        } catch (HibernateException e) {

            logger.error("Error in hqlDelete "+e.getMessage());
            
            throw new HibernateException(e.getMessage());

        } finally {

            if (session != null) {

                session.close();

            }

        }

    }

    /**
     *
     * @param session
     * @param hql
     * @return number of affected rows
     */
    public int hqlInsert(Session session, String hql) {

        try {

            session = HibernateUtil.getSessionFactory().openSession();

            Query query = session.createQuery(hql);

            return query.executeUpdate();

        } catch (HibernateException e) {

            logger.error("Error in hqlInsert "+e.getMessage());

            throw new HibernateException(e.getMessage());

        } finally {

            if (session != null) {

                session.close();

            }

        }

    }
}
